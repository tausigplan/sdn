\subsection{From Programs to Formulas}
\label{sec:vc}

We now show how to convert \CSDN\ programs into first-order formulas, which
can be fed into the theorem prover.
We use the standard Dijkstra's weakest (liberal) precondition calculus,
originally invented for specifying the meaning of guarded
commands~\cite{cacm:Dijkstra75}.
The main idea is to compute a weakest formula in first-order logic that
ensures that execution of a command $c$ leads to the state satisfying a
postcondition $Q$.
Such formula is called the \emph{weakest liberal precondition} $\wlp{c}(Q)$.
The formula $\wlp{c}(Q)$ can be used to check the correctness of $c$ for a
given precondition $P$ by asserting that $P \implies \wlp{c}(Q)$.
Alternatively, models of $P \land \neg \wlp{c}(Q)$ are counterexample to the
fact that $Q$ holds when $c$ is executed on states satisfying $P$.
Finally, $I$ is an inductive invariant for a command $c$ if and only if
$I \implies \wlp{c}(I)$.
Notice that these rules only apply to prove the safety of the networks.

\tabref{WP} contains syntax directed rules for computing
the weakest (liberal) preconditions of \CSDN\ commands.
These rules compute a formula representing the largest
set of states from which a command executes without failure,
thus defining the axiomatic meaning of \CSDN\ commands.
The first section defines the meaning of atomic commands.
The second section defines the meaning of controller and switch events.
For brevity, we omit the rules for the while-loops (e.g.,
see~\cite{frade2011verification}), which assert that
(i)~the loop invariant initially holds,
(ii)~that the loop invariant is preserved by the loop body, and
(iii)~that the loop invariant and the negation of program condition imply the
postcondition.

It is interesting to note that destructive updates to
relations (insertions and removals) are simply handled
using Boolean operations. An alternative is to use
a version of McCarthy~\cite{McCarthy62} store functions specialized to
updating relations in the way Table~\ref{ta:WP} prescribes.
However, in our case, this just introduces more overhead than
savings because relations are never passed as first-class objects
and there are no nested access patterns, where McCarthy stores are
known to provide a more succinct representation.

Controller events $\packetin(s, p, i)$ are assumed to be triggered when a
packet arrives at a switch and there is no entry in the flow table for
handling this packet.
Switch events $\flow(s, p, \ioPorts{i}{o})$ represent a new packet arriving at a switch and being
handled according to an existing entry in the flow table.
Observe that this existing packet-handling rule can use the output port
\texttt{null} to drop the packet.
We will slightly abuse the notation, as flow events are implicit and do not
include commands.
We call events and their commands \emph{guarded commands}.

\begin{table*}
\[
\begin{array}{|l|}
\hline
\wlp{\textbf{skip}}(Q) \eqdef Q\\
\wlp{\textbf{assume}~F}(Q) \eqdef  F \Rightarrow Q\\
\wlp{\textbf{assert}~F}(Q) \eqdef   F \land Q\\
%\WP(\textbf{forward}(s, p, e), Q) = Q [\fr{x}{y}{z}{t}) \mapsto \fr(x, y, z, t) \lor (x=s \land  y=p \land z=i \land t=e)]\\
\wlp{r.\textbf{insert}~P}(Q) \eqdef  \substitute{Q}{r(\vec{x})}{r(\vec{x}) \lor \semp{P}_{FO}(\vec{x})}\\
\wlp{r.\textbf{remove}~P}(Q) \eqdef  \substitute{Q}{r(\vec{x})}{r(\vec{x}) \land \neg \semp{P}_{FO}(\vec{x})}\\
\wlp{s.\textbf{flood}(p, i)}(Q) \eqdef
  \substitute{Q}{S.\mfr(P, I, O)}{S.\mfr(P, I, O)
  \lor (S = s \land P = p \land I = i \land O \neq i \land O \neq null)}\\
\wlp{\textbf{if}~b~\textbf{then}~c_1~\textbf{else}~c_2}(Q)\eqdef
(b \Rightarrow \wlp{c_1}(Q)) \land (\neg b \Rightarrow \wlp{(c_2}(Q))\\
\wlp{c_1 ; c_2}(Q) \eqdef  \wlp{c_1}(\wlp{c_2}(Q))\\
\hline
\wlp{\packetin(s, p, i) \Rightarrow c}(Q) \eqdef
  (\rcv{s}{p}{i} \land \neg \exists O : \Ports.~s.\ftTwo{p}{\ioPorts i O}) \implies \wlp{c}(Q) \\
\wlp{\flow(s, p, i, o) \Rightarrow \dquote{\textbf{forward}}}(Q) \eqdef
  (\rcv{s}{p}{i} \land s.\ftTwo{p}{\ioPorts i o}) \implies  \wlp{s.\bforward(p, i, o)}(Q) \\
\hline
\end{array}
\]
\caption{\label{ta:WP}%
Rules for computing weakest (liberal) preconditions for \CSDN\ programs.
$\substitute{Q}{\varphi}{\psi}$ denotes the substitution of all occurrences of
$\varphi$ in $Q$ by $\psi$.
The meaning of predicates $\semp{P}_{FO}$ is a first-order formula over $r$'s
columns defined in \tabref{predMeaning}.
}
%\vspace{-5pt}
\end{table*}

\begin{table}
\[
\begin{array}{|l|}
\hline
\semp{\gr{exp}}_{FO}(t) \eqdef \gr{exp} = t \\
%\semp{\gr{Fid}: \gr{Exp}}_{FO}(t) \eqdef  \gr{Fid}(\gr{Exp}) = t\\
\semp{*}_{FO}(t) \eqdef \textbf{True} \\
\semp{P_1 \land P_2}_{FO}(t) \eqdef \semp{P_1}_{FO}(t) \land \semp{P_2}_{FO}(t) \\
\hline
\semp{P_1, P_2, \ldots, P_k}_{FO}(t_1, t_2, \ldots, t_k) \eqdef
  \Land_{i=1}^k \semp{P_i}_{FO}(t_i) \\[0.2em]
\hline
\end{array}
\]
\caption{\label{ta:predMeaning}%
Converting predicates into first-order formulas.
The meaning of predicates $\semp{P}$ is a first-order formula over the columns of the relation.
%In the forward rule $i$ is ingress port passed to the unhandled event.
}
\end{table}

%The following theorem establishes the soundness and completeness of the
%generation of verification conditions based on liberal weakest preconditions.
%\begin{theorem}[Soundness and Completeness of Verification Conditions]
%For every \CSDN\ program $\mathcal{P}$ and candidate invariant $I$, $I$ is invariant
%under arbitrary sequence of flow and controller events if and only if for
%every guarded command $gc$ in $\mathcal{P}$,
%$I \implies \wlp{gc}(I)$ holds.
%\end{theorem}

\paragraph{Priorities}
The \OF\ standard supports priorities that allow a programmer to install some
rules without removing existing rules.
Only the flow rule with the maximal priority is executed.
We implement flow tables with priorities by including an extra column in $\mft$.
Accordingly, the semantics of the flow event from \tabref{WP} is modified as
\[
\begin{array}{l}
\wlp{\flow(s, p, i, o) \Rightarrow \dquote{\textbf{forward}}}(Q) \eqdef\\
\quad(\rcv{s}{p}{i} \land \exists \alpha \colon \Priorities.\,
\maxprift(\alpha, s, p, i, o)) \implies\\
\quad\wlp{s.\textbf{forward}(p, i, o)}(Q)
\end{array}
\]
with $\Priorities = \Nat$.
Here, predicate $\maxprift(\alpha, s, p, i, o)$ denotes the fact that $\alpha$
is the maximal priority for the matching flow rules, i.e.,
$s.\ftThree{\alpha}{p}{\ioPorts{i}{o}}$ and
\[
\forall \alpha' \colon \Priorities, O' \colon \Ports.~~
s.\ftThree{\alpha'}{p}{\ioPorts{i}{O'}} \implies \alpha' \leq \alpha.
\]
