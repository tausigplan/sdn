\section{Extensions}
\label{sec:Ext}

This section sketches potential extensions to \RSDN.

\paragraph{Timeouts}
The \OF\ standard supports timeouts which allow rules to expire.
%\begin{changebar}
In principle, timeouts can be handled by adding an event that models the switches behavior in
case of a rule timeout: the switch removes the ``expired'' rule,
and \RSDN\ would then re-validate the correctness of the invariants,
essentially validating \emph{all}
sequences of events in which certain rules expire.
However, in practice it may be necessary to reason about order of rule installation in these cases.
%\end{changebar}
%\aaron{This is not
%strictly true because the controller's view of the flow tables and the
%switches view of the flow tables will be inconsistent until the timeout event
%is processed by the controller.  We would need this to happen atomically,
%similar to the case where an $ft.insert$ call needs to be followed by a
%barrier message to make its installation atomic. To support hard timeouts, we
%could install a rule in the SDN switch that has no timeout and have the
%controller start a timer for the rule; when the timer experiences at the
%controller, the controller issues a command to delete the respective flow
%entry followed by a barrier message to maintain the consistency of the
%controller and switch's view of the flow table. Soft timeouts, which are
%based on a switch not receiving packets that match a flow entry for some
%amount of time, cannot be supported in the same way and require some other
%trick, which is yet to be determined.}

%\newcommand{\rewriteDst}{\textit{rewriteDst}}
\paragraph{Manipulating Packet Headers}
It is possible to handle rewriting of packet headers by adding appropriate
uninterpreted functions and global invariants which define their properties.
For example, a statement such as $p.src$:=$h$ can be modeled via a
substitution:
\begin{equation*}
  \big[
    \textrm{if}~\alpha=p~\textrm{then}~h~\textrm{else}~\msrc(\alpha)
    \big/ \msrc(\alpha)
  \big]
\end{equation*}
However, using such substitutions of functions may complicate reasoning and
further experience is required to determine tractability.

We can also support other fields in the packet header (such as protocol, TTL,
etc.) by defining more uninterpreted functions.

\paragraph{Transitive Closure}
Certain properties of network-wide state can be naturally expressed using transitive closure.
For example an interesting case of ``permanent'' forwarding loops
occurs when the flow tables across the switches contain a forwarding loop,
causing some packet $p$ to go round and round indefinitely.
This is expressible as:
\[
\TC(S, S)(S_1, S_2). \exists \en, \eg \colon \Ports .
  \mft(S_1, p, \en, \eg) \land \tps{S_1}{\eg}{\en}{S_2}
\]
where
$\TC(x, y)(z, t)P(z, t)$ holds if there exist $v_0, v_1, \ldots v_n$ such that
(i)~$v_0 = x$,
(ii)~$v_n = y$,
and
(iii)~for all $0 \leq i < n$, $P(v_i, v_{i+1})$ holds.
In~\cite{cav:IBINS13}, it is shown how to reason precisely on transitive
closure of unique paths using SAT solvers which may be useful here.


\paragraph{Temporal Safety Properties}
\RSDN\ was designed to prove that a controller program is correct in the sense that it preserves an assertion on the network-wide state.
However, as observed in \cite{nsdi:CaniniVPKR12} many network properties can be stated as forbidden sequences of events.
For example, the firewall controller program has the ``direct path property'',  which means
that once a packet reached its destination future packets to the same host are not going to be sent to the controller.
This can be checked by assuring that
sequences of events of the following form are infeasible:
\begin{equation}
\packetin(s, p_1, \_, \_) ; \cdots  ; \packetin(s, p_2, \_, \_)
\label{eq:direct}
\end{equation}
where $p_1.src = p_2.src$ and $p_1.dst = p_2.dst$.
$\dquote{\cdots}$ denotes any sequence of network events,
and can be simulated by an operation which mutates the network-wide state arbitrarily into another state satisfying the invariants.
Tools like Z3 support such operations using multiple-vocabulary formulas.
Notice that \equref{direct} denotes many sequences of events.

The learning switch does not have the direct path property.
It satisfies a weaker property ``strict direct path''  which means that once two packets travel both ways between a source and destination
future packets are not going to the controller.
This can be checked by assuring that the following sequences are infeasible:
\[
\packetin(s, p_1, \_, \_) ; \cdots ; \packetin(s, p_2, \_, \_) ; \cdots  ; \packetin(s, p_3, \_, \_)
\]
%%begin{equation}
%\begin{array}{c}
%\packetin(s, p_1, \_, \_) \\
%\vdots \\
%\packetin(s, p_2, \_, \_)  \\
%\vdots \\
% \packetin(s, p_3, \_, \_)
%\label{eq:strictDirect}
%\end{array}
%\end{equation}
where $p_1.dst = p_2.src$, $p_1.src = p_2.dst$,
$p_3.src = p_2.src$, and $p_3.dst = p_2.dst$.

%\paragraph{Proving General Temporal Formulas}
%In principle, it is possible to add time stamps to packets and reason about time using first order quantifications.
%However, proving liveness properties is beyond the scope of this paper.
