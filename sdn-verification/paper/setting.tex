\section{Symbolically Modeling SDN Properties with First-Order Logic}
\label{sec:setting}

We now show that many interesting properties of software-defined networks can
be naturally expressed in first-order logic.
We use relations to model the network topology, the flow tables of the
switches, the SDN program's internal state, as well as histories of
transmitted packets.
Each of the relations is potentially infinite.
\CSDN\ commands manipulate relations by inserting and removing tuples from
relations.
Queries over network states are defined using first-order formulas.

\subsection{Predefined Relations}

\begin{table*}
\[
\begin{array}{|l|l|p{3.2in}|}
\hline
\textbf{Relation} & \textbf{Attributes} & \textbf{Intended Meaning}\\
\hline
%
\tph{S}{O}{H} &
S\in \Switches, O\in \Ports, H\in \Hosts &
\textrm{Host $H$ is directly connected to switch $S$ via port $O$} \\
%
\tps{S_1}{I_1}{I_2}{S_2} &
S_1\in \Switches, I_1, I_2\in \Ports, S_2\in \Switches &
\textrm{Port $I_1$ of switch $S_1$ is directly connected to port $I_2$ of switch $S_2$} \\
%
\tprh{S}{O}{H} &
S\in \Switches, O\in \Ports, H\in \Hosts &
\textrm{There is a path from $S$ via port $O$ to a host $H$} \\
%
\tprs{S_1}{I_1}{I_2}{S_2} &
S_1, S_2\in \Switches, I_1, I_2\in \Ports &
\textrm{There is a path from port $I_1$ of switch $S_1$ to port $I_2$ of switch $S_2$} \\
%
S.\ftTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}} &
S\in \Switches, \mSrc, \mDst\in \Hosts, I, O\in \Ports &
\textrm{Switch $S$ has a rule to forward packets $\packet{\mSrc}{\mDst}$
arriving from port $I$ to port $O$} \\
%
S.\frTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}} &
S\in \Switches, \mSrc, \mDst\in \Hosts, I, O\in \Ports &
\textrm{Packet $\packet{\mSrc}{\mDst}$ arrived at ingress $I$ is forwarded to egress $O$} \\
%
\rcv{S}{\packet{\mSrc}{\mDst}}{I} &
S\in \Switches, \mSrc, \mDst\in \Hosts, I\in \Ports &
\textrm{Packet $\packet{\mSrc}{\mDst}$ is received at ingress port $I$} \\
\hline
%
\end{array}
\]
\caption{\label{ta:BuildIn}%
Built-in relations describing network states.}
%\vspace{-5pt}
\end{table*}

\tabref{BuildIn} describes the predefined relations supported by \RSDN.
In addition, the programmer also can define her own relation symbols.
The relations $\tph{S}{O}{H}$, $\tps{S_1}{I_1}{I_2}{S_2}$, $\tprh{S}{O}{H}$,
and $\tprs{S_1}{I_1}{I_2}{S_2}$ represent the physical network topology.
SDN programs generally do not explicitly manipulate these
relations, other than populating them based on link-level discovery protocol
(LLDP) information reported by SDN switches.
% tball doesn't say much
%They also are useful to express interesting invariants and
%topology constraints and to effect the control flow of the controller
%code.

For clarity, a packet header is represented as a pair $\packet{\mSrc}{\mDst}$.
Our implementation supports different packet header fields as functions $\Packets\to \textit{Values}$.
The most interesting built-in relation involving packet headers is $S.\ftTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}}$, which
represents the switches' forwarding tables.
This relation denotes the semantics of the switch and not its concrete
storage.
For example, the SDN program command
$s.\binstall(\packet{h}{*}, \ioPorts{i}{o})$ updates switch $s$
to include a general forwarding rule for all packets from host $h$
coming from ingress port $i$ to be forwarded to port $o$.
This is implemented in \OF\ by installing a general matching rule based on the source of the packets.
The symbolic effect on the $\textit{ft}$ relation is to add \emph{all} the tuples
$S.\ftTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}}$ where $S=s$, $\mSrc=h$, $I=i$, and $O=o$ ($\mDst$ is unconstrained).
The effect is handled in the generated verification condition using a
first-order formula expressing the weakest precondition of this command by substituting the $\mathit{ft}$ relation symbol.
Sending to egress port $O=\nullp$ models dropping packets.

%We simplify the reasoning task by not considering the change of packet headers,
%priorities, and timeouts. We explain how these can be handled in \secref{Ext}.

The \emph{history} relation $S.\frTwo{p}{\ioPorts{I}{O}}$ records packets
whose header is $p$ forwarded by a switch $S$ from ingress port $I$ to egress
port $O$.
This can happen either by executing a forwarding rule at a switch or by sending a packet
to the controller which then instructs the switch to forward the
packet from $I$ to $O$.
This relation records the history of forwarding and is used for reasoning.
For example, it occurs in the invariant $\I_1$ defined in \equref{i1}.

Note that ``$S.r(\vec{x})$'' is just syntactic sugar for the first-order formula ``$r(S, \vec{x})$'',
used to enhance readability.

The predicate $\rcv{S}{P}{I}$ allows assertions to refer to the packet
currently being handled by the switch or the controller code.
The examples of such assertions include \emph{transition invariants},
defined below.
For a controller event $\packetin(s, p, i)$
(respectively, for a switch event $\flow(s, p, \ioPorts{i}{o})$), 
$\rcv{S}{P}{I}$ holds if and only if $S=s$, $P=p$ and $I=i$.


\subsection{Invariants}
\label{sec:invariants}

\figref{FOLSyntax} shows the syntax of standard (typed) first-order formulas
which are used to describe invariants of SDN programs and
topology constraints.  In the atomic formulas, $\gr{Rid}$ is either a
predefined or a user-defined relation.
For readability, we write atomic formulas
$r(S, \langle\mSrc, \mDst\rangle, I, O)$ as
$S.r(\packet{\mSrc}{\mDst}, \ioPorts{I}{O})$,
where $S$ denotes an arbitrary switch, $\mSrc$ denotes a source host, $\mDst$
denotes a destination host, $I$ is an input port, and $O$ is an output port.

\RSDN\ supports three kinds of invariants:
\begin{itemize}
\item[(i)] The \emph{topo} invariants define the admissible topologies.
  These are assumed to hold in the initial state.  \RSDN\ checks that these
  invariants are consistent with \emph{safety} and \emph{trans} invariants and
  that together they form an inductive invariant that is preserved under the
  execution of switch and controller events.
\item[(ii)] The \emph{safety} invariants are supposed to hold at the
  initial state and be preserved for any execution of switch and controller
  event sequence.
\item[(iii)] The \emph{trans} invariants are checked %initially and
  after the execution of every event.
  They describe the properties of transitions caused by the event in a similar
  way to postconditions in procedures.
\end{itemize}

\noindent
\RSDN\ simplifies the verification task by assuming that both switch and
controller events are executed atomically.
In particular, when the controller executes a sequence of commands the
invariant is checked before and after the whole sequence and not after
individual commands.
It is straightforward to check that the invariant holds after every command.
However, this will lead to many false alarms as the code usually assumes
atomicity.

%For example, the invariant $I_1$ shown in \equref{i1} is a first-order logic
%formula using the $\fr{\sw}{\pk}{\en}{\eg}$ relation symbol.
%\aaron{Is this last sentence necessary?}
%We use  standard transitive closure to reason about paths in the network.
%Formally, $\TC(x, y)(z, t)P(z, t)$ holds if there exist $v_0, v_1, \ldots v_n$ such that
%(i)~$v_0 = x$,
%(ii)~$v_n = y$,
%and
%(iii)~for all $0 \leq i < n$, $P(v_i, v_{i+1})$ holds.

\begin{figure}
\[
\begin{array}{|lll|l|}
\hline
\gr{F} & ::= & \textbf{True} & \texttt{true} \\
       & | & \gr{Trm} = \gr{Trm} & \texttt{equality} \\
       & | & \gr{Rid} (\gr{Trm}^*) & \texttt{atomic formulas} \\
       & | & \forall \alpha\colon\gr{Tid}. \gr{F} & \texttt{universal quantification} \\
       & | & \exists \alpha\colon\gr{Tid}. \gr{F} & \texttt{existential quantification} \\
%       & | & \TC(\alpha, \beta)(\gamma, \delta)[\gr{Tid}].  \gr{F} & \texttttt{transitive closure}\\
       & | & \neg \gr{F} & \texttt{negation} \\
       & | & \gr{F} \land \gr{F} & \texttt{conjunction} \\
       & | & \gr{F} \lor \gr{F} & \texttt{disjunction} \\
\gr{Trm} & ::= & %\gr{Fid}(\gr{Trm}^*) & \texttt{expressions}\\
               \alpha & \texttt{logical variable} \\
         & | & \gr{Fid} (\gr{Trm}) & \texttt{uninterpreted functions} \\
\hline
\end{array}
\]
\caption{\label{fi:FOLSyntax}%
A syntax of first-order formulas.
% with transitive closure.
%In the transitive closure operation, $\alpha$, $\beta$, $\gamma$, and $\delta$ are ranged over the same type $\gr{Type}$.
}
\end{figure}


\subsubsection{Topology Invariants}

\begin{table}
\[
\begin{array}{|l|l|p{1.0in}|}
\hline
\textbf{T} & \textbf{Formula} & \textbf{Intended Meaning}\\
\hline
T_1 &
%\forall S\colon \Switches, I_1, I_2 \Ports.
\neg \tps{S}{I_1}{I_2}{S} & \textit{no self-loops}\\
T_2 &
% \forall S_1, S_2\Switches, I_1, I_2 \Ports.
\begin{array}{l}
\tps{S_1}{I_1}{I_2}{S_2} \implies \\
\qquad\qquad \tps{S_2}{I_2}{I_1}{S_1}
\end{array} &
  \textit{symmetry of links}\\
T_3 &
% \forall S\colon \Switches, P\colon \Packets, I \colon \Ports.
\begin{array}{l}
\rcv{S}{P}{I} \implies \\
\qquad\qquad \tprh{S}{I}{P.\msrc}
\end{array} &
  \textit{packets arrive from reachable hosts}\\
%T_4 & \forall S\colon \Switches, H\colon \Hosts, I_1, I_2\colon \Ports.
%\tprh{S}{I_1}{H} \land \tprh{S}{I_2}{H} \implies I_1 = I_2   & \textit{unique ports}\\
T_4 &
%\forall N, M\colon Int.
\port{M} = \port{N} \implies M = N & \textit{injective ports}\\
\hline
\end{array}
\]
\caption{\label{ta:GlobalInv}%
Examples of interesting topology invariants for SDNs.}
\end{table}

First-order logic can express many topology invariants naturally, as
shown in~\tabref{GlobalInv}.  These invariants are crucial for \RSDN\
to precisely reason about networks.  For example, the invariant $T_3$
asserts that packets cannot be received from disconnected hosts.
Without this invariant the theorem prover may issue false messages.
Notice that some of these invariants may not be relevant or need not
hold for some topologies.  The current implementation provides a
library of invariants which can optionally be included in the
controller code.
%From a reasoning
%perspective, the most expensive one is $T_4$.  When the number of
%ports $n$ is known, it can be replaced by the invariant
%\[
%\Land_{0 \leq i,j < n} \port{i} = \port{j} \implies i =j
%\]
%\nikolaj{This is {\bf not} expensive. Z3 should detect axioms like $T_4$
%and transform them into $\forall N: \mathit{invPort}(\port(N)) = N$,
%which has the same effect to mean that port is injective. This axiom gets
%instantiated only as many times as $\port{}$ occurs in the formulas}

%\subsection{Interesting General Invariants}
%

\subsubsection{Safety Invariants}
\RSDN\ permits \emph{safety invariants} that define the required consistency
of network-wide states.
\RSDN\ checks that every event preserves all the topology and safety
invariants.
In the firewall example, we check that $\I_1 \land \I_2 \land \I_3$ is preserved
by the execution of flow and controller events.

%The more interesting invariants are the ones specific for the program.
%The learning switch is an example of a simple program with potentially complicated invariants.
%A simple requirement is that the flow table only includes correct entries expressed as:
%\begin{equation}
%I_4 \eqdef
% \forall S\colon SW, P \colon PK, I \colon PR, O \colon PR . \begin{array}{l}\ft{S}{\pk}{I}{\pk.dst} \implies\\
%    \tph{S}{I}{O}\end{array}
%\end{equation} \aaron{This invariant is broken: $P.dst$ is a host, not a port, but it is being used in the flow table as a port.}
%\RSDN\ reports that this invariant is not inductive and we need to add the requirement that the $\mathtt{connected}$ auxiliary relation correctly records connections:
%\begin{equation}
%I_5 \eqdef
% \forall S\colon SW, I, O \colon PR, H \colon HO . \begin{array}{l}\mathtt{connected}(S, O, H) \implies\\
%    \tph{S}{I}{O}\end{array}
%\end{equation}
%Now $I_4 \land I_5$ is an inductive invariant and \RSDN\ is able to prove it.

\subsubsection{Transition Invariants}
\RSDN\ also permits the programmer to define \emph{transition invariants},
which describe the effect of executing event handlers.
\RSDN\ checks that all the transition invariants are satisfied after the
execution of every switch and controller event.

A simple example of a transition invariant is the absence of ``black holes'',
i.e., packets are never dropped.
This is expressed as
\begin{equation*}
  \rcv{S}{\packet{\mSrc}{\mDst}}{I} \implies
  \exists O \in \Ports: S.\frTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}}
\end{equation*}

\noindent
The latter invariant does not hold in the firewall example since it drops
packets from untrusted hosts.
However, the invariant does hold for the simple learning switch shown in
\figref{Learning}.
This SDN program learns connected hosts as soon as new packets from
them appear.
When a packet arrives, it is forwarded to the port to which the destination
host is connected or, if this port is unknown, the packet is sent via $\flood$
to all the ports excluding the input port.
%Learning new connections allows to use better forwarding algorithm, thus
%reducing the network traffic.
%
\begin{figure}
\begin{tabbing}
sd \= sd \= sd \= asdfasdfasdfasdfaasdfsdkfjkkk \= \kill
\rel\ $\connected\,(\Switches, \Ports, \Hosts) = \{\}$   \> \> \> \> // new relation with 3 args \\
\packetin$(s, \packet{\msrc}{\mdst}, i) \Rightarrow$   \> \> \> \> // packet from $\msrc$ into $\mdst$ \\
\>  \bvar\ $o: PR$                                   \> \> \> // var. for egress ports \\
\>  $\connected.\rinsert (s, i, \msrc)$              \> \> \> // learn a new connection \\
\>  \rif\ $\connected (s, o, \mdst)$ \rthen          \> \> \> // if dest. is already known \\
\> \> $s.\bforward (\packet{\msrc}{\mdst}, \ioPorts{i}{o})$                           \> \> // forward the packet \\
\> \> $s.\binstall (\packet{\msrc}{\mdst}, \ioPorts{i}{o})$                           \> \> // install a new rule \\
\>  \relse\ $s.\flood(\packet{\msrc}{\mdst}, i)$                         \> \> \> // flood if dest. is unknown
\end{tabbing}
\caption{\label{fi:Learning}%
A simple learning switch controller code}
\end{figure}

Interestingly, with \RSDN\ one can even state (and prove) that the learning
switch SDN program correctly forwards packets.
One way to express this using the transition invariant
\begin{equation*}
\begin{array}{ll}
  \mathit{trans}:
    & \rcv{S}{\packet{\mSrc}{\mDst}}{I} \land O \neq I \land \tprh{S}{O}{\mDst} \implies \\
    & \quad S.\frTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}}
\end{array}
\end{equation*}
However, \RSDN\ reports that this invariant is violated in network topologies where multiple ports can be used to reach hosts.
We can restrict the topology by adding an extra topology invariant
\begin{equation*}
\begin{array}{ll}
%T \eqdef \begin{array}{l} %\forall S\colon \Switches, H\colon \Hosts, I_1, I_2\colon \Ports.
\mathit{topo}: &
\tprh{S}{I_1}{H} \land \tprh{S}{I_2}{H} \implies I_1 = I_2 %\hspace{2cm}
\end{array}
\end{equation*}

Alternatively, it is possible to state the correctness of the learning switch
in networks with multiple outgoing ports using the formula $L_4$ shown in
\tabref{Learning}.
%\[
%\begin{array}{ll}
%% \forall S \colon  SW, P \colon  PK, I \colon PR.\\
%\mathit{trans}_1: & (\rcv{S}{P}{I} \land O_1 \neq I \land \tprh{S}{O_1}{P.dst}
%    ) \implies\\
%              & \enspace       \exists O_2\colon PR. \tprh{S}{O_2}{P.dst} \land \fr{S}{P}{I}{O_2}
%\end{array}
%\]
Here, $O_1$ quantifies over the existence of path and $O_2$ denotes the port
chosen by the flood command or via learning.
This invariant is a bit complicated since it deals with situations in which
there are multiple ports leading to the destination host.
In fact, it is possible to show that the learning switch also correctly learns
the connections using the invariants shown in \tabref{Learning}.

%Finally, it is even possible to use the topology restriction $T$ to show with \RSDN\ that the learning switch learns using the invariants:
%\begin{eqnarray}
%I_7 & \eqdef & \begin{array}{l} \forall S \colon  SW, P \colon  PK, I \colon PR. \rcv{S}{P}{I}  \implies\\
% \mathtt{connected}(S, I, P.src)\end{array}\\
%I_8 & \eqdef &  \begin{array}{l}\forall S \colon  SW, P \colon  PK, I, O \colon PR.\\
%    (\rcv{S}{P}{I} \land \mathtt{connected}(S, O,P.dst)) \implies\\
%                      \fr{S}{P}{I}{O}\end{array}
%\end{eqnarray}
%Again \RSDN\ will report a bug showing states of the switches which are inconsistent with the state of the controller, in our case the
%connected relations.
%Thus, to show that $I_7 \land I_8$ are inductive we need to make sure that connected relation is consistent with the
%state of the switch tables:
%\begin{eqnarray}
%I_9 & \eqdef & \forall S \colon  SW, P \colon  PK, I \colon PR.  \begin{array}{l}\ft{S}{P}{I}{O}  \implies\\
% \mathtt{connected}(S, O, P.dst)\end{array}\\
%I_{10} & \eqdef & \forall S \colon  SW, P \colon  PK, I \colon PR.  \begin{array}{l}\ft{S}{P}{I}{O}  \implies\\
%\mathtt{connected}(S, O, P.src)\end{array}
%\end{eqnarray}
%\aaron{Don't both invariants need $O$ in the $\forall$ part of the formula?  Also, shouldn't the second invariant use $I$ in the $\mathtt{connected}$ relation instead of $O$?}
%%\tabref{SpecInv} expresses some \CSDN\ invariants in first-order logic.
%%$I_4$ and $I_5$ expresses the correctness of the stateful firewall code shown in \figref{FireWall}.
%%$I_4$ expresses the correctness of the stateless firewall code shown in \figref{StatelessFireWall}.
%%
%%\begin{table*}
%%\[
%%\begin{array}{|l|l|l|}
%%\hline\hline
%\textbf{I} & \textbf{Formula} & \textbf{Intended Meaning}\\
%\hline
%I_4 &  \forall s \colon \Switches, p \colon \Packets. \fr(s, k, 2, 1)  \Rightarrow  \exists p': \Packets: p'.dst = p.src  \land  \fr(s, p�, 1, 2) &
%\texttt{Limited forwarding}\\
%I_5 &  \forall s \colon \Switches, h \colon \Hosts. \textit{trusted}(s, h)  \Rightarrow  \exists p': \Packets: p'.dst = h  \land  \fr(s, p�, 1, 2) &
%\texttt{Correct trusted recording}\\
%I_6 & \forall s\colon \Switches, p \colon \Packets, i\colon \Ports, o\colon \Ports.
%ft(s, p, i, o) \Rightarrow \tph(s, o, p.dst) & \texttt{Correct rule instalation}\\
%I_7 & \forall s\colon \Switches, sw, p \colon \Packets, i \colon \Ports, o \colon \Ports. \tph(s, o, p.dst))  \land p.src) \neq  p.dst) \Rightarrow
%\fr(s, p, i, o) & \texttt{All packets forwarded}\\
%I_8 & \forall s\colon \Switches, sw\colon \Switches,  i \colon \Ports, h \colon \Hosts. \textit{connect}(s, i, h) \Rightarrow
%\tph(s, i, h) & \texttt{Correct connection recording}\\
%\hline\hline
%\end{array}
%\]
%\caption{\label{ta:SpecInv}%
%Protocol specific invariants.}
%\end{table*}

\begin{table*}
\[
\begin{array}{|c|l|l|}
\hline
\textbf{I} & \textbf{Formula} & \textbf{Intended Meaning}\\
\hline
L_1 & S.\ftTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}} \implies \tprh{S}{O}{\mDst}
    & \textit{Correctly learned connections} \\
L_2 & \connected(S, I, H) \implies \tprh{S}{I}{H}
    & \textit{Consistent SDN program data structure} \\
L_3 & S.\ftTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O}} \implies
        \connected(S, I, \mSrc) \land \connected(S, O, \mDst)
    & \textit{Consistent learning} \\
\hline
L_4 &
\begin{array}{l}
    \rcv{S}{\packet{\mSrc}{\mDst}}{I} \land (\exists O_1 \in \Ports:\, O_1 \neq I \land \tprh{S}{O_1}{\mDst}) \implies \\
    \quad\exists O_2\in \Ports: \tprh{S}{O_2}{\mDst} \land S.\frTwo{\packet{\mSrc}{\mDst}}{\ioPorts{I}{O_2}}
\end{array}
    & \textit{Guranteed forwarding} \\
\hline
\end{array}
\]
\caption{\label{ta:Learning}%
Invariants for the learning switch.
$L_1, L_2, L_3$ are safety invariants and $L_4$ is a transition invariant.}
%\vspace{-5pt}
\end{table*}
