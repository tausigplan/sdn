\section{Related Work}
\label{sec:related}

The past few years have witnessed a surge of interest in SDNs.
We now discuss the work most relevant to ours along these lines.

\paragraph{Language abstractions.}
\cite{IEEECOMM:FosterGRSFKMRRSWH13,sigcomm:VoellmyWYFH13} introduce
abstractions for programming controllers in order to simplify the task of
programming controllers.
\cite{PLDI:GuhaRF13} shows that the compiler from a high-level language,
called NetCore, to OpenFlow generates semantically equivalent code.
\cite{FlowLog} defines a nice declarative language to ease the task of programming and verifying SDN programs.
NetKAT~\cite{DBLP:conf/popl/AndersonFGJKSW14} presents an equational calculus
for imperative, finite state, SDN programs.
Declarative programming is also successfully used for updating multi-tenant
networks~\cite{NLOG}.

In contrast, our focus is on the orthogonal problem of verifying the safety of
infinite state SDN programs.
The ultimate goal is to verify SDN programs in stylized Java or Python, but we
focus on a imperative language like \CSDN\ which captures the essence of imperative SDN programming.
In the future it is worthwhile to apply VeriCon to declarative programs.

\paragraph{Finite-state model checking of SDN programs.}
NICE~\cite{nsdi:CaniniVPKR12} was the first system to use finite-state
model checking to verify the correctness of SDN controllers.
The SDN program is modeled as a state-transition system with events
similar to those in \RSDN.
The concrete network topology is also explicitly modeled.
Finite state model checking has many advantages over Hoare style
verification: it does not require inductive invariants, and it can employ simpler verification technology.
It can be easily applied to arbitrary programs.
However, it is unsound in the sense that it can never prove the absence of
errors in the infinite state SDN program.
Also, it is hard to scale finite state model checking to realistic networks.
In contrast, we use first-order logic to model the admissible topology and
network-wide invariants.
Consequently, \RSDN\ is able to verify the absence of errors and can also
potentially handle huge topologies.
We note, however, that \RSDN\ relies on user-provided invariants and a
first-order (potentially non-terminating) theorem prover. Our preliminary experience with Z3 is fairly
positive.

Finite-state model checking has also been applied to verify
SDN programs on large networks~\cite{FMACAD:SNM13}.
Two examples, the learning switch and the stateful firewall from this work,
use manual abstractions.
Our results establish that verification with VeriCon (with infinite states) is
orders of magnitude faster than the approach in~\cite{FMACAD:SNM13}
($0.13s$ vs. $68352s$ for the finite-state abstraction).


Verificare~\cite{Verificare} also uses finite state CTL model checking.
The FlowLog~\cite{FlowLog} system also employs finite-state model checking by limiting the number of packets sent.
It also shows that for some specifications this bound suffices to obtain sound results.


In~\cite{conext:uzniarPCVK12}, NICE was extended to perform concolic
testing~\cite{pldi:GodefroidKS05} and thus reduce the number of missed bugs.
%Static analysis techniques such as shape analysis can be applied in the
%future to automatically infer first-order invariants~\cite{TOPLAS:SRW02}.



\paragraph{Checking invariants by analyzing snapshots of the network.}
\cite{nsdi:KVM12} suggests a novel method for checking certain network
properties by analyzing packet headers.
As with the above schemes, the approach described in~\cite{nsdi:KVM12} can
establish the existence of bugs but not their absence.

\paragraph{Dynamically checking SDN programs.}
\cite{CCR:KhurshidZCG12,nsdi:KCZCMW13} show how to monitor the correctness of
certain properties of SDN programs in real time.
The main challenge is to guarantee that this can be accomplished without
harming network performance.
\RSDN\ runs at compile-time, and so it verifies correctness or, alternatively,
exhibits errors before the code is actually executed.
We view controller code verification ala \RSDN\ and dynamic checking as in
\cite{CCR:KhurshidZCG12,nsdi:KCZCMW13} as two complementary approaches.
