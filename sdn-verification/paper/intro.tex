\section{Introduction}

Software defined networking (SDN) is an emerging architecture for
operating and managing computer networks, fueled by adoption by major
technology companies~\cite{SIGCOMM:Jain13}.  SDN allows network
administrators to program a (logically) centralized software-based
network ``controller'' that maintains a global view of the network
rather than manage tens of thousands of lines of configuration
scattered among thousands of network devices (e.g., routers).  By
centralizing network control and separating it from network hardware,
SDN enables network administrators to run (in-house and third-party) programs on top of the controller, e.g.,
for route computation and to enforce access control policies, without having to wait
for features to be embedded in router/switch vendors' proprietary and
closed software environments.

The controller configures the network devices, called
``switches'', through a simple API in the form of a \emph{flow table}: each
flow table entry contains a set of packet fields to match, and an
action to be taken, such as ``send packet out of port X'' or ``drop
packet''. When a switch receives a packet it seeks a matching flow
entry in its table; if the table has no matching flow entries, the
switch sends the packet to the controller, which can then add a flow
entry directing the switch how to handle similar packets in the
future. The controller maintains a global view of the network by
gathering information from the switches (e.g., traffic flow statistics
and switch/link failures) and can change the flow table in response to
changes in the network and network traffic. The controller
effectively plays the role of the network's operating system; programs
running on top of it (often called ``applications'') can use the
controller to specify forwarding rules, access control policies, etc., without
directly interacting with network hardware or with other SDN programs.

Operating large networks is a complex task that is highly prone to error. This
problem is only expected to be exacerbated as network configuration shifts
from today's human time scale to event-driven automated configuration with
SDN. Guaranteeing network-wide invariants (e.g., enforcing access control) is
hence of great importance. SDN opens up the possibilities of applying formal
methods to SDN programs to prove the correctness of computer networks. Indeed,
much effort has recently been invested in applying finite state model checking
to check that SDN programs behave correctly. However, generally speaking,
finite-state model checking of SDN programs suffers from two main problems:
(1) scaling these methods to large networks is highly nontrivial; and (2)
finite-state model checking might identify errors, but cannot guarantee the
absence of errors. We present \RSDN, a tool for \emph{provably} verifying
network-wide invariants of SDN programs at \emph{compile time}.

\RSDN\ symbolically reasons about (potentially infinite) network states to verify
that network-wide invariants are preserved for \emph{any} sequence of
``events'' (e.g., the controller receives a packet header from a switch,
or a link fails) and on \emph{all} admissible topologies, where
invariants and topologies are expressed via first-order logic. \RSDN\
is \emph{sound} in the sense that if it outputs ``no errors'' then the
preservation of the specified invariants is guaranteed. When
verification fails, \RSDN\ displays a concrete scenario that violates the invariants, in
the form of an admissible topology and an event, and it is therefore a
useful tool for debugging controller programs.
Notice that \RSDN\ reasons about both the controller and switch correctness.


We designed a simple imperative programming language, called
\textbf{C}ore SDN (\CSDN), for writing SDN programs.
\CSDN\ is but a means to an end; we use
it to illustrate that Hoare style verification of SDN programs is feasible in
{\em many} programming languages.
\RSDN\ models the semantics of both \emph{controller events}, such as the receipt of a packet from a
switch at the controller, and \emph{switch events}, such as executing a rule entry in a switch's flow table and forwarding an
incoming packet to a certain port (or dropping it).
Thus, sequences of events in \RSDN\ capture both controller-to-switch interaction and switch-to-switch interaction,
making it possible to reason about SDN programs' behavior for arbitrary sequences of network events (at both the controller and the switches).
In fact, we treat the network as just a guarded command program where guards model controller and switch events.
This allows us to naturally handle the non-deterministic aspect of networks where switch events can be triggered
if the switch includes a matching rule in the flow table, and otherwise controller events occur.
Also, these events interact in a non-trivial way.
The handler of controller events can change the content of the flow table and then a switch event can cause a violation of
an invariant.
A premature installation of a flow table rule can break an invariant if the state of the controller is not correctly updated after
subsequent switch event.
\RSDN\ can catch these kinds of errors before the program is executed, provided that the programmer can specify the desired invariants.

States in
\CSDN\ consist of relations representing the network topology and the
flow tables of individual switches, and also of auxiliary relations
that maintain the controller's information about the global network state.
The state~--- of the controller and switches~--- is updated
by adding or removing tuples from relations (e.g., the switches' flow tables).
\CSDN\ was designed in the spirit of the OpenFlow standard~\cite{OpenFlowSpec1.4.0}.
%so that \CSDN\ controller programs can be compiled to execute
%efficiently against the OpenFlow API. \CSDN\ is an event-driven program;


\RSDN\ takes as input a \CSDN\ program as well as formulas
in first-order logic that specify constraints on the network topology
and desired safety properties (network-wide invariants).
%\RSDN\ reports a bug if the topology constraints are not satisfiable (no
%admissible topology exists) and otherwise generates a verification
%condition, i.e., a first-order formula that is valid if and only if
%every event preserves the specified invariants for every admissible
%topology and event.
\RSDN\ employs the standard  weakest preconditions~\cite{cacm:Dijkstra75} in order to generate verification condition (VC).
The VC is a first order formula
which holds if and only if: (i)~the initial network state satisfies the invariant
and (ii)~the invariant is inductive, i.e., the execution of arbitrary controller and switch events maintain the invariant.
The semantics of the controller events is taken from the \CSDN\ program.
The semantics of the switch is dictated by the OpenFlow standard ~\cite{OpenFlowSpec1.4.0}.
The soundness of completeness of such approaches is completely standard (e.g.,
see~\cite{frade2011verification}).



An automated theorem prover (Z3~\cite{DBLP:conf/tacas/MouraB08})
checks the verification condition, to either verify the correctness of
the controller program or to generate a concrete network topology and
event that violate the safety properties.
It is well known
that tools like Z3 are not guaranteed to terminate in general.  However, we
notice that the generated VCs for many network protocols belong to a class of
formulas that are easy to verify by SAT solvers.


We show that \RSDN is practical for a large repertoire of controller programs, ranging from simple but
well-studied examples, e.g., MAC-learning switches and firewalls, to simplified versions of
recently proposed SDN-based systems:
Resonance~\cite{WREN:NRFC09} for dynamic access control and
Stratos~\cite{arXiv:Gember13} for orchestrating the steering of
traffic through middlebox sequences.
\RSDN\ is compositional, in the sense that it checks the correctness of each
network event independently against the specified invariants, and so it can
scale to handle complex systems well beyond other approaches, e.g.,
finite state model checking, given appropriate inductive invariants.
%We show that for many of the SDN controller programs studied, the
%generated verification conditions are expressible in a strict
%sub-language of first order logic, called ``effectively propositional
%logic'' (EPR), thus explaining why Z3 succeeds in automatically
%verifying the correctness of infinite-state controller programs even
%for complex invariants.

% NSB: Remove per discussion with Tom.
%Verification condition formulas currently assume that events of the
%controller programs and switches are executed
%atomically, which implies that switch rules are  installed instantaneously.



\paragraph{Organization}

\secref{overview} provides an informal overview of \RSDN.
\secref{setting} shows how admissible topologies and network-wide
invariants in SDNs can be formulated in first-order
logic. \secref{controller} discusses how to generate verification
conditions for controller programs.  To simplify exposition, we limit
the discussion in this section to the small core language \CSDN.
\secref{experience} presents the implementation of \RSDN\ and our
experience with verifying SDN controllers.
%\secref{Ext} discusses how
%to extend the scope of the \CSDN\ language and the verification
%procedure.
\secref{related} reviews related work.
