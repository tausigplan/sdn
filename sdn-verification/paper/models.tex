\subsection{From Formulas to Theorems and Models}
\label{sec:proofs-and-models}

Our evaluation in \secref{experience} shows that the verification
conditions can be solved by Z3. The invariants we encountered
so far were all restricted to formulas with a quantifier
prefix $\forall\exists$. Proving such invariants could
easily be highly intractable and we explain the apparent
ease based on the following observation:

%\emph{Observation 1: Functions used in \CSDN{} are record accesses in disguise}.
%The main functions are $\msrc$ and $\mdst$.
%They are used to access packet contents. Reasoning about records and fields in records
%is significantly simpler than reasoning about functions that may be applied recursively.
%Intuitively, functions that act as record access patterns are only applied once and
%a proof search engine does not have to build terms with progressively deeper record
%access patterns.
%Other functions are Skolem functions introduced for existential quantifiers.

\emph{Observation: Instantiation dependencies are shallow}.
By inspecting verification conditions from \CSDN{} programs we observed that
the formulas could be proved or disproved using relatively few instantiations.
The reason is that the formulas do not contain opportunities for \emph{pumping}.
In other words, 
instantiations do not produce new opportunities for instantiations.
For example, when Skolemizing $\I_3 \land \neg \wlp{c}(\I_3)$
we obtain a formula of the form (Skolem functions have hats, and the transition relation
is abstracted as $\rho$):
\[
\begin{array}{ll}
& \ttrusted(S, H)  \implies
S.\frTwo{\packet{\widehat{\mSrc}(S,H)}{H}}{\ioPorts{\port{1}}{\port{2}}} \\
\land & \rho(\ttrusted, \mathit{sent}, \ttrusted', \mathit{sent'}) \\
\land & \ttrusted'(\hat{S}, \hat{H}) \\
\land
& \forall  \mSrc \in \Hosts:  \neg \hat{S}.\mathit{sent}'(\packet{\mSrc}{\hat{H}},\ioPorts{\port{1}}{\port{2}})
\end{array}
\]
The opportunities for instantiating $\mSrc$ is limited to $\widehat{\mSrc}(S,H)$.
%$\forall x.\,\mathit{Nat}(x) \implies \mathit{Nat}(\mathit{succ}(x))$ pumps any
%instantiation $t$ to the \emph{successor} $\mathit{succ}(t)$.
%In contrast, formulas from verifying \CSDN{} programs used

\papercomment{
\emph{Observation 1: functions in \CSDN are record accesses in disguise}.
In fact, \CSDN\ and associated program invariants use just two functions:
$\msrc$ and $\mdst$.
Their role is to access what corresponds to packet header fields.
We could alternatively have written each packet as a record, and used record
construction whenever accessing packets.
But this is neither as succinct using record accessors,
and unnecessary from the point of view of the theorem prover
that covers a similar search space.
There is a very pleasant consequence of this observation:
the use of equalities involving $\msrc$ and $\mdst$ is also seen to be superficial.
For example
\[
\exists P': \Packets. P'.\mdst = P.\msrc \land \fr{S}{P'}{\port{1}}{\port{2}}
\]
corresponds to
\[
\begin{array}{ll}
\multicolumn{2}{l}{\exists P'\msrc, P'\mdst, P'\mathit{rest} .} \\
& P'\mdst = P.\msrc \land \\
& \fr{S}{\mathit{mkPkt}(P'\msrc, P'\mdst, P'\mathit{rest})}{\port{1}}{\port{2}},
\end{array}
\]
which is, by destructive equality resolution, seen equivalent to
\[
\begin{array}{ll}
\multicolumn{2}{l}{\exists P'\msrc, P'\mathit{rest} .} \\
& \fr{S}{\mathit{mkPkt}(P'\msrc, P.\msrc, P'\mathit{rest})}{\port{1}}{\port{2}}.
\end{array}
\]
Luckily, we do not have to encode these observations into Z3; they are already
handled as a by-product of the way the theorem prover solves formulas.

\emph{Observation 2: Instantiation chains are pumping free}. We will explain
more precisely what we mean by \emph{pumping} below, but let us point out
a typical challenge with instantiation-based theorem provers: they may produce
instantiations of quantified formulas using progressively deeper terms.
We have not observed this in the context of \CSDNS\ and we explain
this by pointing out one characteristic of formulas produced by \CSDNS\
that prevent infinite instantiation chains.
From observation 1, we can assume that our formulas do not contain equalities
(or if they contain equalities, these equalities are between ground terms).
Furthermore, we can focus on formulas that are conjunctions of clauses by
appealing to a standard satisfiability preserving CNF transformation.
We assign colors to each argument of each predicate: Initially, we assign
a different color to each distinct predicate/position combination.
we merge two colors $p_i, q_j$
whenever there is a clause that has an occurrence of $p$ and $q$ whose
$i$'th, respectively $j$'th position contain terms that have a quantified
variable in common. Now to our notion, a set of clauses is \emph{pumping free},
if it does not have contain a color with positions $p_i$
and $q_j$ such that the depth of the term at $p_i$ is different
from the term depth at position $q_j$. It is easy to see that
pumping free formulas do not admit
infinite instantiation chains: the maximal term depth that is useful
for checking satisfiability of such formulas is bounded by the deepest
term in the original formula.
The reader can inspect that in
all of the running examples, the formulas are pumping free.
}
