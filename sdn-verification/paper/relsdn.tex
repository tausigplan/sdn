\subsection{The \CSDN\ Language}
\label{sec:csdn}

\figref{SDNSyntax} describes the abstract syntax of \CSDN.
The SDN program first declares external relations, initializes these
relations, and specifies constraints on the network topology.
Unless the programmer restricts the set of admissible topologies
(using the keyword
\textbf{topo}), all topologies are admissible.
%Relations besides the ones representing topology (i.e., $\tph{S}{Pr}{H}$ and $\tps{S_1}{I_1}{I_2}{S_2}$)
%are initially empty by default.
The programmer can initialize user-defined relations and specify
controller-specific invariants.
The built-in relations $\mfr$, $\mrecv$ and $\mft$ are initially empty by
default.
Events have attributes that include the switch where they occur (denoted by $s$), the
sending host ($\msrc$), the receiving host ($\mdst$), and
the ingress port at
which the packet arrives at the switch ($i$).

The controller code reacts to events by performing a sequence of (conditionally executed) commands.
The command $\textbf{assume}~\gr{F}$ instructs the verifier to assume that
$\gr{F}$ holds in the sequel, whereas the command $\textbf{assert}~\gr{F}$
causes the verifier to produce an error if $\gr{F}$ does not hold.
The $\brinsert$ and $\brremove$ commands are used to update
a given relation with a set of tuples.
The $\flood$ command forwards a packet to all switch ports
except the packet's ingress port.
For readability, we define an $\binstall$ command as shorthand for updating
the flow table of the relevant switch and a $\bforward$ command to encode
sending the current packet, i.e.,
\begin{eqnarray*}
\sw.\binstall(\pk, \ioPorts{\en}{\eg}) & \eqdef & \mft.\brinsert(\sw, \pk, \ioPorts{\en}{\eg}) \\
\sw.\bforward(\pk, \ioPorts{\en}{\eg}) & \eqdef & \mfr.\brinsert(\sw, \pk, \ioPorts{\en}{\eg})
\end{eqnarray*}

\begin{figure*}
\[
\begin{array}{|l|}
\hline
\begin{array}{cc}
\begin{array}{rll}
\gr{Ctrl}  ::= & \gr{Init}^* (\gr{Evnt} \Rightarrow \gr{Cmd})^* & \text{controller}\\
\gr{Init}  ::= & \textbf{rel}\; \gr{Rid} (\gr{Tid}^*)  & \text{declare relation}\\
           | & \textbf{rel}\; \gr{Rid} (\gr{Tid}^*) = (\gr{Pred}^*)^* & \text{..with initialization}\\
           | & \textbf{var}\; \gr{Id}: \gr{Tid}  & \text{new variable}\\
           | & \textbf{topo}\; \gr{Fid} : \gr{F} & \text{topology invariant}\\
           | & \textbf{inv}\; \gr{Fid} : \gr{F} &  \text{safety invariant}\\
           | & \textbf{trans}\; \gr{Fid} : \gr{F} & \text{transition invariant}\\
\gr{Event} ::= & \packetin\,(\gr{Exp}^*) & \text{packet-in event}\\
\gr{Exp}   ::= & \gr{const} \;|\; \gr{Id}\\ %\;|\; \gr{Aid}.\gr{Fid} \\
               % & \text{literal, identifier, property}\\
\gr{Cond}  ::= & \textbf{True} \;|\; \textbf{False} & \text{boolean values}\\
           | & \gr{Rid}\,(\gr{Exp}^*) & \text{tuple in a relation}\\
           | & \gr{Exp} = \gr{Exp} & \text{equality}\\
           | & \neg \gr{Cond} & \text{negation}\\
           | & \gr{Cond} \land \gr{Cond} & \text{conjunction} \\
           | & \gr{Cond} \lor \gr{Cond} & \text{disjunction} \\
           %| & (\gr{Cond}) & \text{parenthesis}\\
\end{array} &
\begin{array}{rll}
\gr{Var}   ::= & \textbf{var}\; \gr{Id}: \gr{Tid}  & \text{local variable} \\
\gr{Cmd} ::=  & \textbf{skip} & \text{do nothing}\\
        | & \gr{Id}.\textbf{flood}\,(\gr{Exp}^*) & \text{flood to all ports} \\ %but ingress port of switch}~id\\
        | & \textbf{assume}\;\gr{F} & \text{assume that $\gr{F}$ holds}\\
        | & \textbf{assert}\;\gr{F} & \text{assert that $\gr{F}$ holds}\\
        | & \gr{Rid}. \textbf{insert}\,(\gr{Pred}^*) &  \text{relation insertion} \\ %\text{insert a set of tuples into the relation}~\gr{Rid}\\
        | & \gr{Rid}. \textbf{remove}\,(\gr{Pred}^*) &  \text{relation removal} \\ %\text{remove a set of tuples from the relation}~\gr{Rid}\\
        | & \textbf{if}\;\gr{Cond}\;\textbf{then}\;\gr{Cmd}^* \textbf{else}\;\gr{Cmd}^* & \text{conditional}\\
        | & \textbf{while}\; \gr{Cond}\; \textbf{inv}\; \gr{F}\; \textbf{do}\; \gr{Cmd} & \text{while-loop} \\
        | & \gr{Id} = \gr{Exp} &  \text{variable assignment}\\
        | & \gr{Cmd}\;\gr{Cmd} &  \text{sequence}\\
        | & \{ \gr{Var}^* \gr{Cmd} \} &  \text{block of commands}\\
%       & |& \textbf{foreach}~\Brack{\gr{Pred}^*}  \in \gr{Rid} ~ \textbf{do}~\gr{F}~\gr{Cmd}^* &  \text{Loops}\\
%         & | & \gr{Id} & \text{identifier}\\
%         & | &  \gr{Aid}.\gr{Fid} & \text{property}\\
%         & | & \textbf{call} ~\gr{Fid}(\gr{Exp}) & \text{Foreign Functions}\\
\gr{Pred} ::= & \gr{Exp}  & \text{restrict to values}\\
%           | & \gr{Fid}: \gr{Exp} & \text{restrict to attribute values}\\
           | & \gr{Pred} \land \gr{Pred} & \text{conjunction of preds}\\
           | & * & \text{wildcard}
\end{array}
\end{array}\\
\hline
\end{array}
\]
\caption{\label{fi:SDNSyntax}%
The abstract syntax of \CSDN. $\gr{F}$ is a first-order formula defined in \figref{FOLSyntax}.}
%\vspace{-5pt}
\end{figure*}
