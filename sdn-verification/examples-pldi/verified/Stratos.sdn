# Very simplified version of Stratos (http://stratos.cs.wisc.edu)
# Steers traffic through a chain of middleboxes (in one direction only!)
#
# In this example, we consider the case of one switch only.

var s0 : SW   # The only network switch

# We use PR as indexes of elements
rel element (PR, HO)
rel client  (HO)

rel chain (PR, PR)
var chainHead : PR

# A client cannot occur in the chain
topo ClientsAreNotInChain :
    forall H : HO, E : PR, P : PR .
        client (H) && element (E, H) =>
        E != chainHead && !chain (P, E)

# Packets only sent and received by clients
topo SrcDstClients :
    forall P : PK . client(P.src) && client(P.dst)

# We model a simple version of Stratos with one switch only
topo OneSwitch :
    forall H : HO . exists P : PR . tph (s0, P, H)

# There is at least one instance of each element in the chain
topo OnePlusInstPerElem :
    forall E1 : PR , E2 : PR .
        exists I : HO .
            chainHead = E1 || chain(E2, E1) => element(E1, I) 

# Each instance is only associated with exactly one element
topo OneElemPerInst :
    forall I : HO .
        (exists E : PR . element(E, I)) &&
        forall E1 : PR, E2 : PR .
        element(E1, I) && element(E2, I) => E1 = E2

# chainHead is a head of the chain indeed.
topo ChainHeadIsHead :
    forall E : PR . !chain (E, chainHead)

# The chain must be fully linked
topo ChainLinked :
   forall E : PR , N : PR .
       chain(E, N) => chainHead = E || exists P : PR. chain(P, E) 

# The chain must not fork forwards
topo ChainNoForkForwards :
    forall E : PR, N1 : PR, N2 : PR .
        chain (E, N1) && chain (E, N2) => N1 = N2

# The chain must not fork backwards
topo ChainNoForkBackwards :
    forall E1 : PR, E2 : PR, N : PR .
        chain (E1, N) && chain (E2, N) => E1 = E2

# Forbid self-loops in chains
topo NoLoopsInChains :
    forall E : PR . !chain (E, E)

# There must only be one switch/port per host
topo OneHostPerPort :
    forall H : HO, S1 : SW, S2 : SW, P1 : PR, P2 : PR .
        tph(S1, P1, H) && tph(S2, P2, H) => S1 = S2 && P1 = P2

# There must only be one host per port
topo OnePortPerHost :
    forall S : SW, H1 : HO, H2 : HO, P : PR .
        tph(S, P, H1) && tph(S, P, H2) => H1 = H2

inv flow_consistent :
    forall S : SW, P : PK, I : PR, O : PR,
           srcE : PR, dstE : PR, srcI : HO , dstI : HO.
    ft (S, P, I, O) =>
    (tph (S, I, P.src) && tph(S, O, dstI) => element (chainHead, dstI)) &&
    (tph (S, O, P.dst) &&
      tph (S, I, srcI) && element (srcE, srcI) => forall N : PR . !chain (srcE,N)) ||
    (tph (S, I, srcI) && tph (S, O, dstI) &&
      element (srcE, srcI) && element (dstE, dstI) => chain (srcE, dstE))

inv OnlySendIfRuleExists : forall S : SW, P : PK, I : PR, O : PR .
    sent(S, P, I, O) => ft(S, P, I, O)

# We only install rules based on src and dst fields of the packet header
inv ft_axiom1: 
    forall S : SW, P1 : PK, P2 : PK, I : PR, O : PR .
        ft (S, P1, I, O) && P2.src = P1.src && P2.dst = P1.dst => ft (S, P2, I, O)

rel HasNext (PR)   # HACK: to get a next element in the chain

packetIn (srcSw, pkt, srcPr) ->
    var srcInst : HO 
    var srcElem : PR
    var nxtInst : HO
    var nxtElem : PR
    var nxtPr : PR
    
    # Packet must have come from a host
    if tph (srcSw, srcPr, srcInst) then {
        # If the packet came from a client host, then send it to the first 
        # middlebox in the chain
        if pkt.src = srcInst then {
            assume nxtElem = chainHead
            assume element (nxtElem, nxtInst)
            assume tph (srcSw, nxtPr, nxtInst)

            ft.insert (srcSw, src: pkt.src && dst: pkt.dst, srcPr, nxtPr)
            send (srcSw, pkt, nxtPr) 
        } else {
            assume forall E : PR . HasNext (E) <=> exists N : PR. chain(E, N)
            assume element (srcElem, srcInst)

            if HasNext (srcElem) then {
                # If there is another middlebox in the chain, then send the packet to
                # the next middlebox in the chain
                assume chain (srcElem, nxtElem)
                assume element (nxtElem, nxtInst)
                assume tph (srcSw, nxtPr, nxtInst)

                ft.insert (srcSw, src: pkt.src && dst: pkt.dst, srcPr, nxtPr)
                send (srcSw, pkt, nxtPr) 
            } else {
                # If there are no next element in the chain, try to
                # deliver the packet to the client:
                # if there is a destination host for the packet, then
                # send the packet to it.
                assume tph (srcSw, nxtPr, pkt.dst)

                ft.insert (srcSw, src: pkt.src && dst: pkt.dst, srcPr, nxtPr)
                send (srcSw, pkt, nxtPr)
            }
        }
    }
