rel connected (SW, PR, HO)

topo Wired : forall S : SW, P : PK, I : PR .
    recv(S, P, I) => tph(S, I, P.src)

init topo data_structure_inv_learning_init:
    forall S : SW, H : HO, I : PR . connected(S, I, H) => tph(S, I, H)

########################      Main invariants      ###########################
# 1. The installed flow rules are correct with respect to the topology
inv correct_topology:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => tph(S, O, P.dst)

########################    Auxiliary invariants    ##########################
# 1. Recorded connections are correct with respect to the topology
inv data_structure_inv_learning:
    forall S : SW, H : HO, I : PR . connected(S, I, H) => tph(S, I, H)

# 2. Flow rules are consistent with recorded connections
inv data_structure_inv_flow_table_1:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => connected(S, O, P.dst)

# 3. Flow rules are consistent with recorded connections
inv data_structure_inv_flow_table_2:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => connected(S, I, P.src)


########################    Transition invariants    #########################
trans progress: forall S : SW, P : PK, I : PR, H : HO .
    recv(S, P, I) => connected(S, I, P.src)

trans simple_fwd: forall S : SW, P : PK, I : PR.
    (recv(S, P, I) && (exists O1: PR. O1 != I && tph(S, O1, P.dst))) =>
                      (exists O2: PR. tph(S, O2, P.dst) && sent(S, P, I, O2))


packetIn (s, p, i) ->
    var o : PR
    connected.insert (s, i, p.src)
    if connected(s, o, p.dst) then {
        send (s, p, o)
        ft.insert (s, p, i, o)
    } else {
        flood (s,p)
    }
