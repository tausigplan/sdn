# This program models a process of authentication on the network with a
# learning controller

topo Wired : forall S : SW, P : PK, I : PR .
    recv(S, P, I) => tph(S, I, P.src)

# authentication server
var authServ : HO

rel auth (HO)   # a list of authenticated servers
init auth = (authServ)

rel connected (SW, PR, HO)
rel _sent (SW, PK, PR, PR)

init topo data_structure_inv_learning_init:
    forall S : SW, H : HO, I : PR . connected(S, I, H) => tph(S, I, H)

inv correct_topology:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => tph(S, O, P.dst)
inv data_structure_inv_flow_table_1:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => connected(S, O, P.dst)
inv data_structure_inv_flow_table_2:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => connected(S, I, P.src)
inv data_structure_inv_learning:
    forall S : SW, H : HO, I : PR . connected(S, I, H) => tph(S, I, H)

inv correct_ft_access :
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => auth (P.src) && auth (P.dst)

#inv I_9: forall S : SW, P : PK, I : PR, O : PR, H : HO .
#    sent(S, P, I, O) => connected(S, I, P.src)

# Reply from servAuth means that the destination host is authenticated
trans I_auth :
    forall S : SW, P : PK, I : PR . recv (S, P, I) && P.src = authServ => auth (P.dst)

# Drop all the packets from non-authenticated hosts which are not addressed to authServ
trans flow_access_control :
    forall S : SW, P : PK, I : PR, O : PR .
        recv (S, P, I) && !auth (P.src) && !P.dst = authServ && !_sent (S, P, I, O) => !sent (S, P, I, O)


packetIn (s, p, i) ->
    var o : PR
    assume forall S : SW, P : PK, I : PR, O : PR . ( _sent(S,P,I,O) <=> sent(S,P,I,O) )
    connected.insert (s, i, p.src)
    if p.src = authServ then {   # got a reply from the authentication server
        # destination host is now authenticated
        auth.insert (p.dst)
    }
    #BUG: Forgot to remove all flow entries of the un-authenticated host rendering future authentications
    #      impossible
    else if p.dst = authServ then auth.remove (p.src)
    if auth (p.src) then {       # got a packet from an authenticated host
        # Only allow packets to authenticated hosts
        if auth (p.dst) then {
            if connected(s, o, p.dst) then {
                send (s, p, o)
                ft.insert (s, p, i, o)
            } else {
                flood (s,p)
            }
        }
    } else {                     # the sender is not authenticated (hence, it is not authServ) 
        if p.dst = authServ then
            flood (s, p)
    }
    assert (exists O : PR. O != i && tph (s, O, p.dst)) && 
           (auth(p.src) && auth(p.dst) || !auth(p.src) && p.dst = authServ) =>
             exists O : PR. tph (s, O, p.dst) && sent(s, p, i, O)
