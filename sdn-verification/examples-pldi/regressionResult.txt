=========== Good Benchmarks ===========
	FileName		TotalTime	TotalFormula	TotalQuantifiers	TtlSpec		specDepth
Auth.sdn			0		0		0		0		0
Firewall.sdn			0		0		0		0		0
FirewallMigration.sdn			0		0		0		0		0
Learning.sdn			0		0		0		0		0
Resonance.sdn			0		0		0		0		0
StatelessFirewall.sdn			0		0		0		0		0
Stratos.sdn			0		0		0		0		0

=========== Bad Benchmarks ===========
	FileName			TotalTime	TotalFormula	TotalQuantifiers	TtlSwiches	TtlHosts	TtlSpec		specDepth
Auth-NoFlowRemoval.sdn			0.15		2317		19			2		3		95		1
Firewall-ForgotConsistency.sdn			0.11		969		24			3		5		51		2
Firewall-ForgotPortCheck.sdn			0.13		976		24			4		6		54		2
Firewall-ForgotTrustedInvariant.sdn			0.08		616		16			4		6		36		2
Learning-NoSend.sdn			0.16		1248		18			1		1		76		2
Resonance-StatesNotMutuallyExclusive.sdn			0.16		4440		17			4		11		191		1
StatelessFireWall-AllowAll2to1Traffic.sdn			0.06		444		12			2		4		36		2
