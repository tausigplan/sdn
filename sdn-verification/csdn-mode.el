;;; csdn-mode.el --- Major mode for editing CSDN files

;; Copyright (C) 2014  Kalev Alpernas

;; Author: Kalev Alpernas <kalevalp@tau.ac.il>
;; Keywords: VeriCon CSDN SDN major-mode

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Large part of the code below have been adapted from:
;;
;;   wpdl-mode by Scott Andrew Borton <scott@pp.htv.fi>
;;     see <http://www.emacswiki.org/emacs/wpdl-mode.el> and
;;         <http://www.emacswiki.org/emacs/ModeTutorial>     
;;     
;;   'Writing a Major Mode' by Xah Lee <xah@xahlee.org>
;;     see <http://ergoemacs.org/emacs/elisp_syntax_coloring.html> and
;;         <http://ergoemacs.org/emacs/elisp.html>

;;; Code:

;; define several class of keywords
(setq csdn-keywords '("rel" "type" "func" "topo" "inv" "init" "trans" "var" "packetIn" "import"))
(setq csdn-types '())
(setq csdn-constants '("PR" "PK" "HO" "SW" "BUF" "True" "False"))
(setq csdn-events '())
(setq csdn-functions '("if" "then" "else" "insert" "remove" "assume" "assert" "skip" "flood" "do" "while" "forall" "exists"))

;; The following faces are available by default from font-lock
;; font-lock-builtin-face
;; font-lock-comment-delimiter-face
;; font-lock-comment-face
;; font-lock-constant-face
;; font-lock-doc-face
;; font-lock-function-name-face
;; font-lock-keyword-face                 
;; font-lock-negation-char-face
;; font-lock-preprocessor-face            
;; font-lock-reference-face
;; font-lock-string-face                  
;; font-lock-syntactic-face-function
;; font-lock-type-face                    
;; font-lock-variable-name-face
;; font-lock-warning-face


;; create the regex string for each class of keywords
(setq csdn-keywords-regexp (regexp-opt csdn-keywords 'words))
(setq csdn-type-regexp (regexp-opt csdn-types 'words))
(setq csdn-constant-regexp (regexp-opt csdn-constants 'words))
(setq csdn-event-regexp (regexp-opt csdn-events 'words))
(setq csdn-functions-regexp (regexp-opt csdn-functions 'words))

;; clear memory
(setq csdn-keywords nil)
(setq csdn-types nil)
(setq csdn-constants nil)
(setq csdn-events nil)
(setq csdn-functions nil)

;; create the list for font-lock.
;; each class of keyword is given a particular face
(setq csdn-font-lock-keywords
  `(
    (,csdn-type-regexp . font-lock-type-face)
    (,csdn-constant-regexp . font-lock-constant-face)
    (,csdn-event-regexp . font-lock-builtin-face)
    (,csdn-functions-regexp . font-lock-function-name-face)
    (,csdn-keywords-regexp . font-lock-keyword-face)
))

;; syntax table
(defvar csdn-syntax-table nil "Syntax table for `csdn-mode'.")
(setq csdn-syntax-table
      (let ((synTable (make-syntax-table)))
	
        ;; sh style comment
        (modify-syntax-entry ?# "< b" synTable)
        (modify-syntax-entry ?\n "> b" synTable)
	
	;; fix word delimiters
	(modify-syntax-entry ?_ "w" synTable)
	(modify-syntax-entry ?- "w" synTable)
	 
	synTable))

;; command to comment/uncomment text
(defun csdn-comment-dwim (arg)
  "Comment or uncomment current line or region in a smart way.
For detail, see `comment-dwim'."
  (interactive "*P")
  (require 'newcomment)
  (let (
        (comment-start "#") (comment-end "")
        )
    (comment-dwim arg)))


;; indentation function
;; indentation logic:
;;   the following keywords at the start of the line set the indentation level to 0: (anchors)
;;    rel, topo, inv, packetIn
;;   the following lines produce a partial indentation:
;;    'if' without a terminating 'then'
;;    'packetIn' without a terminating ->
;;   the following lines produce a full indentation
;;    '{'
;;    [inv|topo|rel]---:
;;    'forall---.'
;;    '->'
;;   the following reduce by a full indentation:
;;    '}'
;;   

(setq ind-width 4)
(setq half-width (/ ind-width 2))
(defun reduce-indent ()
  (progn
    (save-excursion
      (forward-line -1)
      (setq cur-indent (- (current-indentation) ind-width)))
    (if (< cur-indent 0) ; We can't indent past the left margin
	(setq cur-indent 0))))
(defun set-indent-as-curr ()
  (progn
    (setq cur-indent (current-indentation))
    (setq not-indented nil)))
(defun add-indent-of (arg)
  (progn
    (setq cur-indent (+ (current-indentation) arg)) ; Do the actual indenting
    (setq not-indented nil)))
(defun add-indent () 
  (add-indent-of ind-width))
(defun add-half-indent ()
  (add-indent-of half-width))

(defun csdn-indent-line ()
  "Indent current line as CSDN code"
  (interactive)
  (beginning-of-line)
  
  (if (or (bobp)
	  (looking-at "^[ \t]*\\(rel\\|type\\|func\\|init\\|topo\\|inv\\|trans\\|packetIn\\|import\\)[ \t]")) ; beginning of buffer, or anchor keyword
      (indent-line-to 0)    
    (let ((not-indented t) cur-indent)
      (if (looking-at "^[ \t]*#") ; indent comments same as the first following non comment line
	  (save-excursion
	    (while not-indented
	      (forward-line 1)
	      (if (not (looking-at "^[ \t]*#\\|^[ \t]*$"))
		  (set-indent-as-curr)
		(if (eobp)
		    (setq not-indented nil)))))
	(if (looking-at "^[ \t]*}") ; If the line we are looking at starts with a '}', we need to reduce the level of indentation
	    (reduce-indent)
	  (save-excursion
	    (progn
	      (setq is-var-line (looking-at "^var"))
	      (while not-indented ; Iterate backwards until we find an indentation hint
		(forward-line -1)
		(if (looking-at "^[ \t]*}") ; This hint indicates that we need to indent at the level of the '}' token
		    (set-indent-as-curr)	      
		  (if (looking-at "^[ \t]*\\(if.*then[ \t]*{\\|while.*do[ \t]*{\\|\\(inv\\|topo\\|trans\\).*:\\|\\(forall\\|exists\\).*\.\\|packetIn.*->\\)") ; This hint indicates that we need to indent an extra level
			(add-indent)
		    (if (looking-at "^[ \t]*if\\|^[ \t]*while\\|^[ \t]*packetIn\\|^[ \t]*\\(inv\\|rel\\|topo\\|if\\)\\|.*{") ; This hint indicates that we need to indent half a level
			(if is-var-line
			    (set-indent-as-curr)
			  (add-half-indent))
		      (if (bobp) ; we've reached the buffer start
			  (setq not-indented nil)))))))))
	(if cur-indent
	    (indent-line-to cur-indent)
	  (indent-line-to 0)))))) ; If we didn't see an indentation hint, indent to 0

;; define the mode
(define-derived-mode csdn-mode fundamental-mode
  "CSDN"
  "Major mode for editing CSDN files."
  :syntax-table csdn-syntax-table
  
  ;; code for syntax highlighting
  (setq font-lock-defaults '((csdn-font-lock-keywords)))

  ;; clear memory
  (setq csdn-keywords-regexp nil)
  (setq csdn-types-regexp nil)
  (setq csdn-constants-regexp nil)
  (setq csdn-events-regexp nil)	  
  (setq csdn-functions-regexp nil)

  ;; modify the keymap
  (define-key csdn-mode-map [remap comment-dwim] 'csdn-comment-dwim) 
  
  ;; indentation function to use in this mode
  (setq indent-line-function 'csdn-indent-line)
)

(provide 'csdn-mode)

;; csdn-mode.el ends here
