from z3 import *  # @UnusedWildImport

import ast
import copy
import time

from smt import SMT

class VC:

    class Stats:
        def __init__(self, smt):
            self.smt = smt # @@ ugly
            self.reset()
        def reset(self):
            self.specSize = 0
            self.specMaxQuantifierDepth = 0
            self.vcSize = 0
            self.vcMaxQuantifierDepth = 0
        def count_spec(self, invar):
            size, quantifierDepth = self.smt.expr_size(invar)
            self.specSize += size
            self.specMaxQuantifierDepth = max(self.specMaxQuantifierDepth,
                                              quantifierDepth)
        def count_vc(self, vc):
            size, quantifierDepth = self.smt.expr_size(vc)
            self.vcSize += size
            self.vcMaxQuantifierDepth = max(self.vcMaxQuantifierDepth,
                                            quantifierDepth)

    # Unique identifier (used to generate unique variable names)
    u_id = 0

    def next_id (self):
        res = self.u_id
        self.u_id += 1
        return res

    def __init__(self, sys_args):
        self.sys_args = sys_args
        # TODO: clean: remove if syntax_invs are not used
        self.syntax_invs = []
        self.syntax_init_topo_invs = []
        self.syntax_topo_invs = []
        self.syntax_trans_invs = []
        self.invs = []
        self.init_topo_invs = []
        self.topo_invs = []
        self.trans_invs = []
        self.global_preconditions = []
        self.initial = {}           # initial values of relations
        self.init = BoolVal(True)   # the initial state of network (defined by _generate_init_conditions)
        self.cmds = []              # list of commands in the controller program
        self.strength_cmds = []     # list of commands in the controller program used for strengthening
        self.decls = {}
        self.macro_defs = {}
        self.env = []

        # Built-in types
        self.builtin_types = {}
        if self.sys_args.sw:
            SW = EnumSort('SW', tuple("sw!SW!%s" % str(x) for x in range(self.sys_args.sw)))[0]
        else:
            SW = DeclareSort('SW')
        print SW
        if self.sys_args.pr:
            PR = EnumSort('PR', tuple("port!PR!%s" % str(x) for x in range(self.sys_args.pr)))[0]
        else:
            PR = DeclareSort('PR')
        print PR
        if self.sys_args.pk:
            PK = EnumSort('PK', tuple("pkt!PK!%s" % str(x) for x in range(self.sys_args.pk)))[0]
        else:
            PK = DeclareSort('PK')
        print PK
        if self.sys_args.ho:
            HO = EnumSort('HO', tuple("host!HO!%s" % str(x) for x in range(self.sys_args.ho)))[0]
        else:
            HO = DeclareSort('HO')
        print HO
        self.builtin_types.update({ 'SW' : SW })
        self.builtin_types.update({ 'PR' : PR })
        self.builtin_types.update({ 'PK' : PK })
        self.builtin_types.update({ 'HO' : HO })
        self.builtin_types.update({ 'Int'  : IntSort() })
        self.builtin_types.update({ 'Bool' : BoolSort() })
        BUF, (BUF_IN, BUF_OUT) = EnumSort('BUF', ('in', 'out'))
        self.buf_in = BUF_IN
        self.buf_out = BUF_OUT
        self.builtin_types.update({ 'BUF' : BUF })
        T = self.builtin_types

        # Built-in relations and functions
        self.src = Function('src', T["PK"], T["HO"])
        self.dst = Function('dst', T["PK"], T["HO"])
        self.port = Function('port', IntSort(), T["PR"])
        self.recv = Function('recv', T["SW"], T["PK"], T["PR"], BoolSort())     # recv-this relation

        # history of forwarded packets:
        if sys_args.trans:
            self.sent = Function('sent', T["SW"], T["PK"], T["PK"], T["PR"], T["PR"], BoolSort())
        else:
            self.sent = Function('sent', T["SW"], T["PK"], T["PR"], T["PR"], BoolSort())

        # flow table
        if sys_args.ft_prior:
            self.ft = Function('ft', IntSort(), T["SW"], T["PK"], T["PR"], T["PR"], BoolSort())
        elif sys_args.trans:
            self.ft = Function('ft', T["SW"], T["PK"], T["PK"], T["PR"], T["PR"], BoolSort())
        else:
            self.ft = Function('ft', T["SW"], T["PK"], T["PR"], T["PR"], BoolSort())

        # Topology functions and relations
        self.tph = Function('tph', T["SW"], T["PR"], T["HO"], BoolSort())              # topology of hosts
        self.tphf = Function('tphf', T["HO"], T["SW"])                                 # topology of hosts
        self.tps = Function('tps', T["SW"], T["PR"], T["PR"], T["SW"], BoolSort())     # topology of switches
        self.tpsf = Function('tpsf', T["SW"], T["PR"], T["SW"])                        # topology of switches
        self.tpss = Function('tpss', T["SW"], T["PR"], T["SW"])                        # topology of switches
        self.tpsp = Function('tpsp', T["SW"], T["PR"], T["PR"])                        # topology of switches
        self.link = Function('link', T["SW"], T["PR"], BoolSort())                     # topology of switches
        #self.forward = Function('forward', T["SW"], T["PK"], T["PR"], T["PR"], BoolSort())
        self.traffic = Function('traffic', T["PK"], T["SW"], T["PR"], T["BUF"], T["SW"], T["PR"], T["BUF"], BoolSort())
        self.tr_orig = Function('tr_orig', T["PK"], T["SW"], T["PR"], T["BUF"], BoolSort())
        self.null_port = Const('null', T["PR"])
        #self.null_switch = Const('snull', T["SW"])

        self.decls['src'] = self.src
        self.decls['dst'] = self.dst
        self.decls['port'] = self.port
        self.decls['sent'] = self.sent
        self.decls['recv'] = self.recv
        self.decls['ft'] = self.ft
        self.decls['tph'] = self.tph
        self.decls['tphf'] = self.tphf
        self.decls['tps'] = self.tps
        self.decls['tpsf'] = self.tpsf
        self.decls['tpss'] = self.tpss
        self.decls['tpsp'] = self.tpsp
        self.decls['link'] = self.link
        #self.decls['forward'] = self.forward
        self.decls['traffic'] = self.traffic
        self.decls['tr_orig'] = self.tr_orig

        # Built-in methods
        # self.methods['install'] = T["SW"]

        # Built-in events
        # Maps events to corresponding types of arguments
        self.event_prototypes = {ast.PacketInEvent: ["SW", "PK", "PR"],
                                 ast.PacketFlowEvent: ["SW", "PK", "PR", "PR"]}
        #self.env.append({})
        # TODO: move declaration to Null.sdn
        self.env.append({'null': self.null_port})
        self.env.append({'buf_in': self.buf_in})
        self.env.append({'buf_out': self.buf_out})
        #self.env.append({'null': self.null_port, 'snull': self.null_switch})
        self.smt = SMT(sys_args)
        self.stats = self.Stats(self.smt)
        # TODO: clean
        # Different ports are different
        #pv1, pv2, pv3 = [self.port(1), self.port(2), self.port(3)]
        #self.smt.background = And(pv1 != pv2 , pv2 != pv3, pv1 != pv3,
        #                          *[self.null_port != p for p in (pv1,pv2,pv3)])
        # Buffers
        #self.smt.background = And(self.buf_in != self.buf_out)
        self.env.append({'buf_in': self.buf_in})
        self.env.append({'buf_out': self.buf_out})

    def check_consistency_of_initial_state(self, invs=[]):
        conj = lambda l: And([I for _, _, I in l])
        topo = [ i for i in self.topo_invs if not i[1] ]
        s = Solver()
        print self.init
        print self.init_topo_invs
        print topo
        print self.global_preconditions
        print invs
        s.add(conj([self.init] + self.init_topo_invs + topo + self.global_preconditions + invs))
        r = s.check()
        if r == sat:
            msg = 'OK'
        elif r == unknown:
            msg = 'NOT KNOWN'
        else:
            msg = 'FAIL'
        print msg
        sys.stdout.flush()
        return msg

    def verify_initial_state(self, invs):
        print '===   verify the initial state   ==='
        conj = lambda l: And([I for _, _, I in l])
        # Filter out all the topo invariants with non-empty set of recv-parameters
        topo = [ i for i in self.topo_invs if not i[1] ]
        P = conj([self.init] + self.global_preconditions + topo + self.init_topo_invs)
        for inv_id, inv_rp, Q in invs:
            desc = "init %s" % inv_id
            print desc,
            sys.stdout.flush()
            if not inv_rp:
                self.smt.prove(desc, Implies(P, Q))
            else:
                print "SKIPPED"
            sys.stdout.flush()

    def solve_cmds(self, invs, I):
        conj = lambda l: And([I for _, _, I in l] or BoolVal(True))
        for head, wp in self.cmds:
            I_id, I_rp, I_f = I
            desc = "%s / %s" % (I_id, head)
            print '===   %s   ===' % desc
            sys.stdout.flush()
            if self.sys_args.verbose:
                print "computing VCs... ",
                sys.stdout.flush()
                start = time.clock()
            F, Faux, cmd_param = wp(I_f)
            subst = zip(I_rp, cmd_param)
            F = substitute(F, *subst)
            Faux = substitute(Faux, *subst)
            # TODO: perform the substitution in wp()
            topo = [ (tid, trp, substitute(tf, *zip(trp, cmd_param))) for tid, trp, tf in self.topo_invs ]
            glob = self.global_preconditions
            # TODO: improve: invs with recv-params
            P = conj(topo + glob + invs)
            Paux = conj(topo + glob)
            # TODO: perhaps, it makes sense to solve them separately
            vc = And(Implies(P, F), Implies(Paux, Faux))
            end = time.clock()
            if self.sys_args.verbose:
                print "done in %4.2f secs" % (end - start)
                sys.stdout.flush()
            r, m = self.smt.prove(desc, vc)
            if r != unsat:
                print "not proved"
                return False
        return True # success or failure

    def solve(self):
        def _strengthen_aux(Is, k, wp_closures):
            # TODO: Currently, we ignore aux vcs (for while-loops) for
            # strengthening. Should we take them into account?
            if k == 0:
                return Is
            Irec = _strengthen_aux(Is, k-1, wp_closures)
            # Apply wp and record its name in the history
            # (we maintain the history of applied wps for debugging purposes).
            # TODO: improve: allow recv-params in safety invariants
            return Is + [ (wp(fml), hist + [wp_id])
                            for fml, hist in Irec for wp_id, wp in wp_closures ]
        def _strengthen(inv, s, wp_closures, exclude_list):
            inv_id, inv_rp, inv_I = inv
            if inv_id in exclude_list:
                return (inv_id, inv_rp, [(inv_I, [])])
            else:
                return (inv_id, inv_rp, _strengthen_aux([(inv_I, [])], s, self.strength_cmds))
        conj = lambda l: And([I for _, _, I in l] or BoolVal(True))
        topo = self.topo_invs
        glob = self.global_preconditions
        s = max(0, self.sys_args.strength)
        #exclude_list = [ "correct_ft_access", "authServIsAuth" ]
        exclude_list = []
        while True:
            invs = self.invs
            # Strengthening: iterative wp application
            if s > 0:
                print "===   Strengthening invariants: %d iteration(s)   === " % s
                sys.stdout.flush()
                if self.sys_args.verbose:
                    start_s = time.clock()
                #invs = [ (inv_id, _strengthen(T, I, s, strength_cmds)) for inv_id, I in self.invs ]
                if self.sys_args.debug:
                    print "*** input invariants:"
                    print invs
                    sys.stdout.flush()
                # TODO: improve: allow recv-args in safety invariants
                invs = map(lambda I:_strengthen(I, s, self.strength_cmds, exclude_list), invs)
                # print strengthened invariants
                # TODO: use logging.debug()
                if self.sys_args.debug:
                    print "*** strengthened invariants:"
                    for i_id, i_rp, iis in invs:
                        print "id: " + i_id
                        print "rp: " + str(i_rp)
                        for f, h in iis:
                            print "h: " + str(h)
                            print "f: " + str(f)
                    sys.stdout.flush()
                # strip the histories
                invs = [ (inv_id, rp, And([I for I, _ in Is])) for inv_id, rp, Is in invs ]
                if self.sys_args.debug:
                    print "*** histories are stripped"
                    sys.stdout.flush()
                if self.sys_args.verbose:
                    end_s = time.clock()
                    print "done in %4.2f secs" % (end_s - start_s)
                else:
                    print "done"
                sys.stdout.flush()
            # Check the topology for consistency in the initial state
            print '===   initial state consistency   ==='
            if not self.sys_args.skip_init_check:
                self.check_consistency_of_initial_state(invs)
            # Prove invariants hold in the initial state.
            self.verify_initial_state(invs)
            # Prove invariants are preserved by all events
            done = True
            # TODO: verify topology invariants
            print '===   verifying safety invariants   ==='
            # TODO: improve: allow recv-params in safety invariants
            for I in invs:
                done = done and self.solve_cmds(invs, I)
                #if not done:
                #    break 
            print '===   verifying transition invariants   ==='
            for I in self.trans_invs:
                done = done and self.solve_cmds(invs, I)
                #if not done:
                #    break 
            s += 1
            if done or s > self.sys_args.strength_auto:
                break
        print "Spec Sum:", self.stats.specSize , "maxSpec:", self.stats.specMaxQuantifierDepth

    def close(self, vs, f):
        return f if not vs else ForAll(vs, f)

    def visit(self, d):
        # TODO: NSB - review: can we have more than one controller in a file?
        # then what do invariants and initial conditions refer to?
        # Support networks with multiple controllers?
        if isinstance(d, ast.Controller):
            for dd in d.decldefs:
                self.visit(dd)
            self.init = self._generate_init_conditions()
            for g in d.guarded_cmds:
                self.visit(g)
        elif isinstance(d, ast.Relation):
            sorts = [ self.builtin_types[s] for s in d.types] + [BoolSort()]
            self.decls[d.id] = Function(d.id, sorts)
        elif isinstance(d, ast.Function):
            sorts = [ self.builtin_types[s] for s in d.arg_types] + [self.builtin_types[d.ret_type]]
            self.decls[d.id] = Function(d.id, sorts)
        elif isinstance(d, ast.Macro):
            self.macro_defs[d.name] = d
        elif isinstance(d, ast.Type):
            self.builtin_types.update({d.type_id:DeclareSort(d.type_id)})
        elif isinstance(d, ast.TypedId):
            if d.id in self.decls:
                raise NameError, "name '%s' has already been defined (at %s)" % (d.id, d)
            self.env.append({d.id: Const(d.id, self.builtin_types[d.type])})
        elif isinstance(d, ast.InitRelation):
            if d.rel in self.initial:
                raise ValueError, "multiple initialization of '%s' encountered" % d.rel
            self.initial[d.rel] = self._init(self.decls[d.rel], d.preds)
        elif isinstance(d, ast.Invariant):
            # Create a frame of variables for recv-parameters
            frame = {}
            vs = []
            for p in d.recv_params:
                if p.id not in frame:
                    v = Const(p.id, self.builtin_types[p.type])
                    frame[p.id] = v
                    vs.append(v)
                else:
                    raise NameError, "name '%s' has already been defined (at %s)" % (v.id, v)
            # Add the frame to the environment in order for calling self.fml()
            self.env.append(frame)
            fml = self.fml(d.formula)
            new_inv = (d.id, vs, fml)
            # TODO: improve: stat does not take recv-params into account
            self.stats.count_spec(new_inv[2])
            self.env.pop()
            if isinstance(d, ast.InitialTopologyInvariant):
                self.syntax_init_topo_invs += [d]
                self.init_topo_invs += [new_inv]
            elif isinstance(d, ast.TopologyInvariant):
                self.syntax_topo_invs += [d]
                self.topo_invs += [new_inv]
            elif isinstance(d, ast.TransitionInvariant):
                self.syntax_trans_invs += [d]
                self.trans_invs += [new_inv]
            else:
                self.syntax_invs += [d]
                self.invs += [new_inv]
        elif isinstance(d, ast.Assume):
            self.global_preconditions += [('assume', [], self.fml(d.formula))]
        elif isinstance(d, ast.GuardedCmd):
            # At this point we know that all the invariants are parsed.
            head = "%s(%s)" % (type(d.event).__name__, ", ".join(unicode(a) for a in d.event.args))
            event_args = zip(d.event.args, self.event_prototypes[type(d.event)])
            def _name_id(s, i):
                return s + "$" + str(i)
            # Preprocess the list of commands for packetIn and packetFlow events
            #if isinstance(d.event, (ast.PacketInEvent, ast.PacketFlowEvent)):
            frame = { bound_value.exp.id: Const(bound_value.exp.id, self.builtin_types[tpe])
                      for bound_value, tpe in event_args
                      if isinstance(bound_value, ast.ExpTerm) and \
                         isinstance(bound_value.exp, ast.VarExp) } #TODO: improve
            # Convert event args to z3 terms
            self.env.append(frame)
            event_args = map(self.term, d.event.args)
            self.env.pop()
            # TODO: improve: Currently, strengthening only works correctly if all the
            # local variables are declared in the header of the event.
            for v in d.decls:
                if v.id not in frame:
                    frame[v.id] = Const(v.id, self.builtin_types[v.type])
                else:
                    raise NameError, "name '%s' has already been defined (at %s)" % (v.id, v)
            # Definition of wp-closure for vc generation
            def _wp(Q):
                # TODO: improve: why is inp passed as a separate param?
                self.env.append(frame)
                inp = self.term(d.event.port_in)
                f, f_aux = self.wps(d.cmds, Q, inp), self.vc_aux(d.cmds, Q, inp)
                self.env.pop()
                # Additionally, return current event arguments.
                return f, f_aux, event_args
            # Definition of wp-closure for invariant strengthening
            def _strength_wp(Q):
                ## TODO: needs refactoring (remove it?)
                #def _termToFormula(terms, ft_tuple):
                #    eqf = []
                #    if len(terms) != len(ft_tuple):
                #        raise TypeError, "wrong number of arguments (expected %d, got %d)" % (len(ft_tuple), len(terms))
                #    def conjunct(t, symbol):
                #        if isinstance(t, ast.ExpTerm):
                #            return self.exp(t.exp) == symbol
                #        if isinstance(t, ast.FuncTerm):
                #            return self.decls[t.fid](*(self.term(x) for x in t.terms)) == symbol
                #        else:
                #            raise NotImplementedError, t
                #    for term, symbol in zip(terms, ft_tuple):
                #        conj = conjunct(term, symbol)
                #        if conj is not None: eqf.append(conj)
                #    return And(eqf) if eqf else BoolVal(True)
                def _fresh_params(Q, frame):
                    uid = self.next_id()
                    subst_list = [ (frame[x], Const(_name_id(x, uid), frame[x].sort()))
                                        for x in frame.keys() ]
                    vs = [ x for _, x in subst_list ]
                    return (self.close(vs, substitute(Q, *subst_list)), vs)
                #print "strengthening round started"
                self.env.append(frame)
                inp = self.term(d.event.port_in)
                conj = lambda l: And(l) if l else BoolVal(True)
                # Process topological invariants:
                # - select relevant topo invariants with non-empty list of recv-parameters and
                # - substitute recv-parameters with event arguments
                topo_invs = [ substitute(tf, *zip(trp, event_args))
                                for tid, trp, tf in self.topo_invs if trp ]
                #print "selected topo_invs with recv-params: ",
                #print topo_invs
                T = conj(topo_invs)
                # Transition invariants
                # TODO: improve: replace recv-params if Trans is used
                #Trans = conj(self.trans_invs)
                ## Second, dont' forget transition invariants
                #fml = self.wps(cmds, And(Trans, Q), inp)
                # Now apply wp..
                fml = self.wps(d.cmds, Q, inp)
                # ..and add topology constraints.
                fml = Implies(T, fml)
                # Replace event parameters with fresh names and quantify them universally.
                fml = _fresh_params(fml, frame)
                # Finally, pop the current event frame and return.
                self.env.pop()
                # TODO: clean
                return fml[0]
            self.cmds.append((head, _wp))
            self.strength_cmds.append((head, _strength_wp))
        else:
            raise NotImplementedError(d)

    def _generate_init_conditions(self):
        """
        Generates formulas describing the initial state of the network.
        All the relations are valued according to their respective 'init'
        statement.
        Default value for the built-in relations 'sent', 'traffic', and 'ft'
        is empty.
        """
        relations = [ z3obj for _, z3obj in self.decls.iteritems()
                        if z3obj.range() == BoolSort() ]
        def preconditions():
            for r in relations:
                try:
                    yield self.initial[r.name()]
                except KeyError:
                    if r.name() in ['sent','ft']:
                        yield self._init(r, [])     # generate empty init
                    if r.name() in ['traffic']:
                        yield self._init_traffic(r) # generate empty init
                    if r.name() in ['tr_orig']:
                        yield self._init_traffic_orig(r) # generate empty init
                    else:
                        continue
        init_state = And(*preconditions())
        return ("initial state", [], init_state)

    def _generate_bound_vars(self, relation):
        r = relation
        return [Const("%s%d" % (r.domain(i), i), r.domain(i)) for i in xrange(r.arity())]

    def _init(self, r, preds):
        bvars = self._generate_bound_vars(r)
        situation = self.predToFormula(preds, bvars) if preds else BoolVal(False)
        return ForAll(bvars, r(bvars) == situation)

    def _init_traffic(self,r):
        bvars = self._generate_bound_vars(r)
        p,s,u,ub,t,v,vb = bvars
        return ForAll(bvars,
            self.traffic(bvars) ==
            Or(And([ s == t, u == v, ub == vb ]),
               And([ self.tps(s,u,v,t), ub == self.buf_out, vb == self.buf_in ])
            ))

    def _init_traffic_orig(self,r):
        bvars = self._generate_bound_vars(r)
        p,s,u,ub = bvars
        T = self.builtin_types
        t = Const('t', T["SW"])
        v = Const('v', T["PR"])
        return ForAll(bvars,
            self.tr_orig(bvars) == \
                    Or([
                        ub == self.buf_out,
                        And([
                            ub == self.buf_in,
                            ForAll([t,v], Not(self.tps(s,u,v,t)))
                        ])
                    ]))

    def wp(self, cmd, fml, inp):
        if isinstance(cmd, list):
            return self.wps(cmd, fml, inp)
        elif isinstance(cmd, ast.Block):
            frame = {}
            for v in cmd.decls:
                if v.id not in frame:
                    frame[v.id] = Const(v.id, self.builtin_types[v.type])
                else:
                    raise NameError, "name '%s' has already been defined (at %s)" % (v.id, v)
            self.env.append(frame)
            wp = self.wp(cmd.cmds, fml, inp)
            self.env.pop()
            return wp
        elif isinstance(cmd, ast.Assume):
            P = self.fml(cmd.formula)
            return Implies(P, fml)
        elif isinstance(cmd, ast.Assert):
            P = self.fml(cmd.formula)
            return And(P, fml)
        # TODO: remove it later if not needed
        ## Auxiliary event for processing of received packets
        #elif isinstance(cmd, ast.Recv):
        #    s = lambda *args: self.predToFormula(cmd.preds, args)
        #    return self.replace(self.recv, s, fml)
        elif isinstance(cmd, ast.Send):
            cmd_switch = self.exp(cmd.switch)
            cmd_packet = self.exp(cmd.packet)
            cmd_port_out = self.exp(cmd.port_out)
            s = lambda s, p, i, o: Or(self.sent(s, p, i, o), And(s == cmd_switch, p == cmd_packet, i == inp, o == cmd_port_out))
            return self.replace(self.sent, s, fml)
        elif isinstance(cmd, ast.SendTranslate):
            cmd_switch = self.exp(cmd.switch)
            cmd_packet = self.exp(cmd.packet)
            cmd_packet_translated = self.exp(cmd.packet_trans)
            cmd_port_out = self.exp(cmd.port_out)
            s = lambda s, p, pt, i, o: Or(self.sent(s, p, pt, i, o), And(s == cmd_switch, p == cmd_packet, pt == cmd_packet_translated, i == inp, o == cmd_port_out))
            return self.replace(self.sent, s, fml)
        elif isinstance(cmd, ast.Flood):
            cmd_switch = self.exp(cmd.switch)
            cmd_packet = self.exp(cmd.packet)
            s = lambda s, p, i, o: Or(self.sent(s, p, i, o), And(s == cmd_switch, p == cmd_packet, i == inp, o != inp))
            return self.replace(self.sent, s, fml)
        elif isinstance(cmd, ast.Insert):
            if cmd.rid == 'traffic':
                raise ValueError, "cannot insert into the traffic relation"
            if cmd.rid == 'tr_orig':
                raise ValueError, "cannot insert into the traffic relation"
            if cmd.rid == 'ft' and self.sys_args.traffic \
                               and not self.sys_args.ft_prior \
                               and not self.sys_args.trans:
                fml = self._generate_traffic_wp(cmd.preds, fml)
            rel_symb = self.decls[cmd.rid]
            f = lambda *args: self.predToFormula(cmd.preds, args)
            s = lambda *args: Or(rel_symb(*args), f(*args))
            return self.replace(rel_symb, s, fml)
        elif isinstance(cmd, ast.InsertElse):
            # Insert-Else can only be used with flow tables
            if cmd.rid != 'ft':
                raise ValueError, "can only insert 'else' into relation 'ft' (at %s)" % cmd
            if self.sys_args.ft_prior or self.sys_args.trans:
                raise NotImplementedError(cmd) #TODO
            ft = self.decls['ft']
            gamma = Const('gamma', self.builtin_types['PR'])
            cmd_sw, cmd_port_in, cmd_port_out = cmd.pred_sw, cmd.pred_port_in, cmd.pred_port_out
            f = lambda s,p,i,o: Or(ft(s,p,i,o),
                                   And(self.predToFormula((cmd_sw, ast.PredAny(), cmd_port_in, cmd_port_out),
                                                          (s,p,i,o)),
                                       Not(Exists([gamma], ft(s, p, i, gamma)))))
            return self.replace(ft, f, fml)
        elif isinstance(cmd, ast.Install):
            # Install can only be used with switches
            sw = self._lookup(cmd.sid)
            ft = self.decls['ft']
            preds = [ast.PredExp(ast.VarExp(cmd.sid))] + cmd.preds
            if self.sys_args.traffic \
                and not self.sys_args.ft_prior \
                and not self.sys_args.trans:
                fml = self._generate_traffic_wp(preds, fml)
            f = lambda *args: self.predToFormula(preds, args)
            s = lambda *args: Or(ft(*args), f(*args))
            return self.replace(ft, s, fml)
        elif isinstance(cmd, ast.Remove):
            rel_symb = self.decls[cmd.rid]
            f = lambda *args: self.predToFormula(cmd.preds, args)
            s = lambda *args: And(rel_symb(*args), Not(f(*args)))
            return self.replace(rel_symb, s, fml)
        elif isinstance(cmd, ast.Clear):
            rel_symb = self.decls[cmd.rid]
            f = lambda *args: self.predToFormula(cmd.preds, args)
            s = lambda *args: BoolVal(False)
            return self.replace(rel_symb, s, fml)
        elif isinstance(cmd, ast.IfThen):
            P = self.wp(cmd.then_cmd, fml, inp)
            Q = fml
            C = self.fml(cmd.cond)
            return If(C, P, Q)
        elif isinstance(cmd, ast.IfThenElse):
            P = self.wp(cmd.then_cmd, fml, inp)
            Q = self.wp(cmd.else_cmd, fml, inp)
            C = self.fml(cmd.cond)
            return If(C, P, Q)
        elif isinstance(cmd, ast.WhileDo):
            return self.fml(cmd.inv)
        elif isinstance(cmd, ast.ForEach):
            # Note: cmd.inv cannot refer to variables from cmd.typed_ids
            return self.fml(cmd.inv)
        elif isinstance(cmd, ast.Assgn):
            v = self._lookup(cmd.vid)
            s = lambda *args: self.term(cmd.term)
            return self.replace(v.decl(), s, fml)

        raise NotImplementedError(cmd)

    # Generate preconditions for the traffic relation
    def _generate_traffic_wp(self, preds, fml):
        tr = self.decls['traffic']
        tr_orig = self.decls['tr_orig']
        #tpss = self.decls['tpss']
        #tpsp = self.decls['tpsp']
        def extr (p):
            if isinstance(p, ast.PredExp):
                return self.exp (p.exp)
            else:
                None
                #raise TypeError, "traffic relation cannot be used with patterns yet"
        sw, pk, port_in, port_out = \
            extr(preds[0]), extr(preds[1]), extr(preds[2]), extr(preds[3])
        if port_in:
            f_tr = lambda p, a, a_port, a_buf, b, b_port, b_buf: \
                    And(
                        Implies (self.predToFormula([preds[1]], [p]),
                          Or (tr(p, a, a_port, a_buf, b, b_port, b_buf),
                              And(tr(p, a, a_port, a_buf, sw, port_in, self.buf_in),
                                  tr(p, sw, port_out, self.buf_out, b, b_port, b_buf))
                             )
                        ),
                        Implies (Not(self.predToFormula([preds[1]], [p])),
                                 tr(p, a, a_port, a_buf, b, b_port, b_buf))
                    )
        else:
            PR = self.builtin_types['PR']
            ports = [ PR.constructor(x)() for x in range(PR.num_constructors()) ]
            f_tr = lambda p, a, a_port, a_buf, b, b_port, b_buf: \
                    And(
                        Implies (self.predToFormula([preds[1]], [p]),
                            And([
                                #And([ Not(tr (p, port_out, buf_out, x, buf_in)) for x in ports ]),
                                Or (tr(p, a, a_port, a_buf, b, b_port, b_buf),
                                   Or([ And(tr(p, a, a_port, a_buf, sw, x, self.buf_in),
                                            tr(p, sw, port_out, self.buf_out, b, b_port, b_buf))
                                        for x in ports ])
                                )
                            ])
                        ),
                        Implies (Not(self.predToFormula([preds[1]], [p])),
                                 tr(p, a, a_port, a_buf, b, b_port, b_buf))
                    )
        f_tr_orig = lambda p, a, a_port, a_buf: \
                And([
                    tr_orig(p, a, a_port, a_buf),
                    Not(And([
                        self.predToFormula([preds[1]], [p]),
                        a      == sw,
                        a_port == port_out,
                        a_buf  == self.buf_out
                    ]))
                ])
        #print "TRAFFIC"
        #print "TRAFFIC"
        #print "TRAFFIC"
        #print "TR_ORIG"
        #print fml
        fml = self.replace(tr_orig, f_tr_orig, fml)
        #print fml
        fml = self.replace(tr, f_tr, fml)
        return fml

    def wps(self, cmds, fml, inp):
        i = len(cmds) - 1
        while i >= 0:
            fml = self.wp(cmds[i], fml, inp)
            i -= 1
        return fml

    def _vc_aux_cmd(self, cmd, fml, inp):
        if isinstance(cmd, list):
            return self._vc_aux(cmd, fml, inp)
        elif isinstance(cmd, ast.Block):
            frame = {}
            for v in cmd.decls:
                if v.id not in frame:
                    frame[v.id] = Const(v.id, self.builtin_types[v.type])
                else:
                    raise NameError, "name '%s' has already been defined (at %s)" % (v.id, v)
            self.env.append(frame)
            wps = self._vc_aux_cmd(cmd.cmds, fml, inp)
            self.env.pop()
            return wps
        elif isinstance(cmd, ast.Assume):
            return []
        elif isinstance(cmd, ast.Assert):
            return []
        # TODO: remove it later if not needed
        ## Auxiliary event for processing of received packets
        #elif isinstance(cmd, ast.Recv):
        #    return []
        elif isinstance(cmd, ast.Send):
            return []
        elif isinstance(cmd, ast.SendTranslate):
            return []
        elif isinstance(cmd, ast.Flood):
            return []
        elif isinstance(cmd, ast.Insert):
            return []
        elif isinstance(cmd, ast.InsertElse):
            return []
        elif isinstance(cmd, ast.Install):
            return []
        elif isinstance(cmd, ast.Remove):
            return []
        elif isinstance(cmd, ast.Clear):
            return []
        elif isinstance(cmd, ast.IfThen):
            return self._vc_aux_cmd (cmd.then_cmd, fml, inp)
        elif isinstance(cmd, ast.IfThenElse):
            return self._vc_aux_cmd(cmd.then_cmd, fml, inp) + \
                   self._vc_aux_cmd(cmd.else_cmd, fml, inp)
        elif isinstance(cmd, ast.WhileDo):
            I = self.fml(cmd.inv)
            C = self.fml(cmd.cond)
            return self._vc_aux_cmd(cmd.cmd, I, inp) + \
                   [ Implies(And(I, C), self.wp(cmd.cmd, I, inp)),
                     Implies(And(I, Not(C)), fml) ]
        elif isinstance(cmd, ast.ForEach):
            frame = {}
            for v in cmd.typed_vars:
                if v.id not in frame:
                    frame[v.id] = Const(v.id, self.builtin_types[v.type])
                else:
                    raise NameError, "name '%s' has already been defined (at %s)" % (v.id, v)
            self.env.append(frame)
            I = self.fml(cmd.inv)
            C = self.fml(cmd.cond)
            conj = lambda l: And(l) if l else BoolVal(True)
            vcs = self._vc_aux_cmd(cmd.cmd, I, inp) + \
                  [ Implies(And(I, C), self.wp(cmd.cmd, I, inp)) ]
            vs = self.env.pop().values()
            return [ForAll(vs, conj(vcs))]
        elif isinstance(cmd, ast.Assgn):
            return []
        raise NotImplementedError(cmd)

    def _vc_aux(self, cmds, fml, inp):
        i = len(cmds) - 1
        accu = []
        while i >= 0:
            accu.extend (self._vc_aux_cmd(cmds[i], fml, inp))
            fml = self.wp(cmds[i], fml, inp)
            i -= 1
        return accu

    # Generate auxiliary verification conditions for While-loops
    def vc_aux(self, cmds, fml, inp):
        conj = lambda l: And(l) if l else BoolVal(True)
        return conj(self._vc_aux(cmds, fml, inp))

    # Transform the AST-formula to Z3-formula
    def fml(self, f, subst={}):
        # TODO: Alex: subst argument is never used?
        fml = lambda x: self.fml(x, subst)
        if isinstance(f, ast.TrueFOL):
            return BoolVal(True)
        if isinstance(f, ast.FalseFOL):
            return BoolVal(False)
        if isinstance(f, (tuple, list)):
            args = [self.term(t) for t in f.terms]
            fdecl = self.decls[f.id]
            return fdecl(args)
        if isinstance(f, ast.EqualityFOL):
            return self.term(f.term1) == self.term(f.term2)
        if isinstance(f, ast.LtFOL):
            return self.term(f.term1) < self.term(f.term2)
        if isinstance(f, ast.LeFOL):
            return self.term(f.term1) <= self.term(f.term2)
        if isinstance(f, ast.GtFOL):
            return self.term(f.term1) > self.term(f.term2)
        if isinstance(f, ast.GeFOL):
            return self.term(f.term1) >= self.term(f.term2)
        elif isinstance(f, ast.QuantifiedFOL):
            c = [Const(var.id, self.builtin_types[var.type]) for var in f.typed_vars]
            self.env += [{var.decl().name(): var for var in c}]
            subf = self.fml(f.body, subst)
            self.env.pop()
            if isinstance(f, ast.ForAll):
                return ForAll(c, subf)
            else:
                return Exists(c, subf)
        elif isinstance(f, ast.NegateFOL):
            return Not(fml(f.alpha))
        elif isinstance(f, ast.ConjoinFOL):
            return And(fml(f.fol1), fml(f.fol2))
        elif isinstance(f, ast.ConjoinListFOL):
            conj = lambda l: And(l) if l else BoolVal(True)
            return conj([fml(x) for x in f.fols])
        elif isinstance(f, ast.IffFOL):
            return fml(f.fol1) == fml(f.fol2)
        elif isinstance(f, ast.DisjoinFOL):
            return Or(fml(f.fol1), fml(f.fol2))
        elif isinstance(f, ast.AtomicFOL):
            if f.rel in self.macro_defs:
                return fml(f.expansion)
            else:
                return self.decls[f.rel](*(self.term(x) for x in f.terms))
        elif isinstance(f, ast.MethodFOL):
            if (f.mid == 'empty'):
                r = self.decls[f.rid]
                vs = [Const(''.join(['v_', r.domain(i).name(), '_', str(self.next_id())]), r.domain(i))
                        for i in range(0, r.arity())]
                return ForAll(vs, Not(r(vs)))
            else:
                raise NotImplementedError, f.mid
        print f
        raise Z3Exception ("TBD %s" % f)
        return None

    # Transform the AST-term to Z3-term
    def term(self, t):
        if isinstance(t, ast.ExpTerm):
            return self.exp(t.exp)
        if isinstance(t, ast.FuncTerm):
            return self.decls[t.fid](*(self.term(x) for x in t.terms))
        else:
            raise NotImplementedError, t

    # Transform the AST-expression to Z3-expression
    def exp(self, e):
        if isinstance(e, ast.NumericConstExp):
            return IntVal(e.const)
        if isinstance(e, ast.TrueConstExp):
            return BoolVal(True)
        if isinstance(e, ast.FalseConstExp):
            return BoolVal(False)
        elif isinstance(e, ast.VarExp):
            return self._lookup(e.id)
        elif isinstance(e, ast.PropertyVarExp):
            return self.decls[e.fid](self._lookup(e.aid))
        elif isinstance(e, ast.PortExp):
            return self.decls['port'](self.exp(e.port_number))
        else:
            raise NotImplementedError, e

    def _lookup(self, name):
        for frame in reversed(self.env):
            try:
                return frame[name]
            except KeyError:
                continue
        else:
            raise Z3Exception ("Variable not declared:  '%s'" % name)

    #
    # Replace f(children) in e by g(children)
    #
    def replace(self, f, g, e):
        if is_app(e):
            children = [ self.replace(f, g, c) for c in e.children() ]
            if self._get_decl_id(e.decl()) == self._get_decl_id(f):
                return g(*children)
            if is_and(e):
                return And(children)
            if is_or(e):
                return Or(children)
            return e.decl()(children)
        if is_quantifier(e):
            vars = [ Const(e.var_name(i), e.var_sort(i)) for i in range(e.num_vars())]  # @ReservedAssignment
            #vars.reverse()
            new_body = self.replace(f, g, e.body())
            if e.is_forall():
                return ForAll(vars, new_body)
            else:
                return Exists(vars, new_body)
        return e

    def _get_decl_id(self, d):
        return Z3_get_ast_id(d.ctx_ref(), d.as_ast())

    # # From predicates to conjunction of equalities
    # # dict : string -> Ast
    # #
    def predToFormula(self, preds, ft_tuple):
        f = []
        if len(preds) != len(ft_tuple):
            raise TypeError, "wrong number of arguments (expected %d, got %d)" % (len(ft_tuple), len(preds))
        def conjunct(pred, symbol):
            if isinstance(pred, ast.PredExp):
                return self.exp(pred.exp) == symbol
            elif isinstance(pred, ast.PredFidExp):
                fld = self.decls[pred.fid]
                return self.exp(pred.exp) == fld(symbol)
            elif isinstance(pred, ast.ConjoinPred):
                c1 = conjunct (pred.p1, symbol)
                c2 = conjunct (pred.p2, symbol)
                cs = [x for x in [c1,c2] if x is not None]
                return And(cs) if cs else None
            elif isinstance(pred, ast.PredAny):
                return None
            elif isinstance(pred, ast.NegateFOL):
                return Not(conjunct(pred.alpha, symbol))
            else:
                raise NotImplementedError, pred
        for pred, symbol in zip(preds, ft_tuple):
            conj = conjunct(pred, symbol)
            if conj is not None: f.append(conj)
        return And(f) if f else BoolVal(True)


class Preprocess(object):

    def add_flow_event(self, ast_controller, sys_args):
        """
        # Flow tables with priorities:
        packetFlow(s, p, in, out) ->
           var mpr : Int
           assume ft(mpr, s, p, in, out)
           assume forall pr1, out1 . ft(pr1, s, p, in, out1) => pr1 < mpr
           send(s, p, out)

        # Flow tables with translations:
        packetFlow(s, p, p_tr, in, out) ->
           assume ft(s, p, p', in, out)
           send(s, p, p_tr, out)

        # Regular flow tables:
        packetFlow(s, p, in, out) ->
           assume ft(s, p, in, out)
           send(s, p, out)
        """
        etve = lambda vid: ast.ExpTerm(ast.VarExp(vid))
        ve = ast.VarExp
        ti = ast.TypedId
        flow_event = ast.PacketFlowEvent(etve('s'), etve('p'), etve('in'), etve('out'))
        if sys_args.ft_prior:
            flow_decls = [ti('mpr','Int')]
            flow_cmd = [ast.Assume(ast.AtomicFOL('ft', [etve('mpr'), etve('s'), etve('p'), etve('in'), etve('out')])),
                        ast.Assume(
                            ast.ForAll([ti('pr1','Int'), ti('out1','PR')],
                                ast.DisjoinFOL(
                                    ast.NegateFOL(
                                        ast.AtomicFOL('ft', [etve('pr1'), etve('s'), etve('p'), etve('in'), etve('out1')])),
                                    ast.LeFOL(etve('pr1'), etve('mpr'))))),
                        ast.Send(ve('s'), ve('p'), ve('out'))]
        elif sys_args.trans:
            flow_decls = [ti('p_tr','PK')]
            flow_cmd = [ast.Assume(ast.AtomicFOL('ft', [etve('s'), etve('p'), etve('p_tr'), etve('in'), etve('out')])),
                        ast.SendTranslate(ve('s'), ve('p'), ve('p_tr'), ve('out'))]
        else:
            flow_decls = []
            flow_cmd = [ast.Assume(ast.AtomicFOL('ft', [etve('s'), etve('p'), etve('in'), etve('out')])),
                        ast.Send(ve('s'), ve('p'), ve('out'))]
        ast_controller.guarded_cmds += [ast.GuardedCmd(flow_event, flow_decls, flow_cmd)]

    def add_topology_constraint(self, ast_controller):
        """
        For both packetIn and packetFlow events, we add an assumption that the
        packet was received.
        Additionally, for packetIn events, we assume that the packet is not in
        the flow table.
        """
        etve = lambda vid: ast.ExpTerm(ast.VarExp(vid))
        #ve = ast.VarExp
        #et = ast.ExpTerm
        ti = ast.TypedId
        for gcmd in ast_controller.guarded_cmds:
            # Assume there is not matching rule in flow tables for packetIn events.
            # Should be disabled for the modelling of non-atomic rule installations.
            if isinstance(gcmd.event, ast.PacketInEvent):
                s, p, i = gcmd.event.switch, gcmd.event.packet, gcmd.event.port_in
                # assume forall o : PR. !ft(s, p, i, o)
                o = etve('o$')
                q = [ ti(o.exp.id, 'PR') ]
                assume = ast.Assume(ast.ForAll(q, ast.NegateFOL(ast.AtomicFOL('ft', [s, p, i, o]))))
                gcmd.cmds.insert(0, assume)
            if isinstance(gcmd.event, (ast.PacketInEvent, ast.PacketFlowEvent)):
                s, p, i = gcmd.event.switch, gcmd.event.packet, gcmd.event.port_in
                s_, p_, i_ = etve('s$'), etve('p$'), etve('i$')
                typed_spe_ = [ ti(s_.exp.id, 'SW'), ti(p_.exp.id, 'PK'), ti(i_.exp.id, 'PR') ]
                # assume forall s' : SW, p' : PK, i' : PR.
                #    recv(s', p', i') <=> s' = s && p' == p && i' == i
                assume = ast.Assume(ast.ForAll(typed_spe_,
                        ast.IffFOL(
                            ast.AtomicFOL('recv', [s_, p_, i_]),
                            ast.ConjoinFOL(
                                ast.ConjoinFOL(ast.EqualityFOL(s_, s),
                                               ast.EqualityFOL(p_, p)),
                                               ast.EqualityFOL(i_, i))
                            )))
                gcmd.cmds.insert(0, assume)

    def __call__(self, ast_controller, sys_args):
        self.add_flow_event(ast_controller, sys_args)
        self.add_topology_constraint(ast_controller)
        return ast_controller
