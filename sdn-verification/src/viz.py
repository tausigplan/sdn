import os
import pygraphviz


class NetworkViz(object):
    """
    Employs pygraphviz to depict structures describing states of the network.
    """

    BUILTIN_RELATIONS = ["tph", "tps", "tps+", "tps*", "path", "recv", "ft", "traffic"]

    def __init__(self, sys_args, desc):
        self.sys_args = sys_args
        self.desc = desc

    def depict_hosts_and_switches(self, sigma, m):
        """ should go to a separate class """
        SW, PR, PK, HO = m.domain['SW'], m.domain['PR'], m.domain['PK'], m.domain['HO']
        try:
            tph = m.interpretation['tph']
        except KeyError:
            tph = lambda s, p, h: False
        try:
            tps = m.interpretation['tps']
        except KeyError:
            tps = lambda s1, p1, p2, s2: False
        g = pygraphviz.AGraph(strict=False)
        def add_node(n, **attrs):
            g.add_node(n, **attrs)
            return n
        def portname(i): return i.split(':')[-1]
        PR_labels = " | ".join("<%s>%s" % (portname(x),portname(x)) for x in PR)
        nodes = {s: add_node(s, shape="Mdiamond", regular="true") for s in SW}
        nodes.update({(s, 'PR'): add_node(s+"/PR", shape="record", label=PR_labels) for s in SW})
        # Draw switches and hosts
        for s in SW:
            g.add_edge(nodes[s], nodes[(s,'PR')])
            for i in PR:
                for h in HO:
                    n = add_node(h, shape="square")
                    if tph(s, i, h):
                        g.add_edge(nodes[(s,'PR')], n, tailport=portname(i))
                for o in PR:
                    for t in SW:
                        if tps(s, i, o, t):
                            g.add_edge(nodes[(s,('PR'))], nodes[(t,('PR'))], 
                                       tailport=portname(i), headport=portname(o))
        # Draw packets
        for p in PK:
            nodes[p] = add_node(p, shape="record", label=self._packet_fmt(m, p))
        # Draw the flow table
        if "ft" in m.interpretation:
            for s in SW:
                ft_this = set(tup[1:] for tup in m.interpretation['ft'] if tup[0] == s)
                ft_this = self._simplify_ft(ft_this, [PK, PR, PR])
                nodes['%s/ft' % s] = ft_node = \
                    add_node('%s/ft' % s, shape='record', label=self._fmt_relation(ft_this, title="flow-table"))
                g.add_edge(nodes[s], ft_node)
        # Draw the traffic table
        if "traffic" in m.interpretation:
            edges = self._simplify_traffic(m.interpretation['traffic'])
            for p in PK:
                if p in edges:
                    nodes['%s/traffic' % p] = tr_node = \
                        add_node('%s/traffic' % p, shape='record', label=self._fmt_relation(set(edges[p]), title="traffic-table"))
                    g.add_edge(nodes[p], tr_node)
        # Draw other relations
        for r, _ in sigma.preds:
            if r not in self.BUILTIN_RELATIONS and '!' not in r.literal:
                rm = m.interpretation[r]
                nodes[r] = add_node(r.literal, shape='record', label=self._fmt_relation(rm, title=r.literal))
        # Add constant labels
        for symbol, value in m.interpretation.iteritems():
            if sigma.sorts.is_const(symbol) and not self._is_hidden_symbol(symbol):
                g.add_node(symbol, shape="plaintext")
                g.add_edge(symbol, value, dir="forward")
        # Print the counterexample
        os.environ["PATH"] = "/opt/local/bin:" + os.environ["PATH"]
        print g.string()
        g.layout(prog="dot")
        desc = "".join(x if x.isalnum() else "_" for x in self.desc)
        filename = "%s/%s.png" % (self.sys_args.output, desc)
        g.draw(filename)

    def _packet_fmt(self, model, packet):
        I = model.interpretation
        emp = lambda x: '?'
        src = I.get('src', emp)(packet)
        dst = I.get('dst', emp)(packet)
        return "%s | src=%s | dst=%s" % (packet, src, dst)

    def _fmt_relation(self, r, title):
        if r and not isinstance(iter(r).next(), (list, tuple)):
            r = [[x] for x in r]
        brec = lambda x: "{%s}" % "|".join(x)
        return brec([title] + ([brec(x) for x in r] or ["(empty)"]))

    def _simplify_traffic(self, tt):
        p_to_entries = {}
        for e in tt:
            p, rest = e[0], e[1:]
            p_to_entries[p] = p_to_entries.get(p, []) + [rest]
        edges = {}
        for p in p_to_entries:
            entries = p_to_entries[p]
            dep = {}
            for e in entries:
                u, v = e[:3], e[3:]
                dep[v] = dep.get(v, set()) | set([u])
            levels = list(self.toposort2(dep))
            # Correct on the assumption that 'traffic' is a linear relation:
            # forall x,y,z. R(x,y) & R(x,z) -> R(y,z) | R(z,y)
            if len(levels) == 1:
                continue
            for i in range(len(levels)):
                l = levels[i]
                for c in l:
                    found_successor = False
                    for j in range(i+1, len(levels)):
                        lnext = levels[j]
                        for cnext in lnext:
                            e = c + cnext
                            if e in entries:
                                found_successor = True
                                edges[p] = edges.get(p, []) + [e]
                            if found_successor:
                                break
                        if found_successor:
                            break
        return edges

    def _simplify_ft(self, ft, domains):
        tup_replace = lambda tup, idx, val: tup[:idx] + (val,) + tup[idx+1:]
        if len(ft) > 0:
            # This is somewhat dirty...
            arity = len(iter(ft).next())
            for idx in [arity-1,arity-2]:
                remove = set() ; add = set()
                for tup in ft:
                    if all(tup_replace(tup, idx, o) in ft for o in domains[idx]):
                        remove.add(tup)
                        add.add(tup_replace(tup, idx, "*"))
                ft.difference_update(remove)
                ft.update(add)
        return ft

    def _is_hidden_symbol(self, symbol):
        return symbol == 'null' or '!' in symbol.literal

    @classmethod
    def toposort2(cls, data):
        """Dependencies are expressed as a dictionary whose keys are items
    and whose values are a set of dependent items. Output is a list of
    sets in topological order. The first set consists of items with no
    dependences, each subsequent set consists of items that depend upon
    items in the preceeding sets.

    >>> print '\\n'.join(repr(sorted(x)) for x in toposort2({
    ...     2: set([11]),
    ...     9: set([11,8]),
    ...     10: set([11,3]),
    ...     11: set([7,5]),
    ...     8: set([7,3]),
    ...     }) )
    [3, 5, 7]
    [8, 11]
    [2, 9, 10]

    """

        from functools import reduce

        # Ignore self dependencies.
        for k, v in data.items():
            v.discard(k)
        # Find all items that don't depend on anything.
        extra_items_in_deps = reduce(set.union, data.itervalues()) - set(data.iterkeys())
        # Add empty dependences where needed
        data.update({item:set() for item in extra_items_in_deps})
        while True:
            ordered = set(item for item, dep in data.iteritems() if not dep)
            if not ordered:
                break
            yield ordered
            data = {item: (dep - ordered)
                    for item, dep in data.iteritems()
                        if item not in ordered}
        assert not data, "Cyclic dependencies exist among these items:\n%s" % '\n'.join(repr(x) for x in data.iteritems())

