# encoding=utf-8
import logging
import os
import yacc
import argparse
import sys

from vc import VC, Preprocess


logging.basicConfig(
    level = logging.DEBUG,
    filename = "parselog.txt",
    filemode = "w",
    format = "%(filename)10s:%(lineno)4d:%(message)s"
)

def csdn_library():
    import os.path
    here = os.path.dirname(__file__)
    fn = os.path.join(here, "..", "lib", "network.sdn")
    return open(fn).read() + "\n"


class CircularImportError(Exception):
    def __init__(self, orig):
        self.orig = orig

    def __str__(self):
        return "Circular import dependency when importing: " + self.orig


def parse_prog(args, log):
    program = open(args.filename).read()

    # retain current working directory
    original_wd = os.path.dirname(os.path.realpath(__file__))
    # change the working directory to the one of the currently opened file, so that relative paths will work
    os.chdir(os.path.dirname(os.path.realpath(args.filename)))

    prog_ast = yacc.parser.parse(program, tracking=True, debug=log)
    prog_ast = Preprocess()(prog_ast, args)

    asts = [prog_ast]
    file_imports = [os.path.realpath(f) for f in prog_ast.imports]


    #Map the previously parsed files to their ASTs
    fdict = {}

    # Go over all the imports
    while file_imports is not None and len(file_imports) != 0:
        yacc.lexer.lineno = 1
        current_fname = file_imports.pop()
        if current_fname in fdict: # Was previously parsed
            cur_ast = fdict[current_fname]
            asts.remove(cur_ast)    # remove it from the AST list
            asts.insert(0, cur_ast) # and replace it at the start
            file_imports.extend([os.path.realpath(f) for f in cur_ast.imports]) # and make sure that its dependancies are also processed
        else: # need to parse the file
            f = open(current_fname).read()
            os.chdir(os.path.dirname(os.path.realpath(current_fname)))
            imp_ast = yacc.parser.parse(f)
            asts.insert(0, imp_ast)
            fdict[current_fname] = imp_ast

            # Add dependencies, and look for circular dependencies
            fname_stack = [current_fname] # start with the current file
            while len(fname_stack) != 0 : # go over all the dependencies
                cur_elem = fname_stack.pop()
                if cur_elem in fdict : # was parsed before
                    fname_stack.extend([os.path.realpath(f) for f in fdict[cur_elem].imports]) # add its dependencies to the check
                if current_fname in fname_stack : # the current file is a dependency for something in it's own dependency hierarchy
                    raise CircularImportError(current_fname) # which is not allowed
            file_imports.extend([os.path.realpath(f) for f in imp_ast.imports]) # add all other dependencies.
    # restore the original working directory
    os.chdir(original_wd)
    return asts

def solve(args):
    library = csdn_library()

    log = logging.getLogger()
    lib_ast = yacc.parser.parse(library, tracking = True, debug = log)

    # Reset line number tracking for the program file;
    # Otherwise parsing errors are reported with a line number offset by the number of lines in the library.
    yacc.lexer.lineno = 1

    asts = parse_prog(args, log)

    vcgen = VC(args)
    vcgen.visit(lib_ast)
    vcgen.stats.reset()

    for ast in asts:
        vcgen.visit(ast)

    r = 'OK'
    if not args.skip_init_check:
        print '===   initial state consistency (topology and global assumptions only)   ==='
        r = vcgen.check_consistency_of_initial_state()
    # if constraints are consistent, proceed..
    if r == 'OK' and not args.only_init_check:
        vcgen.solve()

def check_options(args):
    if args.ft_prior and args.trans:
        print "The options --translation and --ft-prior are not compatible."
        return False
    else:
        if args.debug:
            args.verbose = True
        return True

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf8')  # @UndefinedVariable

    # Parse command-line arguments
    a = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description = '''VeriCon: a tool for verification of SDN controller programs''')
    a.add_argument('-v', '--verbose', action='store_true',
            help = 'verbose')
    a.add_argument('--debug', action='store_true',
            help = 'print debug messages')
    a.add_argument('-g', '--graph', action='store_true',
            help = 'draw graphs for counterexamples')
    a.add_argument('-d', '--dump', action='store_true',
            help = 'dump verification conditions as SMTLIB2 benchmarks')
    a.add_argument('-s', '--stat', action='store_true',
            help = 'print statistics')
    a.add_argument('-m', '--memory', type=int, metavar='NUM', default=0,
            help = 'limit amount of memory used by the solver (in megabytes)')
    a.add_argument('--skip-init-check', action='store_true',
            help = 'do not check the initial state for consistency')
    a.add_argument('--only-init-check', action='store_true',
            help = 'only check the initial state for consistency and terminate')
    a.add_argument('--sw', type=int, metavar='SW', default=0,
            help = 'bound the number of switches'),
    a.add_argument('--pr', type=int, metavar='PR', default=0,
            help = 'bound the number of ports'),
    a.add_argument('--ho', type=int, metavar='HO', default=0,
            help = 'bound the number of hosts (addresses)'),
    a.add_argument('--pk', type=int, metavar='PK', default=0,
            help = 'bound the number of packets'),
    a.add_argument('--traffic', action='store_true',
            help = 'generate preconditions for the traffic relation')
    a.add_argument('--ft-prior', action='store_true',
            help = 'use flow tables with priorities (no traffic relation yet supported)')
    a.add_argument('--trans', action='store_true',
            help = 'allow translation of packets on switches')
    a.add_argument('--timeout', type=int, metavar='NUM', default=0,
            help = 'set timeout on SMT calls. If strengthening is enabled, then proceed to next level on a timeout')
    a.add_argument('--strength', type=int, metavar='NUM', default=0,
            help = argparse.SUPPRESS
            # help = 'strengthening of invariants, number of iterations (default: %(default)s)'
    )
    a.add_argument('--strength-auto', type=int, metavar='NUM', default=0,
            help = argparse.SUPPRESS
            # help = 'try automatic strengthening of invariants, maximum number of iterations (default: %(default)s)'
    )
    a.add_argument('filename', type=str, metavar='FILE',
            help = 'input CSDN controller program')
    a.add_argument('-o', '--output', type=str, metavar='DIR', action='store', default='.',
            help = 'output path, create if does not exist (default: %(default)s)')
    args = a.parse_args()

    # Check compatibility of command-line options
    if not check_options(args): sys.exit()

    #import profile
    #profile.run('solve(args)', sort=1)

    # Verify the program
    solve(args)

