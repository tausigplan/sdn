

class AST(object):
    def __str__(self):
        d = "{%s}" % ', '.join( "%s: %s" % (k,v) for k,v in self.__dict__.iteritems())
        return "%s %s" % (type(self).__name__, d)

    __repr__ = __str__


#The controller program
class Controller(AST):
    def __init__(self,imports,dds,gcs):
        self.imports = imports
        self.decldefs = dds
        self.guarded_cmds = gcs

# Class for typed variables
class TypedId(AST):
    def __init__(self, id, ty):  # @ReservedAssignment
        self.id = id
        self.type = ty

class DeclDef(AST): pass

# Class of relations
class Relation(DeclDef):
    def __init__(self, rid, types):
        self.id = rid
        self.types = types

# Class of functions
class Function(DeclDef):
    def __init__(self, fid, arg_types, ret_type):
        self.id = fid
        self.arg_types = arg_types
        self.ret_type = ret_type

# Macro definition
class Macro(DeclDef):
    def __init__(self, name, typed_args, expansion):
        self.name = name
        self.typed_args = typed_args
        self.expansion = expansion

# Type declaration
class Type(DeclDef):
    def __init__(self,type_id):
        self.type_id = type_id

#TODO: rel -> rid
# Initialize relation
class InitRelation(DeclDef):
    def __init__(self, rel, preds):
        self.rel = rel        # relation id
        self.preds = preds    # initial values of components

# Class of partial maps
#class Map(DeclDef):
#    def __init__(self,id,types):
#        self.id = id
#        self.types = types
#        self.outtype = type

#TODO: mapping initialization?
# Currently maps are initialized by empty


class Invariant(DeclDef):
    def __init__(self, id, params, fol):  # @ReservedAssignment
        self.id = id
        self.recv_params = params
        self.formula = fol

class TopologyInvariant(Invariant): pass

class InitialTopologyInvariant(Invariant): pass

class TransitionInvariant(Invariant): pass


class GuardedCmd(AST):
    def __init__(self,e,d,c):
        self.event = e     # type Event
        self.decls = d
        self.cmds = c

class Event(AST): pass

class PacketInEvent(Event):
    def __init__(self, sw, pk, port):
        self.switch = sw
        self.packet = pk
        self.port_in = port
    @property
    def args(self):
        return (self.switch, self.packet, self.port_in)

class PacketFlowEvent(Event):
    def __init__(self, sw, pk, port_in, port_out):
        self.switch = sw
        self.packet = pk
        self.port_in = port_in
        self.port_out = port_out
    @property
    def args(self):
        return (self.switch, self.packet, self.port_in, self.port_out)


class Block(AST):
    def __init__(self,d,c):
        self.decls = d
        self.cmds = c


class Cmd(AST): pass

class ActionCmd(Cmd):
    def __init__(self,id,action):  # @ReservedAssignment
        self.id = id
        self.action = action

class Send(Cmd):
    def __init__(self,sw,pk,pt):
        self.switch = sw
        self.packet = pk
        self.port_out = pt

class SendTranslate(Cmd):
    def __init__(self,sw,pk,pk_tr,pt):
        self.switch = sw
        self.packet = pk
        self.packet_trans = pk_tr
        self.port_out = pt

class Flood(Cmd):
    def __init__(self,sw,pk):
        self.switch = sw
        self.packet = pk

# TODO: rename preds -> params
class Insert(Cmd):
    def __init__(self, rid, preds):
        self.rid = rid       # relation id
        self.preds = preds

class InsertElse(Cmd):
    def __init__(self, rid, pred_sw, pred_port_in, pred_port_out):
        self.rid = rid
        self.pred_sw = pred_sw
        self.pred_port_in = pred_port_in
        self.pred_port_out = pred_port_out

class Install(Cmd):
    def __init__(self, sid, preds):
        self.sid = sid       # switch id
        self.preds = preds

class Remove(Cmd):
    def __init__(self,rid,preds):
        self.rid = rid
        self.preds = preds

class Clear(Cmd):
    def __init__(self, rid):
        self.rid = rid


class IfThen(Cmd):
    def __init__(self,cond,then_cmd):
        self.cond = cond
        self.then_cmd = then_cmd

class IfThenElse(Cmd):
    def __init__(self,cond,then_cmd,else_cmd):
        self.cond = cond
        self.then_cmd = then_cmd
        self.else_cmd = else_cmd

class WhileDo(Cmd):
    def __init__(self,cond,inv,cmd):
        self.cond = cond
        self.inv = inv
        self.cmd = cmd

class ForEach(Cmd):
    def __init__(self,t_vs,cond,inv,cmd):
        self.typed_vars = t_vs
        self.cond = cond
        self.inv = inv
        self.cmd = cmd

class Assume(Cmd):
    def __init__(self, formula):
        self.formula = formula

class Assert(Cmd):
    def __init__(self, formula):
        self.formula = formula

# TODO: remove?
class Method(Cmd):
    def __init__(self, oid, mid, preds):
        self.oid = oid
        self.mid = mid
        self.preds = preds

class Assgn(Cmd):
    def __init__(self, vid, term):
        self.vid = vid
        self.term = term

# TODO: remove it later if not needed
#class Recv(Cmd):
#    def __init__(self, preds):
#        self.preds = preds


class Cond(AST): pass

class TrueCond(Cond): pass

class TupleCond(Cond):
    def __init__(self,id,terms):  # @ReservedAssignment
        self.id = id
        self.terms = terms


class EqualExprs(Cond):
    def __init__(self,e1,e2):
        self.expr1 = e1;
        self.expr2 = e2;

class NegateCond(Cond):
    def __init__(self,c):
        self.cond = c

class ConjoinConds(Cond):
    def __init__(self,c1,c2):
        self.cond1 = c1
        self.cond2 = c2

class DisjoinConds(Cond):
    def __init__(self,c1,c2):
        self.cond1 = c1
        self.cond2 = c2


# Alex: should we use the term Parameter instead of Predicate?

class Pred(AST): pass
class PredExp(Pred):
    def __init__(self,e):
        self.exp = e

class PredFidExp(Pred):
    def __init__(self,fid, e):
        self.fid = fid
        self.exp = e

class ConjoinPred(Pred):
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

class PredAny(Pred): pass

class Exp(AST): pass

class NumericConstExp(Exp):
    def __init__(self,c):
        self.const = c

class TrueConstExp(Exp): pass
class FalseConstExp(Exp): pass

class VarExp(Exp):
    def __init__(self,id):  # @ReservedAssignment
        self.id = id

class PropertyVarExp(Exp):
    def __init__(self, aid, fid):
        self.aid = aid
        self.fid = fid
        
class PortExp(Exp):
    def __init__(self, port_number):
        self.port_number = port_number

class FunExp(Exp):
    def __init__(self,fid,e):
        self.fid = fid
        self.exp = e


class FOL(AST): pass

class TrueFOL(FOL): pass
class FalseFOL(FOL): pass

class AtomicFOL(FOL):
    def __init__(self, rel, terms):
        self.rel = rel
        self.terms = terms

class MethodFOL(FOL):
    def __init__(self, rid, mid):
        self.rid = rid
        self.mid = mid

class EqualityFOL(FOL):
    def __init__(self,t1,t2):
        self.term1 = t1
        self.term2 = t2

class LtFOL(FOL):
    def __init__(self,t1,t2):
        self.term1 = t1
        self.term2 = t2

class LeFOL(FOL):
    def __init__(self,t1,t2):
        self.term1 = t1
        self.term2 = t2

class GtFOL(FOL):
    def __init__(self,t1,t2):
        self.term1 = t1
        self.term2 = t2

class GeFOL(FOL):
    def __init__(self,t1,t2):
        self.term1 = t1
        self.term2 = t2

class NegateFOL(FOL):
    def __init__(self,fol):
        self.alpha = fol

class BinaryConnectiveFOL(FOL):
    def __init__(self,f1,f2):
        self.fol1 = f1
        self.fol2 = f2

class ConjoinFOL(BinaryConnectiveFOL): pass

class DisjoinFOL(BinaryConnectiveFOL): pass

class IffFOL(BinaryConnectiveFOL): pass

class ConjoinListFOL(FOL):
    def __init__(self,fols):
        self.fols = fols

class QuantifiedFOL(FOL):
    def __init__(self,typed_vars, body):
        self.typed_vars = typed_vars
        self.body = body

class ForAll(QuantifiedFOL): pass
class Exists(QuantifiedFOL): pass


class TransitiveClosure(FOL):
    def __init__(self,alpha,beta,gamma,delta,ty,fol):
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.delta = delta
        self.type = ty
        self.fol = fol


class Term(AST): pass

class ExpTerm(Term):
    def __init__(self,e):
        self.exp = e

#TODO: remove?
class RelTerm(Term):
    def __init__(self,rid,terms):
        self.rid = rid
        self.terms = terms

#TODO: remove?
#class LogicVar(Term): pass

class FuncTerm(Term):
    def __init__(self,fid,terms):
        self.fid = fid
        self.terms = terms

class Var(AST): pass

class SimpleVariable(Var):
    def __init__(self,var):
        self.var = var

class ComplicatedVariable(Var):
    def __init__(self,parentVar,childAttribute):
        self.parent = parentVar
        self.child = childAttribute
