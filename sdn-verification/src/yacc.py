# yacc grammar for CSDN
import ply.yacc as yacc
from lex import tokens, lexer  # @UnusedImport
from ast import *  # @UnusedWildImport


### Program Grammar
def p_controller(p):
    'controller : imports decldefs guarded_cmds'
    p[0] = Controller(p[1],p[2],p[3])

# Error rule for syntax errors, some explanations added
def p_error(p):
    if p is not None:
        print "Syntax error in input at line "+str(p.lineno)
        print "additional info is:"
        print p.type, p.value, p.lexpos
        raise SyntaxError, "Syntax error in input at line %d" % p.lineno
    else:
        raise SyntaxError, "Syntax error: unexpected end of input"

def p_imports(p):
    '''imports : import imports
               | empty'''
    if len(p) == 3:
        p[2].insert(0,p[1])
        p[0] = p[2]
    else:
        p[0] = []

def p_import(p):
    '''import : IMPORT FILE'''
    p[0] = p[2]

def p_empty(p):
    'empty :'
    pass

def p_typed_ids(p):
    '''typed_ids : typed_id COMMA typed_ids
                 | typed_id'''
    if (len(p) == 4):
        p[3].insert(0,p[1])
        p[0] = p[3]
    else:
        p[0] = [p[1]]

def p_types(p):
    '''types : type COMMA types
             | type'''
    if (len(p) == 4):
        p[3].insert(0,p[1])
        p[0] = p[3]
    else:
        p[0] = [p[1]]

def p_type(p):
    '''type : ID'''
    p[0] = p[1]

def p_typed_id(p):
    'typed_id : ID COLON type'
    p[0] = TypedId(p[1],p[3])

### Declarations

def p_decldefs(p):
    '''decldefs : decldef decldefs
                | empty'''
    if (len(p) == 3):
        p[2].insert(0,p[1])
        p[0] = p[2]
    else:
        p[0] = []

def p_guarded_cmds(p):
    '''guarded_cmds : guarded_cmd guarded_cmds
                    | empty'''
    if (len(p) == 3):
        p[2].insert(0,p[1])
        p[0] = p[2]
    else:
        p[0] = []

# TODO: no nullary relation allowed
def p_decldef_relation(p):
    'decldef : RELATION ID LPAREN types RPAREN'
    p[0] = Relation(p[2],p[4])

def p_decldef_function(p):
    'decldef : FUNCTION ID LPAREN types RPAREN COLON type'
    p[0] = Function(p[2],p[4],p[7])

def p_decldef_macro(p):
    'decldef : DEF ID LPAREN typed_ids RPAREN COLON fol'
    p[0] = Macro(p[2],p[4],p[7])

def p_decldef_var(p):
    'decldef : vardecl'
    p[0] = p[1]

def p_decldef_type(p):
    'decldef : TYPE type'
    p[0] = Type(p[2])

# TODO: check for decl, matching arity, redefinition, ...
def p_decldef_initrelation(p):
    'decldef : INIT ID EQUALS LPAREN preds RPAREN'
    p[0] = InitRelation(p[2],p[5])

# TODO: semantic checking of fol, consistency
def p_inv_prefix_inv(p):
    'inv_prefix : INV'
    p[0] = Invariant

def p_inv_prefix_topo(p):
    'inv_prefix : TOPO'
    p[0] = TopologyInvariant

def p_inv_prefix_init_topo(p):
    'inv_prefix : INIT TOPO'
    p[0] = InitialTopologyInvariant

def p_inv_prefix_trans(p):
    'inv_prefix : TRANS'
    p[0] = TransitionInvariant

# NB: recv-params are not used now
def p_decldef_invariant(p):
    '''decldef : inv_prefix ID COLON recv_params_option fol'''
    p[0] = p[1] (p[2], p[4], p[5])

def p_recv_params_none(p):
    'recv_params_option : empty'
    p[0] = []

def p_recv_params_some(p):
    'recv_params_option : LBRACK typed_ids RBRACK'
    p[0] = p[2]

def p_decldef_global_assume(p):
    'decldef : ASSUME fol'
    p[0] = Assume(p[2])

def p_guarded_cmd(p):
    '''guarded_cmd : PACKETIN LPAREN term COMMA term COMMA term RPAREN ARROW vardecls cmds'''
    p[0] = GuardedCmd(PacketInEvent(p[3],p[5],p[7]),p[10],p[11])

def p_vardecl(p):
    'vardecl : VAR ID COLON type'
    p[0] = TypedId(p[2],p[4])

def p_guarded_vardecls_empty(p):
    'vardecls : empty'
    p[0] = []

def p_guarded_vardecls(p):
    'vardecls : vardecl vardecls'
    p[0] = [p[1]] + p[2]


### Commands

def p_cmds(p):
    '''cmds : cmd cmds
            | empty'''
    if (len(p) == 3):
        p[2].insert(0,p[1])
        p[0] = p[2]
    else:
        p[0] = []

def p_cmd_send(p):
    'cmd : SEND LPAREN exp COMMA exp COMMA exp RPAREN'
    p[0] = Send(p[3],p[5],p[7])

def p_cmd_send_translate(p):
    'cmd : SEND LPAREN exp COMMA exp COMMA exp COMMA exp RPAREN'
    p[0] = SendTranslate(p[3],p[5],p[7],p[9])

# 'forward' is a synonym of 'send'
def p_cmd_forward(p):
    'cmd : ID DOT FORWARD LPAREN exp COMMA exp RPAREN'
    p[0] = Send(VarExp(p[1]),p[5],p[7])

def p_cmd_forward_translate(p):
    'cmd : ID DOT FORWARD LPAREN exp COMMA exp COMMA exp RPAREN'
    p[0] = SendTranslate(VarExp(p[1]),p[5],p[7],p[9])

def p_cmd_flood(p):
    'cmd : FLOOD LPAREN exp COMMA exp RPAREN'
    p[0] = Flood(p[3],p[5])

def p_cmd_insert(p):
    'cmd : ID DOT INSERT LPAREN preds RPAREN'
    p[0] = Insert(p[1], p[5])

# Alex: insert-'else' is only allowed for ft. Check in vc.wp() is performed.
def p_cmd_insert_else(p):
    'cmd : ID DOT INSERT LPAREN pred COMMA ELSE COMMA pred COMMA pred RPAREN'
    p[0] = InsertElse(p[1], p[5], p[9], p[11])

# Install new flow rules on a switch
def p_cmd_install(p):
    'cmd : ID DOT INSTALL LPAREN preds RPAREN'
    p[0] = Install(p[1], p[5])

def p_cmd_remove(p):
    'cmd : ID DOT REMOVE LPAREN preds RPAREN'
    p[0] = Remove(p[1],p[5])

def p_cmd_clear(p):
    'cmd : ID DOT CLEAR LPAREN RPAREN'
    p[0] = Clear(p[1])

def p_cmd_ifthen(p):
    'cmd : IF cond THEN cmd    %prec THEN'
    p[0] = IfThen(p[2],p[4])

def p_cmd_ifthenelse(p):
    'cmd : IF cond THEN cmd ELSE cmd  %prec ELSE'
    p[0] = IfThenElse(p[2],p[4],p[6])

def p_cmd_whiledo(p):
    'cmd : WHILE cond loopinvs DO cmd'
    p[0] = WhileDo(p[2], ConjoinListFOL(p[3]), p[5])

def p_cmd_block(p):
    'cmd : block'
    p[0] = p[1]

def p_cmd_assert(p):
    'cmd : ASSERT fol'
    p[0] = Assert(p[2])

def p_cmd_assume(p):
    'cmd : ASSUME fol'
    p[0] = Assume(p[2])

def p_cmd_method(p):
    'cond : ID DOT ID LPAREN preds RPAREN'
    p[0] = Method(p[1],p[3],p[5])

def p_block(p):
    'block : LCURLY vardecls cmds RCURLY'
    p[0] = Block(p[2], p[3])

def p_cmd_foreach(p):
    'cmd : FOREACH typed_ids DOT cond loopinvs DO cmd'
    p[0] = ForEach(p[2], p[4], ConjoinListFOL(p[5]), p[7])

def p_cmd_assgn(p):
    'cmd : ID EQUALS term'
    p[0] = Assgn(p[1],p[3])


# Loop invariants

def p_loop_invs(p):
    '''loopinvs : loopinv loopinvs
                | empty'''
    if (len(p) == 3):
        p[0] = [p[1]] + p[2]
    else:
        p[0] = []

def p_loop_inv(p):
    'loopinv : INV fol'
    p[0] = p[2]


### Conditions

def p_cond_true(p):
    'cond : TRUE'
    p[0] = TrueFOL()

def p_cond_false(p):
    'cond : FALSE'
    p[0] = FalseFOL()

def p_cond_tuple(p):
    'cond : ID LPAREN exps RPAREN'
    p[0] = AtomicFOL(p[1],[ExpTerm(x) for x in p[3]])

def p_cond_equals(p):
    'cond : exp EQUALS exp'
    p[0] = EqualityFOL(ExpTerm(p[1]), ExpTerm(p[3]))

def p_cond_nequals(p):
    'cond : exp NEQUALS exp'
    p[0] = NegateFOL(EqualityFOL(ExpTerm(p[1]), ExpTerm(p[3])))

def p_cond_negate(p):
    'cond : NOT cond'
    p[0] = NegateFOL(p[2])

def p_cond_disjoin(p):
    'cond : cond OR cond'
    p[0] = DisjoinFOL(p[1],p[3])

def p_cond_conjoin(p):
    'cond : cond AND cond'
    p[0] = ConjoinFOL(p[1],p[3])

def p_cond_parens(p):
    'cond : LPAREN cond RPAREN'
    p[0] = p[2]

def p_cond_method(p):
    'cond : ID DOT ID LPAREN RPAREN'
    p[0] = MethodFOL(p[1],p[3])

def p_cond_exists(p):
    'cond : EXISTS typed_ids DOT cond   %prec EXISTS'
    p[0] = Exists(p[2],p[4])


### Expressions

def p_exps_ind(p):
    '''exps : exp COMMA exps'''
    p[0] = [p[1]] + p[3]

def p_exps_singleton(p):
    '''exps : exp'''
    p[0] = [p[1]]

def p_pred_exp(p):
    'pred : exp'
    p[0] = PredExp(p[1])
    
def p_pred_ne_exp(p):
    'pred : NEQUALS exp'
    p[0] = NegateFOL(PredExp(p[2]))

def p_pred_colon(p):
    'pred : ID COLON exp'
    p[0] = PredFidExp(p[1], p[3])

# Perhaps, instead we should introduce a class of patterns which will be used
# to install new rules into flow tables.
def p_pred_conjoin(p):
    'pred : pred AND pred'
    p[0] = ConjoinPred(p[1],p[3])

def p_pred_any(p):
    'pred : WILD'
    p[0] = PredAny()

def p_preds_nil(p):
    'preds : empty'
    p[0] = []

def p_preds_cons(p):
    'preds : preds_nonempty'
    p[0] = p[1]

def p_preds_nonempty(p):
    '''preds_nonempty : pred COMMA preds_nonempty
                      | pred'''
    if (len(p) == 4):
        p[0] = [p[1]] + p[3]
    else:
        p[0] = [p[1]]

def p_exp_numeric(p):
    'exp : NUMBER'
    p[0] = NumericConstExp(p[1])

def p_exp_bool_true(p):
    'exp : TRUE'
    p[0] = TrueConstExp()

def p_exp_bool_false(p):
    'exp : FALSE'
    p[0] = FalseConstExp()

def p_exp_var(p):
    'exp : ID'
    p[0] = VarExp(p[1])

def p_exp_property(p):
    'exp : ID DOT ID'
    p[0] = PropertyVarExp(p[1],p[3])

def p_exp_port(p):
    'exp : PORT LPAREN exp RPAREN'
    p[0] = PortExp(p[3])


######### Formula Grammar ##########
def p_fol_true(p):
    'fol : TRUE'
    p[0] = TrueFOL()

def p_fol_false(p):
    'fol : FALSE'
    p[0] = FalseFOL()

def p_fol_equality(p):
    'fol : term EQUALS term'
    p[0] = EqualityFOL(p[1],p[3])

def p_fol_disequality(p):
    'fol : term NEQUALS term'
    p[0] = NegateFOL(EqualityFOL(p[1],p[3]))
    
def p_fol_lt(p):
    'fol : term LT term'
    p[0] = LtFOL(p[1],p[3])

def p_fol_le(p):
    'fol : term LE term'
    p[0] = LeFOL(p[1],p[3])

def p_fol_gt(p):
    'fol : term GT term'
    p[0] = GtFOL(p[1],p[3])

def p_fol_ge(p):
    'fol : term GE term'
    p[0] = GeFOL(p[1],p[3])

def p_fol_atom(p):
    'fol : ID LPAREN terms RPAREN'
    p[0] = AtomicFOL(p[1],p[3])

def p_fol_method(p):
    'fol : ID DOT ID LPAREN RPAREN'
    p[0] = MethodFOL(p[1],p[3])

def p_fol_forall(p):
    'fol : FORALL typed_ids DOT fol   %prec FORALL'
    p[0] = ForAll(p[2],p[4])

def p_fol_exists(p):
    'fol : EXISTS typed_ids DOT fol   %prec EXISTS'
    p[0] = Exists(p[2],p[4])

def p_fol_tc(p):
    'fol : TC LPAREN ID COMMA ID RPAREN LPAREN ID COMMA ID RPAREN LBRACK type RBRACK DOT fol  %prec TC'
    p[0] = TransitiveClosure(p[3],p[5],p[8],p[10],p[13],p[16])

def p_fol_negate(p):
    'fol : NOT fol'
    p[0] = NegateFOL(p[2])

def p_fol_implies(p):
    'fol : fol IMPLIES fol'
    p[0] = DisjoinFOL(NegateFOL(p[1]),p[3])

def p_fol_iff(p):
    'fol : fol IFF fol'
    p[0] = DisjoinFOL(ConjoinFOL(p[1],p[3]),
                      ConjoinFOL(NegateFOL(p[1]), NegateFOL(p[3])))

def p_fol_conjoin(p):
    'fol : fol AND fol'
    p[0] = ConjoinFOL(p[1],p[3])

def p_fol_disjoin(p):
    'fol : fol OR fol'
    p[0] = DisjoinFOL(p[1],p[3])

def p_fol_parens(p):
    'fol : LPAREN fol RPAREN'
    p[0] = p[2]

def p_terms(p):
    '''terms : term COMMA terms
             | term'''
    if (len(p) == 4):
        p[3].insert(0,p[1])
        p[0] = p[3]
    else:
        p[0] = [p[1]]

def p_term_exp(p):
    'term : exp'
    p[0] = ExpTerm(p[1])

def p_term_func(p):
    'term : ID LPAREN terms RPAREN'
    p[0] = FuncTerm(p[1], p[3])

# AND OR NOT EQUALS

precedence = (
    ('right', 'FORALL', 'EXISTS', 'TC'),
    ('right', 'IMPLIES', 'IFF'),
    ('left', 'OR'),
    ('left', 'AND'),
    ('right', 'NOT'),
    ('right', 'THEN'),
    ('right', 'ELSE')
  )


import os.path
here = os.path.dirname(__file__)
parser = yacc.yacc(debug=False, optimize=0,    # optimize=0: auto-generate parsetab if changed
                   tabmodule='autogen.rsdn_parsetab', 
                   outputdir=os.path.join(here,'autogen'))
