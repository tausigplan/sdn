# module: lex.py
# This module just contains the lexing rules

import ply.lex as lex

reserved = {
    'rel' : 'RELATION',
    'func' : 'FUNCTION',
    'def' : 'DEF',
    'init' : 'INIT',
    'var' : 'VAR',
    'inv' : 'INV',
    'topo' : 'TOPO',
    'trans' : 'TRANS',
    'packetIn' : 'PACKETIN',
    'port': 'PORT',
    'send' : 'SEND',
    'sendTr' : 'SEND_TR',
    'forward' : 'FORWARD',
    'flood' : 'FLOOD',
    'insert' : 'INSERT',
    'install' : 'INSTALL',
    'remove' : 'REMOVE',
    'clear' : 'CLEAR',
    'if' : 'IF',
    'then' : 'THEN',
    'else' : 'ELSE',
    'while' : 'WHILE',
    'foreach' : 'FOREACH',
    'in' : 'IN',
    'do' : 'DO',
    'true' : 'TRUE',
    'false' : 'FALSE',
    'forall' : 'FORALL',
    'exists' : 'EXISTS',
    'TC' : 'TC',
    'assert' : 'ASSERT',
    'assume' : 'ASSUME',
    'type' : 'TYPE',
    'import' : 'IMPORT'
}

tokens = [
   'ARROW',
   'LPAREN',
   'RPAREN',
   'LCURLY',
   'RCURLY',
   'EQUALS',
   'NEQUALS',
   'IMPLIES',
   'IFF',
   'WILD',
   'DOT',
   'COMMA',
   'ID',
   'NUMBER',
   'COLON',
   'LBRACK',
   'RBRACK',
   'AND', 'OR', 'NOT',
   'LT', 'LE', 'GT', 'GE',
   'FILE'
] + list(reserved.values())

# Regular expression rules for simple tokens
t_ARROW  = r'\-\>'
t_IMPLIES  = r'\=\>'
t_IFF = r'\<\=\>'
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
t_LCURLY = r'{'
t_RCURLY = r'}'
t_DOT = r'\.'
t_EQUALS = r'\='
t_NEQUALS = r'\!\='
t_COMMA = r'\,'
t_COLON = r'\:'
t_LBRACK = r'\['
t_RBRACK = r'\]'
t_AND = r'\&\&'
t_OR = r'\|\|'
t_NOT = r'\!'
t_WILD = r'\*'
t_LT = r'\<'
t_LE = r'\<\='
t_GT = r'\>'
t_GE = r'\>\='

def t_FILE(t):
    r'\S+\.sdn'
    return t

def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*[*+]?'
    t.type = reserved.get(t.value,'ID')    # Check for reserved words
    return t

# A regular expression rule with some action code
def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_COMMENT(t):
    r'\#.*'
    pass
    # No return value. Token discarded

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    print "Illegal character '%s'" % t.value[0]
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()
