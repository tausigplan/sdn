from z3 import *  # @UnusedWildImport
import sys
import time

try:
    from z3support.adapter import Z3ModelToFolStructure,\
        Z3SymbolsToFolManySortSignature
    has_z3support_adapter = True
except ImportError:
    has_z3support_adapter = False


class SMT(object):
    
    def __init__(self, sys_args):
        self.sys_args = sys_args
        self.background_theory = BoolVal(True)
        self.file_id = 0

    def get_id(self, e):
        return Z3_get_ast_id(e.ctx.ref(), e.as_ast())

    def expr_size(self, e):
        ids = {}
        todo = [(e,0)]
        max_qs = 0
        while todo != []:
           e, qd = todo[-1]
           max_qs = max(max_qs, qd)
           id = self.get_id(e)  # @ReservedAssignment
           todo.pop()
           if id not in ids:
              ids[id] = e
              if is_quantifier(e):
                 todo += [(e.body(), qd+1)]
              elif is_app(e):
                 todo += [(x, qd) for x in e.children()]
        return len(ids), max_qs

    #
    # Print statistics.
    # Z3 does not track statistics from pre-processing,
    # so if a goal is solved by simple rewriting, 
    # not much get printed
    # 
    def print_statistics(self, time, s, claim):
        print "Z3lving time: %.2fs" % time
        print s.statistics()
        sz, qs = self.expr_size(And(self.background_theory, claim))
        print "formula size: ", sz, " # of quantifiers: ", qs

    def prove(self, desc, claim, **kw):
        s = Solver()
        if not kw:
            kw = {}
        if self.sys_args.memory > 0:
            kw["max_memory"] = self.sys_args.memory
        if self.sys_args.timeout > 0:
            kw["timeout"] = self.sys_args.timeout
        if kw: s.set(**kw)
        s.add(self.background_theory)
        s.add(Not(claim))
        if self.sys_args.dump:
            ous = open(self.new_file(), 'w')
            ous.write("; %s\n" % desc)
            ous.write(self.to_smt2(s))
            ous.close()
        #if self.sys_args.verbose:
        #    print s.sexpr()
        now = time.clock()	
        r = s.check()
        delta = time.clock() - now
        m = None
        if r == sat:
            print "counterexample"
            m = self._got_model(desc, s.model())
        elif r == unsat:
            if self.sys_args.verbose:
                end = time.clock()
                print "proved in %4.2f secs" % delta
            else:
                print "proved"
        else:
            print "unknown"
        if self.sys_args.stat:
            self.print_statistics(delta, s, claim)
        sys.stdout.flush()
        return r, m

    def _got_model(self, desc, z3_model):
        if has_z3support_adapter and self.sys_args.graph:
            sigma = Z3SymbolsToFolManySortSignature()(z3_model.decls())
            m = Z3ModelToFolStructure(sigma, sigma.z3_decls)(z3_model)
            print "universe =", m.domain
            for k, v in m.interpretation.iteritems():
                if '!' not in k.literal:
                    print "    %s = %s" % (k, v)
            # Draw it!
            try:
                from viz import NetworkViz
                viz = NetworkViz(self.sys_args, desc)
                viz.depict_hosts_and_switches(sigma, m)
            except ImportError:
                pass
            self._model_stats(z3_model)
        else:
            print z3_model
            self._model_stats(z3_model)
            return z3_model

    def _model_stats(self, z3_model):
        sw = z3_model.get_universe(DeclareSort("SW"))
        ho = z3_model.get_universe(DeclareSort("HO"))
        print '# of switches', len(sw) if sw else 0
        print '# of hosts', len(ho) if ho else 0
    
    def new_file(self):
        id = self.file_id
        self.file_id = id + 1
        return "vc%d.smt2" % id

    def to_smt2(self, s):
        """return SMTLIB2 formatted benchmark for solver's assertions"""
        es = s.assertions()
        sz = len(es)
        sz1 = sz
        if sz1 > 0:
            sz1 -= 1
        v = (Ast * sz1)()
        for i in range(sz1):
            v[i] = es[i].as_ast()
        if sz > 0:
            e = es[sz1].as_ast()
        else:
            e = BoolVal(True, s.ctx).as_ast()
        return Z3_benchmark_to_smtlib_string(s.ctx.ref(), "benchmark generated from python API", "", "unknown", "", sz1, v, e)
