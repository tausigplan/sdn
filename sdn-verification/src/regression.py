import csdn
import z3
import subprocess

from os import listdir
from os.path import isfile, join

z3.Z3_global_param_set(":pp.max_depth","100")

with open("../examples-pldi/regressionResult1.txt", 'w') as resFile:

    def parseResults(f,fileFolder):
        p = subprocess.Popen(['python', "csdn.py", f, '-s'], stdout=subprocess.PIPE,
                                                                    stderr=subprocess.PIPE)
        out, err = p.communicate()
        outLines = out.split("\n")
        ttlTime = 0
        ttlFormulasSize = 0
        ttlQuantifiers = 0
        ttlSpec = 0
        specDepth = 0
        for line in outLines:
            if "Z3lving time:" in line:
                ttlTime += float(line.replace("Z3lving time: ","").replace("s",""))
            elif "formula size:" in line:
                splitRes = line.replace("formula size:  ","").split("  # of quantifiers:  ")
                ttlFormulasSize += int(splitRes[0])
                ttlQuantifiers += int(splitRes[1])
            elif "Spec Sum:" in line:
                splitRes = line.replace("Spec Sum: ","").split("maxSpec:")
                ttlSpec = int(splitRes[0])
                specDepth = int(splitRes[1])

        clearFileName = f.replace(fileFolder, "")
        line = clearFileName[:21]+"\t\t\t"+str(ttlTime)+"\t\t"+str(ttlFormulasSize)+"\t\t"+str(ttlQuantifiers)+"\t\t"+str(ttlSpec)+"\t\t"+str(specDepth)+"\n"
        print line
        resFile.write(line)


    def parseBugsResults(f,fileFolder):

        p = subprocess.Popen(['python', "csdn.py", f, '-s'], stdout=subprocess.PIPE,
			     stderr=subprocess.PIPE)
        out, err = p.communicate()
        outLines = out.split("\n")
        ttlTime = 0
        ttlFormulasSize = 0
        ttlQuantifiers = 0
        cntrSwch = 0
        cntrHosts = 0
        ttlSpec = 0
        specDepth = 0
        for line in outLines:
            if "Z3lving time:" in line:
                ttlTime += float(line.replace("Z3lving time: ","").replace("s",""))
            elif "formula size:" in line:
                splitRes = line.replace("formula size:  ","").split("  # of quantifiers:  ")
                ttlFormulasSize += int(splitRes[0])
                ttlQuantifiers += int(splitRes[1])
            elif "# of switches " in line:
                cntrSwch += int(line.replace("# of switches ", ""))
            elif "# of hosts " in line:
                cntrHosts += int(line.replace("# of hosts ", ""))
            elif "Spec Sum:" in line:
                splitRes = line.replace("Spec Sum: ","").split("maxSpec:")
                ttlSpec = int(splitRes[0])
                specDepth = int(splitRes[1])

        clearFileName = f.replace(fileFolder, "")
        resFile.write(clearFileName+"\t\t\t"+str(ttlTime)+"\t\t"+str(ttlFormulasSize)+"\t\t"+str(ttlQuantifiers)+"\t\t\t"+str(cntrSwch)+"\t\t"+str(cntrHosts)+"\t\t"+str(ttlSpec)+"\t\t"+str(specDepth)+"\n")

    bugsBenchmarkFodlder = "../examples-pldi/buggy/"
    goodBenchmaekFolder =  "../examples-pldi/verified/"

    bugsBenchmarkList = [ f for f in listdir(bugsBenchmarkFodlder) if
                 isfile(join(bugsBenchmarkFodlder,f)) ]

    benchmarkList = [ f for f in listdir(goodBenchmaekFolder) if
                 isfile(join(goodBenchmaekFolder,f)) ]

    resFile.write("=========== Good Benchmarks ===========\n")
    resFile.write("\tFileName\t\tTotalTime\tTotalFormula\tTotalQuantifiers\tTtlSpec\t\tspecDepth\n")
    for f in benchmarkList:
        if ".DS_" in f:
            continue
        f1=goodBenchmaekFolder+f
        parseResults(f1, goodBenchmaekFolder)

    resFile.write("\n")


    resFile.write("=========== Bad Benchmarks ===========\n")
    resFile.write("\tFileName\t\t\tTotalTime\tTotalFormula\tTotalQuantifiers\tTtlSwiches\tTtlHosts\tTtlSpec\t\tspecDepth\n")

    for f in bugsBenchmarkList:
        if ".DS_" in f:
            continue
        f1=bugsBenchmarkFodlder+f
        parseBugsResults(f1, bugsBenchmarkFodlder)
    resFile.close()
