# Network with ring topology

topo PortInj :
    port (0) != port (1) &&
    port (0) != port (2) &&
    port (1) != port (2) &&
    port (0) != null &&
    port (1) != null &&
    port (2) != null

topo Ports : forall P : PR .
    P = port(0) || P = port(1) || P = port(2) || P = null

#topo Switches :
#    exists S1 : SW , S2 : SW, S3 : SW, S4 : SW .
#    forall S : SW . S = S1 || S = S2 || S = S3 || S = S4

#topo Switches1 :
#    exists S1 : SW .
#        S1 != snull &&
#        (forall S : SW . S = S1 || S = snull)

#topo Switches2 :
#    exists S1 : SW , S2 : SW .
#        S1 != S2 &&
#        S1 != snull && S2 != snull &&
#        (forall S : SW . S = S1 || S = S2 || S = snull)

topo Switches3 :
    exists S1 : SW , S2 : SW, S3 : SW .
        S1 != S2 && S1 != S3 && S2 != S3 &&
        S1 != snull && S2 != snull && S3 != snull &&
        (forall S : SW . S = S1 || S = S2 || S = S3 || S = snull)

#topo Switches4 :
#    exists S1 : SW , S2 : SW, S3 : SW, S4 : SW .
#        S1 != S2 && S1 != S3 && S1 != S4 && S2 != S3 && S2 != S4 && S3 != S4 &&
#        S1 != snull && S2 != snull && S3 != snull && S4 != snull &&
#        (forall S : SW . S = S1 || S = S2 || S = S3 || S = S4 || S = snull)

topo tphf_snull :
    forall H : HO . tphf (H) != snull

topo tphf_inj :
    forall H1 : HO, H2 : HO . tphf (H1) = tphf (H2) => H1 = H2

topo tph_tphf :
    forall S : SW, H : HO .
        S = tphf (H) <=> tph (S, port(0), H)

#topo tpsf_snull :
#    forall S : SW, P : PR .
#        tpsf (S, P) = snull => (P = port(0) || S = snull)

# redundant
topo tps_tpss_1 :
    forall S : SW, S1 : SW, P : PR .
        S != snull && S1 = tpss (S, port(1)) <=> tps (S, port(1), port(2), S1)

topo tps_tpsf_2 :
    forall S : SW, S1 : SW, P : PR .
        S != snull && S1 = tpss (S, port(2)) <=> tps (S, port(2), port(1), S1)

topo tps_snull :
    forall P : PR .
        tpss (snull, P) = snull && tpsp (snull, P) = null

topo tpss_succ :
    forall S : SW .
        S != snull =>
        tpss (S, null) = snull &&
        tpss (S, port(0)) = snull &&
        tpss (S, port(1)) != snull &&
        tpss (S, port(2)) != snull

topo tpsp_succ :
    forall S : SW .
        S != snull =>
        tpsp (S, null) = null &&
        tpsp (S, port(0)) = null &&
        tpsp (S, port(1)) = port(2) &&
        tpsp (S, port(2)) = port(1)

topo tps_eq :
    forall S1 : SW, I1 : PR, S2 : SW, I2 : PR .
        tpss (S1, I1) = S2 && tpsp (S1, I1) = I2 &&
        S1 != snull && S2 != snull && I1 != null && I2 != null =>
        tpss (S2, I2) = S1 && tpsp (S2, I2) = I1

var P0 : PK

topo P0inv : forall P : PK . P = P0


rel ordSW (SW, SW)

topo ordSW_null :
    forall S1 : SW, S2 : SW .
        ordSW (S1, S2) => S1 != snull && S2 != snull

topo ordSW_refl :
    forall S : SW . S != snull => ordSW (S, S)

topo ordSW_antisym :
    forall S1 : SW, S2 : SW .
        ordSW (S1, S2) && ordSW (S2, S1) => S1 = S2

topo ordSW_trans :
    forall S1 : SW, S2 : SW, S3 : SW .
        ordSW (S1, S2) && ordSW (S2, S3) => ordSW (S1, S3)

topo ordSW_ends :
    exists S1 : SW, S2 : SW .
        forall S : SW . S != snull => ordSW (S1, S) && ordSW (S, S2)

topo ordSW_lin :
    forall S1 : SW, S2 : SW .
        S1 != snull && S2 != snull =>
        ordSW (S1, S2) || ordSW (S2, S1)

topo ordSW_tps :
    forall S1 : SW, S2 : SW .
        ordSW (S1, S2) && S1 != S2 &&
        (forall S3 : SW . ordSW (S1, S3) && S1 != S3 => ordSW (S2, S3)) =>
        tps (S1, port(2), port(1), S2)

inv traffic_null :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) =>
        S1 != snull && I1 != null &&
        S2 != snull && I2 != null

topo traffic_refl :
    forall P : PK, S : SW, I : PR, B : BUF .
        S != snull && I != null =>
        traffic (P, S, I, B, S, I, B)

inv traffic_trans :
    forall P : PK, S1 : SW, S2 : SW, S3 : SW, I1 : PR, I2 : PR, I3 : PR, B1 : BUF, B2 : BUF, B3 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        traffic (P, S2, I2, B2, S3, I3, B3) =>
        traffic (P, S1, I1, B1, S3, I3, B3)

#inv traffic_lin :
#    forall P : PK, S1 : SW, S2 : SW, S3 : SW, I1 : PR, I2 : PR, I3 : PR, B1 : BUF, B2 : BUF, B3 : BUF .
#        traffic (P, S1, I1, B1, S2, I2, B2) &&
#        traffic (P, S1, I1, B1, S3, I3, B3) =>
#        traffic (P, S2, I2, B2, S3, I3, B3) ||
#        traffic (P, S3, I3, B3, S2, I2, B2)

# holds only in Ring without failures
inv traffic_illegal_1 :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF . 
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        (I2 = port(1) && B2 = buf(true) ||
         I2 = port(2) && B2 = buf(false)) =>
        S1 = S2 && I1 = I2 && B1 = B2

# holds only in Ring without failures
inv traffic_illegal_2 :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF . 
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        (I1 = port(1) && B1 = buf(true) ||
         I1 = port(2) && B1 = buf(false)) =>
        S1 = S2 && I1 = I2 && B1 = B2

inv traffic_in :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF . 
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I2 = port(0) && B2 = buf(false) =>
        S1 = S2 && I1 = I2 && B1 = B2

inv traffic_in_step :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I1 = port(0) && B1 = buf(false) &&
        (S1 != S2 || I1 != I2) =>
        traffic (P, S1, I1, B1, S1, port(2), buf(true))
        && traffic (P, S1, port(2), buf(true), S2, I2, B2)

inv traffic_in_src :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF . 
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I1 = port(0) && B1 = buf(false) &&
        (S1 != S2 || I1 != I2 || B1 != B2) =>
        S1 = tphf (P.src)

inv traffic_out :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I1 = port(0) && B1 = buf(true) =>
        S1 = S2 && I1 = I2 && B1 = B2

inv traffic_out_dst :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I2 = port(0) && B2 = buf(true) &&
        (S1 != S2 || I1 != I2 || B1 != B2) =>
        S2 = tphf(P.dst)

inv traffic_out_step :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I2 = port(0) && B2 = buf(true) &&
        (S1 != S2 || I1 != I2) =>
        traffic (P, S2, port(1), buf(false), S2, I2, B2)
        && traffic (P, S1, I1, B1, S2, port(1), buf(false))

inv traffic_ft :
    forall S : SW, P : PK, I : PR, O : PR .
        ft (S, P, I, O) <=> traffic (P, S, I, buf(false), S, O, buf(true))

# not true in the presence of failures
# not really needed either
#inv traffic_buf :
#    forall P : PK, S : SW, I1 : PR, I2 : PR, B : BUF .
#        traffic (P, S, I1, B, S, I2, B) => I1 = I2

inv traffic_mid :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW .
        S1 != S2 &&
        traffic (P, S1, I1, B1, S2, port(2), buf(true)) =>
        traffic (P, S1, I1, B1, S2, port(1), buf(false))

# cannot prove it, since we cannot reason about casuality
inv traffic_mid_step_backward :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I1 = port(1) && B1 = buf(false) &&
        (S1 != S2 || I1 != I2 || B1 != B2) =>
        traffic (P, tpss (S1, I1), tpsp (S1, I1), buf(true), S1, I1, B1)

inv traffic_mid_step_forward :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I1 = port(1) && B1 = buf(false) &&
        S1 != S2 =>
        traffic (P, S1, I1, B1, S1, port(2), buf(true))
        && traffic (P, S1, port(2), buf(true), S2, I2, B2)

inv traffic_link_1 :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        I2 = port(2) && B2 = buf(true) &&
        (S1 != S2 || I1 != I2 || B1 != B2) =>
        traffic (P, S2, I2, B2, tpss(S2, I2), tpsp(S2, I2), buf(false))

inv traffic_link_2 :
    forall P : PK, S1 : SW, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, port(2), buf(true), S2, I2, B2) &&
        (S1 != S2 || port(2) != I2) =>
        traffic (P, S1, port(2), buf(true), tpss(S1, port(2)), port(1), buf(false))

#inv traffic_antisym :
#    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
#        traffic (P, S1, I1, B1, S2, I2, B2) &&
#        traffic (P, S2, I2, B2, S1, I1, B1) =>
#        S1 = S2 && I1 = I2 && B1 = B2

#inv traffic_dst :
#    forall P : PK .
#        !traffic (P, tphf (P.dst), port(1), buf(false), tphf (P.dst), port(2), buf(true))

inv traffic_dst :
    forall P : PK, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, tphf (P.dst), I1, B1, S2, I2, B2) =>
        tphf (P.dst) = S2 && I1 = I2 && B1 = B2 ||
        tphf (P.dst) = S2 && I2 = port(0) && B2 = buf(true)

inv traffic_link :
    forall P : PK, S1 : SW, S2 : SW .
        traffic (P, S1, port(2), buf(true), S2, port(1), buf(false)) =>
        traffic (P, S1, port(1), buf(false), S1, port(2), buf(true)) ||
        traffic (P, S1, port(0), buf(false), S1, port(2), buf(true))

#inv traffic_blah :
#    forall P : PK .
#        !traffic (P, tphf(P.src), port(2), buf(true), tphf(P.src), port(1), buf(false))

#inv traffic_port :
#    forall P : PK, S : SW, I : PR, B1 : BUF, B2 : BUF .
#        traffic (P, S, I, B1, S, I, B2) => B1 = B2 || I = port(0)

inv traffic_src_dst :
    forall P : PK, S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        traffic (P, S1, I1, B1, S2, I2, B2) &&
        (S1 != S2 || I1 != I2 || B1 != B2) =>
        traffic (P, tphf (P.src), port(0), buf(false), S1, I1, B1)
        #&& traffic (P, S2, I2, B2, tphf (P.dst), port(0), buf(true))

#topo ordSW_inv :
#    forall P : PK, S1 : SW, S2 : SW, I : PR .
#        ordSW (P, S1, port(1), S2, I) && ordSW (P, S2, I, S1, port(2)) => S1 = S2
#
#topo ordSW_src_dst :
#    forall P : PK, S1 : SW, I1 : PR, S2 : SW, I2 : PR .
#        ordSW (P, S1, I2, S2, I2) =>
#        ordSW (P, tphf(P.src), port(0), S1, I1) &&
#        ordSW (P, S2, I2, tphf(P.dst), port(0))
# Alex: we could also introduce functions tphs and tphp and use
#        ordSW (P, tphs(P.src), tphp(P.src), S1, I1) &&
#        ordSW (P, S2, I2, tphs(P.dst), tphp(P.dst))

inv traffic_acyclicity :
    forall P : PK, S1 : SW, S2 : SW, I1 : PR, I2 : PR  .
        traffic (P, S1, I1, buf(false), S2, I2, buf(false)) &&
        traffic (P, S2, I2, buf(false), S1, I1, buf(false)) =>
        S1 = S2 && I1 = I2

topo recv_sent :
    forall S : SW, P : PK .
        recv (S, P, port(1)) =>
        sent (tpss(S, port(1)), P, port(1), port(2)) ||
        sent (tpss(S, port(1)), P, port(0), port(2))

inv sent_ft :
    forall S : SW, P : PK, I : PR, O : PR .
        sent (S, P, I, O) => ft (S, P, I, O)

#rel traffic_new (PK, SW, PR, BUF, SW, PR, BUF)
#rel traffic_diff (PK, SW, PR, BUF, SW, PR, BUF)

#rel ft_new (SW, PK, PR, PR)

var Port0 : PR
var Port1 : PR
var Port2 : PR
var In  : BUF
var Out : BUF
#var s_next : SW
#var i_next : PR
#var p_src : HO
#var p_dst : HO

packetIn(s, p, i) ->
    assume s != snull
    assume i != null
#    assume p_src = p.src
#    assume p_dst = p.dst
    assume Port0 = port(0)
    assume Port1 = port(1)
    assume Port2 = port(2)
    assume In  = buf(false)
    assume Out = buf(true)
#    assume forall P : PK, S1 : SW, S2 : SW, I1 : PR, I2 : PR, B1 : BUF, B2 : BUF .
#        traffic_diff (P, S1, I1, B1, S2, I2, B2) <=>
#        traffic_new (P, S1, I1, B1, S2, I2, B2) && !traffic (P, S1, I1, B1, S2, I2, B2)
    if i = port(0) then {
        assume s = tphf (p.src)
        if tph (s, port(0), p.dst) then {
#            assume s_next = tpss (s, port(0))
#            assume i_next = tpsp (s, port(0))
            ft.insert(s, p, port(0), port(0))
            send (s, p, port(0))
        } else {
#            assume s_next = tpss (s, port(2))
#            assume i_next = tpsp (s, port(2))
            ft.insert(s, p, port(0), port(2))
            send (s, p, port(2))
        }
    } else {
        if i = port(1) then {
            if tph (s, port(0), p.dst) then {
#                assume s_next = tpss (s, port(0))
#                assume i_next = tpsp (s, port(0))
                ft.insert(s, p, port(1), port(0))
                send (s, p, port(0))
            } else {
#                assume s_next = tpss (s, port(0))
#                assume i_next = tpsp (s, port(0))
                ft.insert(s, p, port(1), port(2))
                send (s, p, port(2))
            }
        } 
    }
#    assume forall S : SW, P : PK, I : PR, O : PR .
#        ft_new (S, P, I, O) <=> ft (S, P, I, O)
#    assume forall P : PK, S1 : SW, S2 : SW, I1 : PR, I2 : PR, B1 : BUF, B2 : BUF .
#        traffic_new (P, S1, I1, B1, S2, I2, B2) <=> traffic (P, S1, I1, B1, S2, I2, B2)

