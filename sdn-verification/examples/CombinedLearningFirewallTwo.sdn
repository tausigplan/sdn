# ***   Learning - Firewall
# ***   State is maintained by the controller   ***
#
# Topology configuration
# The network consists of exactly two switches, the learning switch and the
# firewall, connected to each other via port(0).
#
#        L1 (0) -- (0) F (1)
#

#topo onePacket :
#    exists P1 : PK . forall P : PK . P = P1

topo Ports : port(0) != port(1)
topo TwoPorts :
    forall P : PR . P = port(0) || P = port(1)

var Learn : SW
var Fire : SW

topo Switches :
    Learn != Fire &&
    forall S : SW . S = Learn || S = Fire

### Bounds on a topology
#topo twoPackets :
#    exists P1 : PK, P2 : PK .
#        P1 != P2 && forall P : PK . P = P1 || P = P2

topo threePackets :
    exists P1 : PK, P2 : PK, P3 : PK .
        #P1 != P2 && P1 != P3 && P2 != P3 &&
        forall P : PK . P = P1 || P = P2 || P = P3

#topo twoHosts :
#    exists H1 : HO, H2 : HO .
#        H1 != H2 && forall H : HO . H = H1 || H = H2

topo threeHosts :
    exists H1 : HO, H2 : HO, H3 : HO.
        #H1 != H2 && H1 != H3 && H2 != H3 &&
        forall H : HO . H = H1 || H = H2 || H = H3


### Topology specification

# Topology constraints for packet flows in the network

#topo network_tps :
#    tps (Learn, port(0), port(0), Fire) &&
#    tps (Fire, port(0), port(0), Learn) &&
#    (forall P1 : PR, P2 : PR .
#        tps (Fire, P1, P2, Learn) =>
#        P1 = port(0) && P2 = port(0)) &&
#    (forall P1 : PR, P2 : PR .
#        tps (Learn, P1, P2, Fire) =>
#        P1 = port(0) && P2 = port(0))
 
topo network_tps :
    tps (Learn, port(0), port(0), Fire)

# TODO: this should be part of a standard library
#inv link_sent :
topo link_sent :
    forall S : SW, P : PK, I : PR, O : PR .
        sent (S, P, I, O) &&
        (exists SS : SW, OO : PR . tps (SS, OO, I, S)) =>
        exists SS : SW, OO : PR, II : PR .
            tps (SS, OO, I, S) && sent (SS, P, II, OO)

topo link_recv :
    forall S : SW, P : PK, I : PR .
        recv (S, P, I) &&
        (exists SS : SW, OO : PR . tps (SS, OO, I, S)) =>
        exists SS : SW, OO : PR, II : PR .
            tps (SS, OO, I, S) && sent (SS, P, II, OO)


# reachable (S, I, H) iff host H is reachable on switch S from port I
#rel reachable (SW, PR, HO)

#### TODO: should this go to the library(?)
topo tph_inj :
    forall H : HO, S1 : SW, S2 : SW, I1 : PR, I2 : PR .
        tph (S1, I1, H) && tph (S2, I2, H) => S1 = S2 && I1 = I2

# Specification of incoming port of the learning switch
#inv Learn_incoming :
topo Learn_incoming :
    forall P : PK, H : HO, O : PR .
        sent (Learn, P, port(1), O) =>
        tph (Learn, port(1), P.src)

topo Learn_incoming_recv :
    forall P : PK, H : HO .
        recv (Learn, P, port(1)) =>
        tph (Learn, port(1), P.src)

# Specification of incoming port of the firewall
#inv Fire_incoming :
topo Fire_incoming :
    forall P : PK, H : HO, O : PR .
        sent (Fire, P, port(1), O) =>
        tph (Fire, port(1), P.src)

topo Fire_incoming_recv :
    forall P : PK, H : HO .
        recv (Fire, P, port(1)) =>
        tph (Fire, port(1), P.src)

### Data structures used in the network
#
# Learning
rel connected (SW, PR, HO)
init connected = ()

# Firewall
rel trusted (SW, HO)
init trusted = ()

### Goal invariants
#
# 1. Learning switch installs correctly flow table rules.
#
inv Learn_correct_forwarding :
    forall P : PK, I : PR, O : PR .
        ft (Learn, P, I, O) =>
        exists PP : PK, O1 : PR .
            PP.src = P.dst && sent (Learn, PP, O, O1)

# 2. (optional) On the Learning, only send packets arriving on port(0) from
# hosts which are authorized.
# (We say that the host is authorized if it received a packet from a trusted
# subnetwork.)
# This also means that an authorized (but not trusted) host cannot authorize
# other hosts.
#
inv correct_forwarding_within_firewall_policy :
    forall P : PK, O : PR .
        sent (Learn, P, port(0), O) =>
        exists PP : PK .
            PP.dst = P.src && sent (Learn, PP, port(1), port(0))

# 3. Firewall only sends packets from authorized hosts
#
inv correct_filtering :
    forall S : SW, P : PK .
        sent(Fire, P, port(1), port(0)) =>
        exists PP : PK .
            PP.dst = P.src && sent (Fire, PP, port(0), port(1))


### Auxiliary invariants

### Auxiliary invariants for Learning switch

## 1a. Flow rules are consistent with recorded connections
inv data_structure_inv_flow_table_1:
    forall P : PK, I : PR, O : PR .
        ft (Learn, P, I, O) => connected (Learn, O, P.dst)

## 2a. Flow rules are consistent with recorded connections
inv data_structure_inv_flow_table_2:
    forall P : PK, I : PR, O : PR .
        ft (Learn, P, I, O) => connected (Learn, I, P.src)

## 3a. 'connected' is consistent
inv connected_sent :
    forall I : PR, H : HO .
        connected (Learn, I, H) =>
        exists P : PK, O : PR .
            P.src = H && sent (Learn, P, I, O)

## 4a. Sent back to the port the packet arrived only in the case its
## destination is learned (flooding is excluded).
#
inv data_structure_inv_flow_loop :
    forall P : PK, I : PR .
        sent (Learn, P, I, I) =>
        connected (Learn, I, P.dst)


#### Auxiliary invariants for Firewall
#
# 1a. Only send from hosts which received messages
#inv correct_firewall :
#    forall P : PK .
#        sent (Fire, P, port(1), port(0)) =>
#        trusted (Fire, P.src)

## 2a. Correct trusted host recording
inv representation_invariant:
    forall H : HO .
        trusted (Fire, H) =>
        exists P: PK .
            P.dst = H && sent (Fire, P, port(0), port(1))

## 3a. Correct flow table rules
inv consistent_flow_table:
    forall P : PK .
        ft (Fire, P, port(1), port(0)) =>
        exists PP: PK .
            PP.dst = P.src && sent (Fire, PP, port(0), port(1))

#inv consistent_flow_table_trusted :
#    forall P : PK .
#        ft (Fire, P, port(1), port(0)) =>
#        trusted (Fire, P.src)

# Some traffic restrictions for the firewall
inv Fire_sent_0 :
    forall P : PK, O : PR .
        sent (Fire, P, port(0), O) => O = port(1)

inv Fire_sent_1 :
    forall P : PK, O : PR .
        sent (Fire, P, port(1), O) => O = port(0)

inv Firewall_ft_1 :
    forall P : PK, O : PR .
        ft (Fire, P, port(0), O) => O = port(1)

inv Firewall_ft_2 :
    forall P : PK, I : PR .
        ft (Fire, P, I, port(1)) => I = port(0)


# auxiliary structures for debugging 
var port0 : PR
assume port0 = port(0)
var port1 : PR
assume port1 = port(1)

rel connected1 (SW, PR, HO)

rel sent0 (SW, PK, PR, PR)
rel sent1 (SW, PK, PR, PR)

rel is_connected (SW, HO)

### Firewall rules
#
#packetIn(Fire, p, port(0)) ->          # packets from trusted hosts
#    send(Fire, p, port(1))             # forward the packet to untrusted hosts
#    trusted.insert(Fire, p.dst)        # insert the target of p into trusted controller memory
#    Fire.install(p, port(0), port(1))  # insert a per-flow rule to forward future packets
#
#packetIn(Fire, p, port(1)) ->          # packets from untrusted hosts
#    if trusted(Fire, p.src) then {
#        send(Fire, p, port(0))         # forward the packet to trusted hosts
#        Fire.install (p, port(1), port(0)) # insert a per-flow rule to forward future packets
#    }

### Learning switch rules

packetIn (s, p, i) ->
    var o : PR
    #assume forall S : SW, P : PK, I : PR, O : PR .
    #    sent0 (S, P, I, O) <=> sent (S, P, I, O)
    if (s = Learn) then {
        connected.insert (s, i, p.src)
        if connected(s, o, p.dst) then {
            send (s, p, o)
            s.install (p, i, o)
        } else {
            flood (s, p)
        }
#        var o : PR
#        connected.insert (s, i, p.src)
#        assume forall S : SW, H : HO .
#            is_connected (S, H) <=> exists O : PR . connected (S, O, H)
#        if is_connected(s, p.dst) then {
#            assume connected (s, o, p.dst)
#            send (s, p, o)
#            s.install (p, i, o)
#        } else {
#            flood (s, p)
#        }
    }
    if (s = Fire) then {
        if (i = port(0)) then {
            send(Fire, p, port(1))             # forward the packet to untrusted hosts
            trusted.insert(Fire, p.dst)        # insert the target of p into trusted controller memory
            Fire.install(p, port(0), port(1))  # insert a per-flow rule to forward future packets
        } else {
            if trusted(Fire, p.src) then {
                send(Fire, p, port(0))         # forward the packet to trusted hosts
                Fire.install (p, port(1), port(0)) # insert a per-flow rule to forward future packets
            }
        }
    }
    #assume forall S : SW, O : PR, H : HO .
    #    connected1 (S, O, H) <=> connected (S, O, H)
    #assume forall S : SW, P : PK, I : PR, O : PR .
    #    sent1 (S, P, I, O) <=> sent (S, P, I, O) && !sent0 (S, P, I, O)

