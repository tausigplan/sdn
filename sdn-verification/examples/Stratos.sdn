# THIS VERSION IS BUGGY!!!
#
# Very simplified version of Stratos (http://stratos.cs.wisc.edu)
# Steers traffic through a chain of middleboxes

rel element (PR, HO)      # Element-ID, Instance-ID
rel chain   (PR, PR)     # Element-ID, Element-ID

topo chainInv1 :
    forall E : PR, N1 : PR, N2 : PR .
        chain (E, N1) && chain (E, N2) => N1 = N2

# Alex: is it possible that two consequtive elements of the chain are
# equal?
# Alex: I think we need also the following one in order to avoid loops
#topo chainInv2 :
#    forall E : PR, N : PR . chain (E, N) => E < N

topo Topo2 :
    forall S : SW, P : PR, H1 : HO, H2 : HO .
    tph (S, P, H1) && tph (S, P, H2) => H1 = H2

# Switches are connected in a full mesh
topo Mesh : 
    forall S1 : SW, S2 : SW .
        !S1 = S2 => exists P1 : PR, P2 : PR . tps (S1, P1, P2, S2)

topo Topo1 :
    forall S1 : SW, S2 : SW, P1 : PR, P2 : PR, H : HO .
        tps (S1, P1, P2, S2) => !tph (S1, P1, H)

topo SwConnected_1 :
    forall S1 : SW, S2 : SW, P1 : PR, P2 : PR, P3 : PR, P4 : PR .
        tps(S1, P1, P2, S2) && tps(S1, P3, P4, S2) => P1 = P3 && P2 = P4
        
topo SwConnected_2 :
    forall S : SW, S1 : SW, S2 : SW, P : PR, P1 : PR, P2 : PR .
        tps (S, P, P1, S1) && tps (S, P, P2, S2) => S1 = S2 && P1 = P2

# Every instance is connected to a (unique) switch/port 
topo InstConnected :
    forall Inst : HO .
        (exists S : SW, P : PR . tph (S, P, Inst)) &&
        forall S1 : SW, S2 : SW, P1 : PR, P2 : PR .
            tph (S1, P1, Inst) && tph (S2, P2, Inst) => S1 = S2 && P1 = P2

# There is at least one instance of each element
#topo AtLeastOneInstOfEachElem :
#    forall ElemId : PR . exists Inst : HO .
#        element (ElemId, Inst)
# Alex: This axiom makes Z3 to go into infinite loop while a consistency check

# Each instance is associated with only one element
topo OneElemPerInst :
    forall Inst : HO .
        (exists ElemId : PR . element (ElemId, Inst)) &&
        forall E1 : PR, E2 : PR . element (E1, Inst) && element (E2, Inst) => E1 = E2

# I believe this invariant said there is exactly one instance of each element,
# which is not the right invariant
#topo OneOfEachKind :
#  forall S : SW, ElemId : PR . exists P : PR, H : HO, InstId : PR .
#      tph(S, P, H) && instances_h(H, InstId) && element(ElemId, InstId)

# Packets should only be received from mangling middleboxes/servers
# Since we do not have loops, we cannot write code that maintains this
#inv OnlyFromMangling : forall S : SW, P : PK, I : PR . 
#    exists ElemId : PR, InstId : PR .
#        recv(S, P, I) => mangling(ElemId, 1) && element(ElemId, InstId) &&
#            instances(S, I, InstId)

# All packets of a flow must traverse the same middlebox/server instance
# in both directions
#inv SameInstanceBothDirections : 
#    forall S : SW, P : PK, PI : PK, F : PR, R : PR, O : PR .
#        P.src = PI.dst && P.dst = PI.src && ft (S, P, F, O) => ft (S, PI, R, O)
# Alex: I don't really get it..

# We should never receive more than one packet-in for a flow from a given
# MB, implying that all packets of a flow traverse the same path
#inv SamePathForCompleteFlow :
#    forall S : SW, P : PK, PI : PK, I : PR, O : PR .
#        P.src = PI.dst && P.dst = PI.src &&
#        recv(S, P, I) => ( !ft(S, P, I, O) && !ft(S, PI, I, O) )

# Alex: this is my version of the invariant
inv SamePathForCompleteFlow :
    forall S : SW, P1 : PK, P2 : PK, I : PR, O1 : PR, O2 : PR, M1 : HO, M2 : HO, E : PR .
        P1.src = P2.src && P1.dst = P2.dst &&
        ft (S, P1, I, O1) && ft (S, P2, I, O2) &&
        tph (S, O1, M1) && tph (S, O2, M2) &&
        element (E, M1) && element (E, M2) => O1 = O2


topo RelaxedWired :  forall S : SW, P : PK, I : PR .
    recv(S, P, I) => tph(S, I, P.src) || exists I1 : PR, O1 : PR. sent(S, P, I1, O1) && tph(S, O1, P.src)

inv OnlySendIfRuleExists : forall S : SW, P : PK, I : PR, O : PR .
    sent(S, P, I, O) => ft(S, P, I, O)

# We only install rules based on src and dst fields of the packet header
inv FTaxiom1: 
    forall S : SW, P1 : PK, P2 : PK, I : PR, O : PR .
        ft (S, P1, I, O) && P2.src = P1.src && P2.dst = P1.dst  => ft (S, P2, I, O)
        

# All packets of a flow must traverse an instance of each middlebox in the
# chain
#inv TraverseEntireChain :
#    forall P : PK, ElemId : PR . 
#        exists InstId : PR, S : SW, I : PR, O : PR .
#            element(ElemId, InstId) && chain(ElemId) &&
#            instances(S, O, InstId) && ft(S, P, I, O)

packetIn(srcSw, pkt, srcPr) ->
    var srcInst : HO 
    var srcElemId : PR
    var nxtInst : HO
    var nxtElemId : PR
    var fwdPr : PR
    var revPr : PR
    var nxtSw : SW
    var nxtPr : PR

    # Determine the tenant with which packet is associated
    # For now, assume only one tenant

    # Determine middlebox/server instance which emitted the packet
    # Determine middlebox/server type which emitted the packet
    # Determine middlebox/server instance for which the packet is destined
    # Determine middlebox/server type for which the packet is destined
    # Also, make sure we received the packet from a mangling middlebox/server

    assume exists Inst : HO . tph (srcSw, srcPr, Inst) #&& element (ElemId, Inst)

    if tph (srcSw, srcPr, srcInst) &&
       element (srcElemId, srcInst) then {
        # Only install for the next element in the chain, since we cannot loop

        # Pick one instance of the next element
        # Ideally, we would pick specific instances based on weights, but
        #   we are ignoring this for now

        # Get the switch and port to which the instance is connected
        if chain (srcElemId, nxtElemId) &&
           nxtElemId != srcElemId &&
           element (nxtElemId, nxtInst) &&
           tph (nxtSw, nxtPr, nxtInst) then {

            # If the source and next instance are connected to the same
            # switch, then we install a rule only on that switch
            # Alex: and what about forwarding of pkt? Is the packet dropped?
            if srcSw = nxtSw then {
                # Install a rule for the forward direction
                ft.insert(srcSw, src: pkt.src && dst: pkt.dst, srcPr, nxtPr)
                # Alex: was ft.insert(srcSw, pkt, srcPr, nxtPr)

                # Install a rule for the reverse direction
                ft.insert(srcSw, src: pkt.dst && dst: pkt.src, nxtPr, srcPr) 

            # Otherwise, we need to go between switches
            # Get the port to send from previous to current switch
            # Get the port to send form current to previous switch 
            } else if tps(srcSw, fwdPr, revPr, nxtSw) then {

                # Install a rule to forward from the port for the source
                # instance to the port that links to the next switch
                ft.insert(srcSw, src: pkt.src && dst: pkt.dst, srcPr, fwdPr)
                
                # Install a rule to forward from the port that links to the
                # soure switch to the port for the next instance
                ft.insert(nxtSw, src: pkt.src && dst: pkt.dst, revPr, nxtPr)
                
                # Install a rule to forward from the port for the next
                # instance to the port that links to the source switch
                ft.insert(nxtSw, src: pkt.dst && dst: pkt.src, nxtPr, revPr)
                
                # Install a rule to forward from the port that links to the
                # next switch to the port for the source instance
                ft.insert(srcSw, src: pkt.dst && dst: pkt.src, fwdPr, srcPr)
                
                # Alex: I think we should forward the packet now
                #send (srcSw, pkt, fwdPr)
            }
        }
    }
