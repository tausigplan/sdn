# ***   Firewall:
# ***   State is maintained by the controller   ***

topo Ports : !port(1) = port(2)

rel trusted(SW, HO)
init trusted = ()

### Goal invariants
# 1. Only send from hosts which received messages
inv correct:
    forall S : SW, P : PK .
        sent(S, P, port(2), port(1)) =>
        exists PP: PK. PP.dst = P.src && sent(S, PP, port(1), port(2))

### Auxiliary invariants
# 2. Correct trusted host recording
inv reprsentation_invariant:
    forall S : SW, H: HO.
        trusted(S, H) => exists P : PK. P.dst = H && sent(S, P, port(1), port(2))

# 3. Correct flow table rules
inv consistent_flow_table:
    forall S : SW, P : PK. ft(S, P, port(2), port(1)) =>
        exists PP: PK. PP.dst = P.src && sent(S, PP, port(1), port(2))

packetIn(s, p, port(1)) ->          # packets from trusted hosts
    send(s, p, port(2))             # forward the packet to untrusted hosts
    trusted.insert(s, p.dst)        # insert the target of p into trusted controller memory
    s.install(p, port(1), port(2))  # insert a per-flow rule to forward future packets

packetIn(s, p, port(2)) ->          # packets from untrusted hosts
    if trusted(s, p.src) then {
        send(s, p, port(1))         # forward the packet to trusted hosts
        s.install (p, port(2), port(1)) # insert a per-flow rule to forward future packets
    }
