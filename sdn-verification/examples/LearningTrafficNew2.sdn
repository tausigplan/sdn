
######################################################
#### Successful runs
######################################################
# SW PR PK
# 2  2  inf
# 2  3  2
# 2  3  3
# 2  3  inf
######################################################
# The running time might be better with
#   tp_good_3
# and
#   data_structure_connected_sent_2
# commented OUT
######################################################


#topo bound_switches :
#    exists S1 : SW , S2 : SW, S3 : SW, S4 : SW .
#    forall S : SW . S = S1 || S = S2 # || S = S3 #|| S = S4
#    #forall S : SW . S = S1

#topo bound_ports :
#    exists P1 : PR, P2 : PR, P3 : PR, P4 : PR, P5 : PR, P6 : PR .
#    P1 != P2 &&
#    forall P : PR . P = P1 || P = P2 # || P = P3 #|| P = P4 || P = P5 || P = P6

#topo bound_packets :
#    exists A1 : PK, A2 : PK, A3 : PK, A4 : PK, A5 : PK, A6 : PK .
#    forall A : PK . A = A1 || A = A2 #|| A = A3 #|| A = A4 || A = A5 || A = A6

######################################################
# Extra topological axioms relating tph and tps
######################################################

topo tph_is_functional :
    forall H : HO, S1 : SW, S2 : SW, I1 : PR, I2 : PR .
        tph(S1, I1, H) && tph(S2, I2, H) =>
        S1 = S2 && I1 = I2

topo tps_tph :
    forall H : HO, S1 : SW, S2 : SW, I1 : PR, I2 : PR .
        tps(S1, I1, I2, S2) => !tph(S1, I1, H)

#topo no_double_links :
#    forall S1 : SW, S2 : SW, I1 : PR, I2 : PR, I3 : PR, I4 : PR .
#        tps(S1, I1, I2, S2) && tps(S1, I3, I4, S2) =>
#        I1 = I3 && I2 = I4

######################################################
# The traffic path relation
######################################################

init topo tps_traffic_init :
    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR .
        tps (S1, P1, P2, S2) <=>
        traffic (A, S1, P1, buf_out, S2, P2, buf_in)

# tps is contained in traffic
inv tps_traffic :
    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR .
        tps (S1, P1, P2, S2) =>
        traffic (A, S1, P1, buf_out, S2, P2, buf_in) &&
        (forall T : SW, O : PR, B : BUF .
            (S1 != T || P1 != O || buf_out != B) &&
            traffic (A, S1, P1, buf_out, T, O, B) =>
            traffic (A, S2, P2, buf_in, T, O, B))

inv traffic_tps :
    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR, B2 : BUF .
    traffic (A, S1, P1, buf_out, S2, P2, B2) &&
    (S1 != S2 || P1 != P2 || buf_out != B2) =>
        exists S3 : SW, P3 : PR . tps (S1, P1, P3, S3)

inv ft_traffic :
    forall A : PK, S : SW, I : PR, O : PR .
        ft (S, A, I, O) =>
        traffic (A, S, I, buf_in, S, O, buf_out) &&
        (forall T : SW, P : PR, B : BUF .
            (S != T || I != P || buf_in != B) &&
            traffic (A, S, I, buf_in, T, P, B) =>
            traffic (A, S, O, buf_out, T, P, B))

inv traffic_ft_1 :
    forall A : PK, S : SW, S1 : SW, I : PR, O : PR, B : BUF .
        traffic (A, S, I, buf_in, S1, O, B) &&
        (S != S1 || I != O || B != buf_in) =>
        exists O1 : PR . ft (S, A, I, O1)

inv traffic_refl :
    forall A : PK, S : SW, P : PR, B : BUF .
        traffic (A, S, P, B, S, P, B)

inv traffic_trans :
    forall A : PK, S1 : SW, S2 : SW, S3 : SW, P1 : PR, P2 : PR, P3 : PR, B1 : BUF, B2 : BUF, B3 : BUF .
        traffic (A, S1, P1, B1, S2, P2, B2) &&
        traffic (A, S2, P2, B2, S3, P3, B3) =>
        traffic (A, S1, P1, B1, S3, P3, B3)

inv tp_linear :
    forall A : PK, S1 : SW, S2 : SW, S3 : SW, P1 : PR, P2 : PR, P3 : PR, B1 : BUF, B2 : BUF, B3 : BUF .
        traffic (A, S1, P1, B1, S2, P2, B2) && traffic (A, S1, P1, B1, S3, P3, B3) =>
        traffic (A, S2, P2, B2, S3, P3, B3) || traffic (A, S3, P3, B3, S2, P2, B2)

inv tp_good_1 :
    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR .
        traffic (A, S1, P1, buf_in, S2, P2, buf_in) &&
        S1 != S2  =>
        exists O1 : PR . traffic (A, S1, P1, buf_in, S1, O1, buf_out)

inv tp_good_2 :
    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR, II2 : PR, B2 : BUF .
        traffic (A, S1, P1, buf_out, S2, II2, B2) &&
        tps(S1, P1, P2, S2) =>
        traffic (A, S2, P2, buf_in, S2, II2, B2)

#inv tp_good_3 :
#    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR .
#        traffic (A, S1, P1, buf_out, S1, P1, buf_in) &&
#        tps(S1, P1, P2, S2) =>
#        traffic (A, S2, P2, buf_in, S2, P2, buf_out)


#rel wired (SW, PR, HO)

rel connected (SW, PR, HO)
init connected = ()

#inv correct_topology:
#    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => tph(S, O, P.dst)
#inv data_structure_inv_learning:
#    forall S : SW, H : HO, I : PR . connected(S, I, H) => tph(S, I, H)

inv connected_is_functional :
    forall S : SW, I1 : PR, I2 : PR, H : HO .
        connected(S, I1, H) && connected(S, I2, H) => I1 = I2


inv data_structure_ft_sent :
    forall S : SW, P : PK, I : PR, O : PR .
        ft(S, P, I, O) => exists I1: PR . sent(S, P, O, I1)

inv data_structure_inv_flow_table_1 :
    forall S : SW, P : PK, I : PR, O : PR .
        ft(S, P, I, O) => connected(S, O, P.src)

##inv data_structure_inv_flow_table_2 :
##    forall S : SW, P : PK, I : PR, O : PR .
##        ft(S, P, I, O) => connected(S, I, P.src)

inv data_structure_connected_sent_1 :
    forall S : SW, I : PR, H : HO .
        connected(S, I, H) =>
        exists P : PK, O : PR . sent(S, P, I, O) && H = P.src

#inv data_structure_connected_sent_2 :
#    forall P : PK, S : SW, I : PR, O : PR, H : HO .
#        sent(S, P, I, O) && H = P.src =>
#        exists II : PR . connected(S, II, H)

inv connected_causality_1 :
    forall H : HO, S : SW, I : PR, S1 : SW, O1 : PR .
        connected(S, I, H) && tps(S1, O1, I, S) =>
        exists I1 : PR . I1 != O1 && connected(S1, I1, H)

inv sent_causality_1 :
    forall P : PK, S : SW, I : PR, O : PR, S1 : SW, O1 : PR .
        sent(S, P, I, O) && tps(S1, O1, I, S) =>
        exists I1 : PR . sent(S1, P, I1, O1)

inv sent_tph :
    forall P : PK, S : SW, I : PR, O : PR, Hsrc : HO.
        sent(S, P, I, O) && Hsrc = P.src =>
        exists Sorig : SW, Iorig : PR .
            tph(Sorig, Iorig, Hsrc) &&
            connected(Sorig, Iorig, Hsrc)

inv ft_is_functional:
    forall S : SW, P : PK, I : PR, O1 : PR, O2 : PR .
        ft(S, P, I, O1) && ft(S, P, I, O2) => O1 = O2

inv tp_acyclicity :
    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR, B2 : BUF  .
        traffic (A, S1, P1, buf_in, S2, P2, B2) &&
        traffic (A, S2, P2, B2, S1, P1, buf_in) =>
        S1 = S2

#inv tp_acyclicity_stronger :
#    forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR, B1 : BUF, B2 : BUF  .
#        traffic (A, S1, P1, B1, S2, P2, B2) &&
#        traffic (A, S2, P2, B2, S1, P1, B1) =>
#        S1 = S2

#topo Wired :
#    [S : SW, P : PK, I : PR] wired(S, I, P.src)

#trans progress: forall S : SW, P : PK, I : PR, H : HO .
#    recv(S, P, I) => connected(S, I, P.src)

#trans simple_fwd: forall S : SW, P : PK, I : PR.
#    (recv(S, P, I) && (exists O1: PR. O1 != I && tph(S, O1, P.dst))) =>
#                      (exists O2: PR. tph(S, O2, P.dst) && sent(S, P, I, O2))


# Debug structures
#rel traffic_debug (PK, SW, PR, BUF, SW, PR, BUF)
#assume forall A : PK, S1 : SW, S2 : SW, P1 : PR, P2 : PR, B1 : BUF, B2 : BUF .
#        traffic (A, S1, P1, B1, S2, P2, B2) <=> 
#        traffic_debug (A, S1, P1, B1, S2, P2, B2)


######################################################
# Topological assumtions about received packets
######################################################

topo recv_tps_sent :
    [ Scur : SW, Pcur : PK, Icur : PR ]
        forall S1 : SW, I1 : PR .
            tps(S1, I1, Icur, Scur) =>
            exists I2 : PR. sent(S1, Pcur, I2, I1) 

topo recv_connected :
    [ Scur : SW, Pcur : PK, Icur : PR ]
        forall S1 : SW, I1 : PR, Hsrc : HO .
            Hsrc = Pcur.src &&
            tps(S1, I1, Icur, Scur) =>
            exists I2 : PR. connected(S1, I2, Hsrc)

topo recv_tph :
    [ Scur : SW, Pcur : PK, Icur : PR ]
    forall Hsrc : HO .
        Hsrc = Pcur.src =>
        exists Sorig : SW, Iorig : PR .
            tph(Sorig, Iorig, Hsrc) &&
            connected(Sorig, Iorig, Hsrc)

packetIn (s, p, i) ->
    var o : PR
    #var ii : PR
    #assume forall S1: SW, P1: PR. exists P2: PR. tps(S1, P1, i, s) => sent(S1, p, P2, P1) 
    #if !connected (s, ii, p.src) then {
    #    connected.insert (s, i, p.src)
    #}
    if !(exists II : PR. connected (s, II, p.src)) then {
        connected.insert (s, i, p.src)
        s.install (p,*,i)
    }
    if connected(s, o, p.dst) then {
        send (s, p, o)
    } else {
        flood (s,p)
    }
#    assume forall S1 : SW, S2 : SW, P1 : PR, P2 : PR, B1 : BUF, B2 : BUF .
#        traffic_debug(S1,P1,B1,S2,P2,B2) <=> traffic(S1,P1,B1,S2,P2,B2)

