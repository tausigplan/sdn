# ***    Middlebox Composition:
#
#   This controller program is an example of a middlebox composition 
# in a network with internal hosts connected to a NAS and IPS 
# (Intrusion Prevention System) middleboxes.
#   The IPS may drop suspicious packet.
#   The NAS transforms the packet headers, mapping multiple internal
# addresses to a single external address. It keeps a state of translation 
# allow for bydirectional communication.
# 

import ../examples/./MiddleboxComposition_p1.sdn


##### Axioms for the middlebox calls
## NAT
# topo tr_src__injective:
#     forall P: PK, P1: PK, P2: PK, SRC1: HO, SRC2: HO.
# 	tr_src (P1,SRC1,P) && tr_src (P2,SRC2,P) =>
# 	P1 = P2 && SRC1 = SRC2

topo tr_src__correctness:
    forall P : PK, PP : PK, SRC : HO .
        tr_src (P, SRC, PP) => PP.src = SRC

topo tr_src__dst_agnostic:
    forall P : PK, PP : PK, SRC : HO .
        tr_src (P, SRC, PP) => PP.dst = P.dst

# the following two axioms, while correct, are not essential (?) to the 
# correctness of the solution. However, they might be useful for solving.
# tr_src is idempotent
#topo tr_src__idempotent:
#    forall P : PK, P1 : PK, SRC : HO .
#        tr_src2 (P, SRC, P1) => tr_src2 (P1, SRC, P1)
#	tr_src(tr_src(P,SRC),SRC) = tr_src(P,SRC)

# tr_src with the packets own src has no effect
topo tr_src__self_call:
    forall P : PK . tr_src (P, P.src, P)



rel tr_dst (PK, HO, PK)

topo tr_dst__function:
    forall P: PK, P1: PK, P2: PK, SRC: HO.
        tr_dst (P,SRC,P1) && tr_dst (P,SRC,P2) =>
        P1 = P2
    
# #####
# # Following is a mirroring of the above invariants for tr_dst

topo tr_dst__correctness:
    forall P : PK, PP : PK, SRC : HO .
        tr_dst (P, SRC, PP) => PP.dst = SRC

topo tr_dst__src_agnostic:
    forall P : PK, PP : PK, SRC : HO .
        tr_dst (P, SRC, PP) => PP.src = P.src

# topo tr_dst__correctness:
#     forall P : PK, PP : PK, DST : HO .
# 	tr_dst(P,DST) = PP => PP.dst = DST

# topo tr_dst__src_agnostic:
#     forall P : PK, PP : PK, DST : HO .
# 	tr_dst(P,DST) = PP => PP.src = P.src

# topo tr_dst__idempotent:
#     forall P : PK, DST : HO .
# 	tr_dst(tr_dst(P,DST),DST) = tr_dst(P,DST)

topo tr_dst__self_call:
    forall P : PK . tr_dst (P, P.dst, P)

# topo tr_dst__self_call:
#     forall P : PK .
# 	tr_dst(P,P.dst) = P


## IPS
# # allow packets that are not suspicious
# topo ips_correctness:
#     forall P:PK.
# 	suspicious(P) => ips(P)

# # no false positives
# topo ips_no_false_positives:
#     forall P:PK.
# 	!suspicious(P) => !ips(P)

# # both of the above in a single invariant
# inv ips_full_correctness:
#     forall P:PK.
# 	! suspicious(P) xor ips(P)

#####
# Invariants we would like to verify for the middleboxes

## NAT
### Auxiliary invariants (can be inferred automatically by strengthening)
inv nat_trans :
    forall S : SW, Hs : HO, Hd : HO, New_src : HO. 
        translated (S, Hs, Hd, New_src) =>
        exists P : PK, PP : PK .
            P.src = Hs && P.dst = Hd &&
            tr_src (P, New_src, PP) &&
            sent (S, P, PP, port(1), port(2))

inv ft_nat_trans :
    forall S : SW, P : PK, PP : PK .
        ft (S, P, PP, port(1), port(2)) =>
        translated (S, P.src, P.dst, PP.src) &&
        tr_src (P, PP.src, PP)

inv ft_nat_trans_back :
    forall S : SW, P : PK, PP : PK .
        ft (S, P, PP, port(2), port(1)) =>
        translated (S, PP.dst, P.src, P.dst) &&
        tr_dst (P, PP.dst, PP)

# The following two are not necessary to prove the goal safety invariant
#inv nat_trans_bis :
#    forall S : SW, P : PK, PP : PK . 
#        sent (S, P, PP, port(1), port(2)) =>
#        exists New_src : HO .
#            translated (S, P.src, P.dst, New_src) && tr_src (P, New_src, PP)
#
#inv nat_trans_back :
#    forall S : SW , P : PK, PP : PK . 
#        sent (S, P, PP, port(2), port(1)) =>
#        exists Orig_dst : HO .
#            translated (S, Orig_dst, P.src, P.dst) && tr_dst (P, Orig_dst, PP)

### Goal invariant
inv nat_trans_back_goal :
    forall S : SW , P : PK, PP : PK . 
        sent (S, P, PP, port(2), port(1)) =>
        exists P0 : PK, PP0: PK .
            P0.dst = P.src &&
            tr_src (P0, P.dst, PP0) &&
            tr_dst (P, P0.src, PP) &&
            sent (S, P0, PP0, port(1), port(2))

# TODO: KALEV - duplicate the above constraints for both communication directions.


# ## IPS
# # 

rel ips (PK)

# #
# #####

rel translated_before (SW, HO, HO)

# debugging
var port1 : PR
var port2 : PR
assume port1 = port(1)
assume port2 = port(2)

# debugging
rel translated1 (SW, HO, HO, HO)
rel sent1 (SW, PK, PK, PR, PR)

