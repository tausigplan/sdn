# The Resonance example with learning.
#
# See "Resonance: Dynamic Access Control for Enterprise Networks"
# by Nayak et al. for more details.
#
#
# We model a network of ordinary hosts and distinguished service hosts
# which are authentication servers, scanning servers and update servers.
# Every host in the network has one of four states
#   reg  - host is trying to register
#   auth - host is succesfully authenticated in the network
#   quar - host is quarantined
#   oper - host is operating
#
# Every new host appeared in the network gets a state 'reg' initially.
# In this state, the host is allowed to talk only to authentication servers.
#
# If the host is succesfully authenticated, the state is changed to
# 'auth'.
# Now the host may additionally talk to scanners and update servers.
#
# If the scanner discovers vulnerability on a host, the latter is marked
# as 'quar' and it may talk only to update servers.
#
# The host which successfully passed authentication and scanner phases
# is marked as 'oper'.
# Now it may talk to all operating hosts.
# Authentication, scanning and update servers are always operating.

# Import wired stuff
topo Wired : forall S : SW, P : PK, I : PR . recv(S, P, I) => tph(S, I, P.src)

rel authServ (HO)   # a list of authentication servers
rel scanServ (HO)   # a list of scanners
rel  updServ (HO)   # a list of update servers

var controller : HO # HACK: a dummy 'controller' server

# HACK: _sent represents a set of sent packets right before the event
# occured.
rel _sent (SW, PK, PR, PR)
assume forall S : SW, P : PK, I : PR, O : PR . _sent (S, P, I, O) <=> sent (S, P, I, O)

# The four states of Resonance
rel reg  (HO)       # registration state
rel auth (HO)       # authentication state
rel quar (HO)       # quarantined state
rel oper (HO)       # operation state

rel _reg (HO)
assume forall H : HO . _reg (H) <=> reg (H)
rel _auth (HO)
assume forall H : HO . _auth (H) <=> auth (H)
rel _quar (HO)
assume forall H : HO . _quar (H) <=> quar (H)
rel _oper (HO)
assume forall H : HO . _oper (H) <=> oper (H)

# The states are mutually exclusive.
topo States :
    forall H : HO . (reg (H) => !auth (H) && !quar (H) && !oper (H)) &&
                    (auth (H) => !reg (H) && !quar (H) && !oper (H)) &&
                    (quar (H) => !auth (H) && !reg (H) && !oper (H)) &&
                    (oper (H) => !auth (H) && !quar (H) && !reg (H))

#rel hasState (HO)
#topo HasStateDef :
#    forall H : HO . hasState (H) <=> (reg (H) || auth (H) || quar (H) || oper (H))

# Service hosts are authentication and update servers, and scanners.
rel service (HO)
topo InvService     : forall H : HO . service (H) <=> (authServ (H) || scanServ (H) || updServ (H))
topo InvServiceOper : forall H : HO . service (H) => oper (H) 

# An internal structure for learning connections.
rel connected (SW, PR, HO)
init connected = ()

rel host_of_packet (PK, HO)   # HACK: models passing the host address in a packet
rel type_of_packet (PK, Int)  # HACK: models different kinds of messages
# Two kind of messages are available which are mutually exclusive.
topo InvType0 : forall P : PK . type_of_packet (P, 0) || type_of_packet (P, 1)
topo InvType1 : forall P : PK . type_of_packet (P, 0) <=> !type_of_packet (P, 1)

# The next four formulas are usual invariants for a learning switch.
inv correct_topology:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) && O != null => tph(S, O, P.dst)
inv data_structure_inv_flow_table_1:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) && O != null => connected(S, O, P.dst)
inv data_structure_inv_flow_table_2:
    forall S : SW, P : PK, I : PR, O : PR . ft(S, P, I, O) => connected(S, I, P.src)
inv data_structure_inv_learning:
    forall S : SW, H : HO, I : PR . connected(S, I, H) => tph(S, I, H)

# Flow table rules respect the access policies.
inv ft_consistent_access :
    forall S : SW, P : PK, I : PR, O : PR .
        ft(S, P, I, O) && O != null =>
        (oper(P.src)      && oper (P.dst) ||
         service (P.src)  && auth (P.dst) ||
         authServ (P.src) && reg  (P.dst) ||
         quar(P.src)      && updServ (P.dst) ||
         auth(P.src)      && service (P.dst) ||
         !auth (P.src) && !oper (P.src) && !quar (P.src) && authServ (P.dst))

# Flow table rules respect the access policies.
inv ft_consistent_access_2 :
    forall S : SW, P : PK, I : PR, O : PR .
        ft(S, P, I, null) =>
        !(oper(P.src)      && oper (P.dst) ||
          service (P.src)  && auth (P.dst) ||
          authServ (P.src) && !oper (P.dst) && !auth (P.dst) && !quar (P.dst) ||
          quar(P.src)      && updServ (P.dst) ||
          auth(P.src)      && service (P.dst) ||
          !auth (P.src) && !oper (P.src) && !quar (P.src) && authServ (P.dst))

# Forwarding of packets respects the access policies.
trans flow_drop_access :
    forall S : SW, P : PK, I : PR, O : PR .
        recv (S, P, I) && !_sent (S, P, I, O) &&
        # If the received packet does NOT satisfy the policies...
        !(_oper(P.src)     && _oper (P.dst) ||
          service (P.src)  && _auth (P.dst) ||
          authServ (P.src) && _reg  (P.dst) ||
          _quar(P.src)     && updServ (P.dst) ||
          _auth(P.src)     && service (P.dst) ||
          !_auth (P.src) && !_oper (P.src) && !_quar (P.src) && authServ (P.dst))
        # ... drop it!
        => !sent (S, P, I, O) || O = null

# Forwarding of packets respects the access policies.
trans flow_progress_access :
    forall S : SW, P : PK, I : PR .
        recv (S, P, I) && (exists O1: PR. O1 != I && tph(S, O1, P.dst)) &&
        # If the received packet DOES satisfy the policy..
        (_oper(P.src)     && _oper (P.dst) ||
         service (P.src)  && _auth (P.dst) ||
         authServ (P.src) && _reg (P.dst)  ||
         _quar(P.src)     && updServ (P.dst) ||
         _auth(P.src)     && service (P.dst) ||
         !_auth (P.src) && !_oper (P.src) && !_quar (P.src) && authServ (P.dst)) &&
        # ... and the packet is not a message to the 'controller'..
        !P.dst = controller
        # ... it is correctly forwarded!
        => (exists O2: PR. tph(S, O2, P.dst) && sent(S, P, I, O2))


packetIn (s, p, i) ->
    var o : PR
    var h : HO
    # We assume that the 'controller' never sends packets.
    assume !p.src = controller
    assume !i = null   # This assumption should be added implicitely for every event handler
    if (!auth (p.src) && !quar (p.src) && !oper (p.src)) then {
        # A new host appeared in the network.
        reg.insert (p.src)
    }
    connected.insert (s, i, p.src)
    if reg (p.src) then {
        # Hosts in the registration state are only allowed to establish
        # connection with authentication servers.
        # Currently, we don't model redirection of the web-traffic like
        # in the original Resonance paper.
        if authServ (p.dst) then {
            if connected (s, o, p.dst) then {
                send (s, p, o)
                ft.insert (s, p, i, o)
            } else {
                flood (s, p)
            }
        } else {
            # Drop all other traffic
            ft.insert (s, p, i, null)
        }
    } else if auth (p.src) then {
        # The host was succesfully authenticated.
        # Now it is additionally allowed to talk freely to update and scanner
        # servers.
        if service (p.dst) then {
            if connected (s, o, p.dst) then {
                send (s, p, o)
                # It is OK to install a flow rule, since they will be
                # purged if the source host changes the state to
                # register/quarantined.
                ft.insert (s, p, i, o)
            } else {
                flood (s, p)
            }
        } else {
            # Drop all other traffic
            ft.insert (s, p, i, null)
        }
    } else if quar (p.src) then {
        # Only allow connections to update servers.
        if updServ (p.dst) then {
            if connected (s, o, p.dst) then {
                send (s, p, o)
                ft.insert (s, p, i, o)
            } else {
                flood (s, p)
            }
        } else {
            # Drop all other traffic
            ft.insert (s, p, i, null)
        }
    } else if oper (p.src) then {
        # We got a service message for the controller
        if p.dst = controller && service (p.src) then {
            # The message from an authentication server
            if authServ (p.src) then {
                if host_of_packet (p, h) then {
                    if type_of_packet (p, 0) then {
                        # Authentication failure
                        if reg (h) then {
                        } else if auth (h) then {
                            auth.remove (h)
                            reg.insert (h)
                            ft.remove (*, src: h, *, *)
                            ft.remove (*, dst: h, *, *)
                        # Ignore if the host is quarantined
                        } else if quar (h) then {
                        } else if oper (h) then {
                            oper.remove (h)
                            reg.insert (h)
                            ft.remove (*, src: h, *, *)
                            ft.remove (*, dst: h, *, *)
                        } 
                    } else if type_of_packet (p, 1) then {
                        # The host tried to register and was successfully authenticated.
                        if reg (h) then {
                            reg.remove (h)
                            auth.insert (h)
                            # We remove all the rules with 'h' as a source,
                            # since they may contain drop rules.
                            ft.remove (*, src: h, *, *)
                            ft.remove (*, dst: h, *, *)
                        # Ignore the message otherwise.
                        } else if auth (h) then {
                        } else if quar (h) then {
                        } else if oper (h) then {
                        }
                    }
                }
            }
            # The message from a scanner
            if scanServ (p.src) then { 
                if host_of_packet (p, h) then {
                    if type_of_packet (p, 0) then {
                        # The scanned host is up-to-date.
                        if reg (h) then {
                            # The host is not authenticated yet. Ignore the message.
                        } else if auth (h) then {
                            auth.remove (h)
                            oper.insert (h)
                            ft.remove (*, src: h, *, *)
                            ft.remove (*, dst: h, *, *)
                        } else if quar (h) then {
                            # Ignore for quarantined hosts.
                            # Intervention of the network operator is required
                            # in order to remove the host from the list of quarantined.
                            # Alternatively, we could put 'h' back into the registration state by
                            # quar.remove (h)
                            # reg.insert (h)
                            # In this case, the flow tables and the
                            # access policies must be updated
                            # accordingly!
                        } else if oper (h) then {
                            # Ignore.
                        } 
                    } else if type_of_packet (p, 1) then {
                        # The host is vulnerable/infected.
                        if reg (h) then {
                            # The host is not authenticated yet. Ignore.
                        } else if auth (h) then {
                            auth.remove (h)
                            quar.insert (h)
                            ft.remove (*, src: h, *, *)
                            ft.remove (*, dst: h, *, *)
                        } else if quar (h) then {
                            # The host is quarantined. Ignore.
                        } else if oper (h) then {
                            # Vulnerability is discovered in the operating host.
                            # If it is not infected, mark it as 'auth'.
                            oper.remove (h)
                            auth.insert (h)
                            # Alernatively, e.g., if the infection is revealed,
                            # use quar.insert (h)
                            ft.remove (*, dst: h, *, *)
                            ft.remove (*, src: h, *, *)
                        }
                    }
                }
            }
        } else {
            # We got a 'normal' packet (not a message to the controller)
            # from an operating host. 
            # Talk only to operating destinations
            if oper (p.dst) ||
            # .. or allow incoming service connections.
               service (p.src) && auth (p.dst) ||
               authServ (p.src) && reg (p.dst)
            then {
                if connected (s, o, p.dst) then {
                    send (s, p, o)
                    ft.insert (s, p, i, o)
                } else {
                    flood (s, p)
                }
            }
        }
    }
