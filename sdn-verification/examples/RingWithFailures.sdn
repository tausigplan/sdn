# Network with ring topology

topo PortInj :
    port (0) != port (1) &&
    port (0) != port (2) &&
    port (1) != port (2)

topo Ports : forall P : PR .
    P = port(0) || P = port(1) || P = port(2)

topo Switches :
    exists S1 : SW , S2 : SW, S3 : SW, S4 : SW .
    forall S : SW . S = S1 || S = S2 # || S = S3 || S = S4

topo tph_tphf :
    forall S : SW, H : HO .
        S = tphf (H) <=> tph (S, port(0), H)

# not needed
#topo tph_inj :  
#    forall S : SW, H1 : HO, H2 : HO .
#        S = tphf (H1) && S = tphf (H2) => H1 = H2

# not needed
topo traffic_refl :
    forall S : SW, P : PR, B : BUF . traffic (S, P, B, S, P, B)

# not needed
inv traffic_trans :
    forall S1 : SW, S2 : SW, S3 : SW, P1 : PR, P2 : PR, P3 : PR, B1 : BUF, B2 : BUF, B3 : BUF .
        traffic (S1, P1, B1, S2, P2, B2) &&
        traffic (S2, P2, B2, S3, P3, B3) =>
        traffic (S1, P1, B1, S3, P3, B3)

rel ordSW (SW, PR, BUF, SW, PR, BUF)

## not needed
topo ordSW_0 :
    forall S1 : SW, S2 : SW, I1 : PR, I2 : PR, B1 : BUF, B2 : BUF .
        ordSW (S1, I1, B1, S2, I2, B2) => I1 != port(0) && I2 != port(0)

## not needed
topo ordSW_1 :
    forall S : SW . ordSW (S, port(1), buf(false), S, port(2), buf(true))

# not needed
topo ordSW_lex2 :
    forall S : SW . ordSW (S, port(2), buf(false), S, port(1), buf(true))

# not needed
topo ordSW_refl :
    forall S : SW, I : PR, B : BUF . I != port(0) => ordSW (S, I, B, S, I, B)

# not needed
topo ordSW_antisym :
    forall S1 : SW, I1 : PR, S2 : SW, I2 : PR, B1 : BUF, B2 : BUF .
        ordSW (S1, I1, B1, S2, I2, B2) &&
        ordSW (S2, I2, B2, S1, I1, B1) =>
        S1 = S2 && I1 = I2 && B1 = B2

# not needed
topo ordSW_trans :
    forall S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF, S3 : SW, I3 : PR, B3 : BUF .
        ordSW (S1, I1, B1, S2, I2, B2) &&
        ordSW (S2, I2, B2, S3, I3, B3) =>
        ordSW (S1, I1, B1, S3, I3, B3)

# Non-EPR invariant (doesn't work)
#topo ordSW_step_1 :
#    forall S : SW, S1 : SW, I : PR, I1 : PR, B : BUF, B1 : BUF .
#        ordSW (S, I, B, S1, I1, B1) && (S != S1 || I != I1 || B != B1) &&
#        (forall S2 : SW, I2 : PR, B2 : BUF .
#            ordSW (S, I, B, S2, I2, B2) => ordSW (S1, I1, B1, S2, I2, B2)) =>
#        (S != S1 && I != I1 && B = buf(true) && B1 = buf(false)) ||
#        (S  = S1 && B = buf(false) && B1 = buf(true) && (I = port(2) || I1 = port(2)))

topo ordSW_cons :
    forall S : SW, S1 : SW, P1 : PR, B : BUF, B1 : BUF .
        ordSW (S, port(1), B, S1, P1, B1) && ordSW (S1, P1, B1, S, port(2), buf(true)) => S = S1

topo ordSW_lin_1 :
    forall S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        I1 != port(0) && I2 != port(0) =>
        ordSW (S1, I1, B1, S2, I2, B2) || ordSW (S2, I2, B2, S1, I1, B1)

topo ordSW_lin_2 :
    forall S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        (I1 = port(1) && B1 = buf(false) || I1 = port(2) && B1 = buf(true)) &&
        (I2 = port(1) && B2 = buf(true)  || I2 = port(2) && B2 = buf(false)) =>
        ordSW (S1, I1, B1, S2, I2, B2)

# not needed
topo ordSW_lin_3 :
    forall S1 : SW, I1 : PR, B1 : BUF, S2 : SW, I2 : PR, B2 : BUF .
        (I1 = port(1) && B1 = buf(true) || I1 = port(2) && B1 = buf(false)) &&
        ordSW (S1, I1, B1, S2, I2, B2) =>
        (I2 = port(1) && B2 = buf(true) || I2 = port(2) && B2 = buf(false))

topo ordSW_lin_4 :
    forall S : SW, I : PR, B : BUF .
        ordSW (S, port(1), buf(true), S, I, B) => I = port(1) && B = buf(true)

inv traffic_out :
   forall S0 : SW, P : PR, S : SW, B : BUF .
        traffic (S0, port(0), buf(true), S, P, B) =>
        S = S0 && P = port(0) && B = buf(true)

inv traffic_ord :
    forall S1 : SW, P1 : PR, B1 : BUF, S2 : SW, P2 : PR, B2 : BUF .
    P1 != port(0) && P2 != port(0) &&
    traffic (S1, P1, B1, S2, P2, B2) => ordSW (S1, P1, B1, S2, P2, B2) 

#inv traffic_acyclicity :
#    forall S1 : SW, S2 : SW, P1 : PR, P2 : PR  .
#        traffic (S1, P1, buf(false), S2, P2, buf(false)) &&
#        traffic (S2, P2, buf(false), S1, P1, buf(false)) =>
#        S1 = S2 && P1 = P2

#inv traffic_acyclicity_2 :
#    forall S1 : SW, S2 : SW, P1 : PR, P2 : PR  .
#        traffic (S1, P1, buf(true), S2, P2, buf(true)) &&
#        traffic (S2, P2, buf(true), S1, P1, buf(true)) =>
#        S1 = S2 && P1 = P2

rel traffic_new (SW, PR, BUF, SW, PR, BUF)
rel traffic_diff (SW, PR, BUF, SW, PR, BUF)

var Port0 : PR
var Port1 : PR
var Port2 : PR
var In  : BUF
var Out : BUF

packetIn(s, p, i) ->
    assume Port0 = port(0)
    assume Port1 = port(1)
    assume Port2 = port(2)
    assume In  = buf(false)
    assume Out = buf(true)
    assume forall S1 : SW, S2 : SW, P1 : PR, P2 : PR, B1 : BUF, B2 : BUF .
        traffic_diff (S1, P1, B1, S2, P2, B2) <=>
        traffic_new (S1, P1, B1, S2, P2, B2) && !traffic (S1, P1, B1, S2, P2, B2)
    if i = port(0) then {
        assume s = tphf (p.src)
        if tph (s, port(0), p.dst) then {
            ft.insert(s, p, port(0), port(0))
            send (s, p, port(0))
        } else {
            if link (s, port(2)) then {
                # the link is up
                ft.insert(s, p, port(0), port(2))
                send (s, p, port(2))
            } else {
                # the link is down
                ft.insert(s, p, port(0), port(1)) 
                send (s, p, port(1))
            }
        }
    } else {
        if i = port(1) then {
            if tph (s, port(0), p.dst) then {
                ft.insert(s, p, port(1), port(0))
                send (s, p, port(0))
            } else {
                if link (s, port(2)) then {
                    # the link is up
                    ft.insert(s, p, port(1), port(2))
                    send (s, p, port(2))
                } else {
                    # the link is down
                    ft.insert(s, p, port(1), port(1))
                    send (s, p, port(1))
                }
            }
        } else {
            if i = port(2) then {
                if tph (s, port(0), p.dst) then {
                    ft.insert(s, p, port(2), port(0))
                    send (s, p, port(0))
                } else {
                    # we send the packet even if the link is down
                    ft.insert(s, p, port(2), port(1))
                    send (s, p, port(1))
                }
            }
        }
    }
    assume forall S1 : SW, S2 : SW, P1 : PR, P2 : PR, B1 : BUF, B2 : BUF .
        traffic_new (S1, P1, B1, S2, P2, B2) <=> traffic (S1, P1, B1, S2, P2, B2)

