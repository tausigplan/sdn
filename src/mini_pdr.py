# This is a mini-version of PDR specialized to
# EPR style transition systems and using
# a heuristic to extract quantifier-free invariants
# (so far quantifier-free, an extension is to be for quantifiers).

from z3 import *
import heapq
import copy
from z3support.vocabulary import Z3Alphabet, Z3Renaming, Z3TwoVocabulary


def is_subset(A, B):
    for x in A:
        if x not in B:
            return False
    return True

def get_id(f):
    return Z3_get_ast_id(f.ctx.ref(), f.as_ast())


# General purpose SMT utilities
class SMT:
    def check_assumptions(self, s, assumptions):
        preds = [ Const('p%d' % j, BoolSort()) for j in range(len(assumptions)) ]
        for j in range(len(assumptions)):
            s.add(preds[j] == assumptions[j])
        r = s.check(preds)
        if r == sat:
           return (sat, [])
        preds2assumptions = {}
        for j in range(len(assumptions)):
            preds2assumptions[get_id(preds[j])] = assumptions[j]
        core = self.minimize_core(s)
        core = [preds2assumptions[get_id(c)] for c in core]
        return (unsat, core)
        
    def minimize_core(self, s):
        core = [ c for c in s.unsat_core()]
        ids = { get_id(c):c for c in core }
        seed = [ get_id(c) for c in core]
        current = set(seed)
        for i in seed:
            if i not in current:
                continue
            current.remove(i)
            if unsat == s.check([ids[id] for id in current]):  # @ReservedAssignment
                current = set ([get_id(c) for c in s.unsat_core()])
            else:
                current.add(i)
        return [ids[id] for id in current]  # @ReservedAssignment



class PDR:
    def __init__(self, init, rho, bad, background, globals, locals, relations):  # @ReservedAssignment
        init_id = get_id(init)
        #init_id = get_id(BoolVal(False))    # seems to work just as well
        self.N = 1
        self.Rs = [set([init_id]),set([])]
        self.Init = init
        self.Rho = rho
        self.Bad = bad
        self.background = background
        self.Globals = globals
        self.Locals = Z3TwoVocabulary.promote(locals)
        self.Relations = relations
        self.Univ = []
        self.ground_properties = []
        self.fmls = { init_id: init}
        self.goals = []
        self.smt = SMT()
        self.counterexample = None
        print "init", init
        print "rho", rho
        print "bad", bad
        
        self.iteration_count = 0
        self.sat_query_count = 0
        
        self._opt_Rs_last_induction = []
        #for n in self.Relations:
        #    for args1 in self.populate_args(n, 0, [], []):
        #        self.ground_properties += [n(args1)]

        

    def run(self):
        if not self.check_init():
            return False
        while True:
            self.iteration_count += 1
            if self.is_valid():
                print "valid"
                return True
            if self.unfold():
                self.induction()
                continue
            while self.goals != []:
                (i, props) = self.goals[0]                
                if i == 0:
                    for g in self.goals: print g
                    #print self.goals
                    print "trace"
                    return False
                s = Solver()
                s.add(self.background)
                s.add(Or(self.Init, And(self.rename(self.R(i-1)), self.Rho)))
                #print self.rename(self.R(i-1))
                self.sat_query_count += 1
                (r, core) = self.smt.check_assumptions(s, props)
                if unsat == r:
                    lemma = Not(And(core)) #Or([Not(e) for e in core])
                    if self.Univ:
                        lemma = ForAll(self.Univ, lemma)
                    lemma_id = get_id(lemma)
                    # We Should really do this:
                    for j in range(i+1):
                        self.Rs[j].add(lemma_id)
                    # as opposed to this
                    # self.Rs[i].add(lemma_id)
                    self.fmls[lemma_id] = lemma
                    print "lemma: ", i, lemma
                    # simplification: IC3 uses the priority queue and
                    # re-inserts the goal with a higher index as long
                    # as this is below self.N
                    heapq.heappop(self.goals)
                elif sat == r:
                    m = s.model()
                    props1 = self.get_properties(m, True)
                    #print i-1, props1
                    heapq.heappush(self.goals, (i-1, props1))
                else:
                    raise RuntimeError, 'unknown result (%s)' % r

    def check_init(self):
        s = Solver()
        s.add(self.background)
        s.add(self.Init)
        s.add(self.Bad)
        self.sat_query_count += 1
        if sat == s.check():
            print "initial condition is not safe"
            self.counterexample = s.model()
            return False
        return True


    def is_valid(self, prune=True):
        for i in range(self.N):
            if self.Rs[i].issubset(self.Rs[i+1]):
                if prune:
                    self.prune(i)
                    print "R[%d] =" % i
                    print self.R(i)
                return True
        return False

    #
    # A very simplistic version of inductive strengthening.
    # 
    def induction(self):
        last_Rs = self._opt_Rs_last_induction
        for i in range(len(self.Rs)-1):
            if i < len(last_Rs) and last_Rs[i] == self.Rs[i]: continue   # optimization
            Rs_i = self.Rs[i].copy()
            for lemma_id in Rs_i:
                if lemma_id not in self.Rs[i+1] and self.is_inductive(self.fmls[lemma_id], i):
                    print "Strengthen ", (i+1), self.fmls[lemma_id]
                    # A naive approach would be:
                    #self.Rs[i+1].add(lemma_id)
                    # But this one does something more sophisticated:
                    self.strengthen(lemma_id, i)
            if self.is_valid(prune=False): break  # optimization
                    
        self._opt_Rs_last_induction = copy.deepcopy(self.Rs)
                        
    def is_inductive(self, lemma, i):
        s = Solver()
        s.add(self.background)
        s.add(Or(self.Init, And(self.rename(And(self.R(i), lemma)), self.Rho)))
        s.add(Not(lemma))
        self.sat_query_count += 1
        return unsat == s.check()
    
    def strengthen(self, lemma_id, i):
        lemma = self.fmls[lemma_id]
        literals = [lemma]
        if is_or(lemma):
            literals = lemma.children()
        old_length = len(literals)
        j = 0
        while j < len(literals):
            save = literals[j]
            literals[j] = BoolVal(False)
            if self.is_inductive(Or(literals), i):
                literals[j] = literals[-1]
                literals.pop()
            else:
                literals[j] = save
                j += 1
        if old_length == len(literals):
            self.Rs[i+1].add(lemma_id)
        else:
            for j in range(i+1):
                self.Rs[j].remove(lemma_id)        
            lemma = Or(literals)
            lemma_id = get_id(lemma)
            self.fmls[lemma_id] = lemma
            for j in range(i+2):
                self.Rs[j].add(lemma_id)

    def prune(self, i):
        removed = []
        for j in self.Rs[i]:
            s = Solver()
            s.add(self.background)
            for id in self.Rs[i]:  # @ReservedAssignment
                if j == id:
                    s.add(Not(self.fmls[id]))
                elif id in removed:
                    pass
                else:
                    s.add(self.fmls[id])
            self.sat_query_count += 1
            if unsat == s.check():
                removed += [j]
        self.Rs[i] = set([ j for j in self.Rs[i] if j not in removed])

    def unfold(self):
        s = Solver()
        s.add(self.background)
        s.add(self.Bad)
        s.add(self.R(self.N))
        self.sat_query_count += 1
        if unsat == s.check():
            self.N += 1
            self.Rs += [set([])]
            return True
        m = s.model()
        heapq.heappush(self.goals, (self.N, self.get_properties(m, False)))

    def R(self, i):
        fmls = [self.fmls[id] for id in self.Rs[i]]  # @ReservedAssignment
        if fmls == []:
            return True
        return And(fmls)

    #
    # Return set of EPR properties associated with 'm'
    # when projected to current state variables.
    # really simplistic implementation (equalities can be done
    # more efficiently).
    # This is the quantifier-free version (so not all models
    # will be extended to real counter-examples)
    #
    def toggle_pred(self, m, p, q):
        p_eval = m.eval(p)
        # print p_eval
        if is_true(p_eval):
            return q
        if is_app_of(q, Z3_OP_EQ):
            return (q.arg(0) != q.arg(1))  # looks nicer
        return (Not(q))

    def insert_value(self, vl, x, roots, ps):
        v = get_id(vl)
        if is_true(vl):
           ps += [x]
        elif is_false(vl):
           ps += [Not(x)]
        elif v not in roots:
           roots[v] = x
        else:       # equal terms always have the same sort
           y = roots[v]    
           ps += [y == x]       
        
    def get_properties(self, m, is_transition):
        roots = {}
        ps = []
        for (x0, x) in self.Locals:
            if not is_const(x): continue
            if not is_transition:
                x0 = x
            vl = m.eval(x0)
            self.insert_value(vl, x, roots, ps)
        for y in self.Globals:
            vl = m.eval(y)
            self.insert_value(vl, y, roots, ps)
        # we can also use distinct clause.
        for v in roots:
            for w in roots:
                if v != w:
                    x, y = roots[v], roots[w]
                    if get_id(x.sort()) == get_id(y.sort()):
                        ps += [x != y]
        if is_transition:
            ps += [self.toggle_pred(m, self.rename(prop), prop)
                   for prop in self.ground_properties]
        else:
            ps += [self.toggle_pred(m, prop, prop) 
                   for prop in self.ground_properties]

        return ps

    def populate_args(self, r, i, args, res):
        if i == r.arity():
            return res + [args]
        for _, x in self.Locals:
            if get_id(x.sort()) == get_id(r.domain(i)):
                res = self.populate_args(r, i+1, args + [x], res)
        for y in self.Globals:
            if get_id(y.sort()) == get_id(r.domain(i)):
                res = self.populate_args(r, i+1, args + [y], res)
        return res

    def rename(self, term):
        return self.Locals.rename_past_tense(term)

          
def nPlus(x,y):
    return And(x != y, n(x,y))

if __name__ == '__main__':
    A = DeclareSort('A')
    B = BoolSort()
    n = Function('n', A, A, B)
    
    x, y, l, h, x0, z = Consts('x y l h x0 z', A)
    init    = And(x == h, Implies(n(h, y), nPlus(y, l)))
    rho     = And(nPlus(x0, x), ForAll([z], Implies(nPlus(x,z),n(x0,z))))
    bad     = And(x == y, x == l)
    background = ForAll([z], n(z,z))
    globals = [y, l, h]    # @ReservedAssignment
    locals  = [(x0,x)]  # @ReservedAssignment
    print globals
    print locals
    print rho
    print background
    print init
    print bad
    
    pdr = PDR(init, rho, bad, background, globals, locals, [n])
    pdr.run()
