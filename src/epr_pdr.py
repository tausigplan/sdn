# encoding=utf-8
import linker
from z3 import *

from logic.fol.semantics.extensions.sorts import FolManySortSignature, FolSorts
from synopsis.declare import TypeDeclarations
from pydoc import synopsis
from synopsis.proof import SynopsisFormulaParser, Expansion, TCSpecific
from logic.fol.syntax.formula import FolFormula
from logic.fol.semantics.extensions.equality import FolWithEquality
from logic.fol.syntax import Identifier
from horn_blasting import WeakestPreSynthesis, wps_formula_get, define_symbol
from synopsis.af_comp import detptr_libraries
from logic.fol.syntax.scheme import FolScheme, FolSubstitution
from ui.text.table import Table
from logic.fol.syntax.transform import AuxTransformers
from shape2horn import hoist_forall
from synopsis.stats import Stopwatch
from pattern.collection.basics import OneToOne
from logic.fol.semantics.graphs import AlgebraicStructureGraphTool

from z3support.adapter import Z3ModelToFolStructure
from mini_bmc import BMC


#------------------------------------------------------------------------------#
#  Declarations
#

impSort = V = DeclareSort('V')
n       = Function('n*', V, V, BoolSort())
n0      = Function('n*0', V, V, BoolSort())
n_prime = Function('n*@', V, V, BoolSort())
eqrel   = Identifier('eqrel', 'macro')
eqpred  = Identifier('eqpred', 'macro')

def nPlus(x,y):
    return And(x != y, n(x,y))

#
#------------------------------------------------------------------------------#


class Z3Gate(object):

    def __init__(self):
        self.z3_sort_map = OneToOne({'bool': BoolSort()}).of(self._mksort)
        self.z3_decls = {}
        self.signature = FolManySortSignature()
        self.expansion = None

    def _mksort(self, sort):
        if isinstance(sort, Identifier): sort = sort.literal
        return DeclareSort(sort)

    def define_symbols(self, signature):
        """
        @param signature: an FolManySortSignature instance
        """
        self.signature |= signature
        return {sym: self.define_symbol(sym, (from_, to))
                for sym, (from_, to) in signature.sorts.iterdefs()}

    def define_symbol(self, sym, (from_, to)):
        if from_:
            return Function(sym.literal, *(self.z3_sort_map[x] for x in list(from_)+[to]))
        else:
            return Const(sym.literal, self.z3_sort_map[to])
        
    def formula(self, fol_formula):
        if isinstance(fol_formula, (str, unicode)):
            fol_formula = FolFormula.conjunction(self.expansion([fol_formula]))
        f_aux = AuxTransformers.renumber_bound_vars( fol_formula )
        return fol_formula_to_z3(f_aux, self.z3_decls)
    
    def structure(self, z3_model, signature=None):
        if signature is None:
            signature = self.signature
        return Z3ModelToFolStructure(signature, z3g.z3_decls)(z3_model)



def fol_formula_to_z3(f,decls):
    flen = len( f.subtrees )
    if f.root in decls:
        fsym = decls[f.root]
        args = [fol_formula_to_z3(a, decls) for a in f.subtrees]
        return fsym(*args) if args else fsym
    elif flen == 0:
        if f.root == 'true':
            return True
        elif f.root == FolFormula.FALSE or f.root == 'false':
            return False
        else:
            #raise KeyError, "undeclared constant '%s'" % f
            return Const(f.root.literal, impSort)
    elif flen == 1:
        if f.root == FolFormula.NOT:
            arg = fol_formula_to_z3( f.subtrees[0], decls)
            #Hack, was: return Not( arg )
            if 0:
                if isinstance(arg, bool ):
                    return True
                else:
                    return Not( arg )
            #Hack, was: return Not( arg )
            return Not( arg )
#        elif f.root == 'C':
#            arg = fol_formula_to_z3( f.subtrees[0], decls)
#            res = Select(C, arg)
#            return res
        else:
            raise ValueError, 'unknown formula: %s' %f
    else:
        lhs = fol_formula_to_z3( f.subtrees[0], decls)
        rhs = fol_formula_to_z3( f.subtrees[1], decls)
        if f.root == FolFormula.IMPLIES:
            res = Implies( lhs,rhs )
            return res
        elif f.root == FolFormula.AND:
            res = And( lhs,rhs )
            return res
        elif f.root == FolFormula.IFF:
            return lhs == rhs

        elif f.root == '=':
            return lhs == rhs
        elif f.root == '!=' or f.root == FolWithEquality.neq:
            return lhs != rhs
        elif f.root == FolFormula.OR:
            res = Or( lhs,rhs )
            return res
        elif f.root == 'n*':
            res = n( lhs,rhs )
            return res
        elif f.root == 'n*0':
            res = n0( lhs,rhs )
            return res
        elif f.root == "n*'":
            res = n_prime( lhs,rhs )
            return res
        elif f.root == FolFormula.FORALL:
            if is_quantifier(rhs) and rhs.is_forall():
                vars0 = [ Const(rhs.var_name(i), rhs.var_sort(i)) for i in range(rhs.num_vars())]
                vars0.reverse()
                b = substitute_vars(rhs.body(), *vars0)
                return ForAll( [lhs]+vars0, b )
            else:
                return ForAll( [lhs],rhs)
        elif f.root == FolFormula.EXISTS:
            if is_quantifier(rhs) and rhs.is_exists():
                vars0 = [ Const(rhs.var_name(i), rhs.var_sort(i)) for i in range(rhs.num_vars())]
                vars0.reverse()
                b = substitute_vars(rhs.body(), *vars0)
                return Exists( [lhs]+vars0, b )
            else:
                return Exists( [lhs],rhs)
        else:
            print 'unknown f.root', f.root,f
            raise SystemExit



def prime_equillibrium(vocab,t, aux):
    #print t.sorts
    res = []
    
    prime = lambda x: Identifier("%s'" % x.literal, x.kind)
    vocab_prime = [(x,prime(x)) for _,x in vocab]
    
    for v,vprime in vocab_prime:
        if v == 'alpha' or v in aux: continue
        #t.sorts[v] == [((), 'bool')]: continue
        if  tuple(t.sorts[v][0].from_) == ("V","V"):
            f = FolFormula( eqrel,[FolFormula(v),FolFormula(vprime)] )
        elif  tuple(t.sorts[v][0].from_) == ("V",):
            f = FolFormula( eqpred,[FolFormula(v),FolFormula(vprime)] )
        else:
            f = FolFormula( FolWithEquality.eq,[FolFormula(v),FolFormula(vprime)] )
        res.append(f)

    return FolFormula.conjunction(res), vocab_prime


def transition_system(wps, vocab, t):
    fin, vocab_prime = prime_equillibrium(vocab.locals, t, set())
    L = wps.syn.expansion.expansion.parser
    rho = FolScheme(L*'wp_ea(loopBody,[?trans])')(fin)
    rho0 = FolFormula.conjunction(wps.syn.expansion([rho]))
    # set back in time; x->x0, x'->x
    rho0 = FolSubstitution({x:x0 for x0,x in vocab.locals})(rho0)
    rho0 = FolSubstitution({xprime:x for x,xprime in vocab_prime})(rho0)
    return rho0


def extract_states(model, pre, post):
    m = model

    domain = m.get_universe(impSort)

    def repr_element(x):
        return unicode(x).replace("V!val!", ":")

    for br in [pre, post]:
        t = Table()
        t.header = [br] + map(repr_element, domain)
        t.data += [[repr_element(x)] + ['x' if is_true(m.eval(br(x,y))) else '' for y in domain] for x in domain]
        print t


def generate_z3_condition(folformula,wps,decls):
    c = list( wps.syn.expansion([folformula]) )
    f = FolFormula.conjunction( c )
    f_aux = AuxTransformers.renumber_bound_vars( f )
    f_z3 = fol_formula_to_z3( f_aux, decls)
    return hoist_forall(f_z3)


def generate_gamma(wps,decls):
    L     = SynopsisFormulaParser()
    gamma   = (L*'dtca(n*) & dtca(n*0)') # & forall u v w (n*(v,u) & n*(w,u) -> n*(v,w) | n*(w,v))')
    return generate_z3_condition(gamma, wps, decls)



def generate_named_properties(name_property_list, type_decls):
    named_axioms = []

    for name, property_formula in name_property_list:
        p = Identifier(name, 'predicate')
        type_decls.sorts[p] = [((),'bool')]
        axiom = FolFormula(FolFormula.IFF, [FolFormula(p), property_formula])
        named_axioms += [(p,axiom)]

    return named_axioms 
    

def past_tense(consts, type_decls):
    consts0 = []
    for p in consts: 
        p0 = Identifier('%s0' % p, p.kind)
        type_decls.sorts[p0] = type_decls.sorts[p][:]
        consts0 += [p0]
    
    return consts0
        

def generate_n_properties(wps, consts, vocab):
    """
    Extra properties:  {n(u,v) | u,v program variables + null}
    """
    ntot_template = FolFormula.conjunction(map(FolScheme, wps.syn.expansion("ntot_([?u],[?v])")))

    extras = [('n%s%s' % (u,v), ntot_template(u,v))
              for u in consts for v in consts]

    return AbstractionProperties.from_named_formulas(extras, vocab)


def generate_p_properties(wps, consts, vocab):
    """
    Extra properties:  {n(u,v) | u,v program variables + null}
    """
    ptot_template = FolFormula.conjunction(map(FolScheme, wps.syn.expansion("ptot_([?u],[?v])")))

    extras = [('p%s%s' % (u,v), ptot_template(u,v))
              for u in consts for v in consts]

    return AbstractionProperties.from_named_formulas(extras, vocab)


def generate_nstar_properties(wps, consts):
    nstar_template = FolScheme(wps.syn * "n*([?u],[?v])")
    return generate_binary_properties_from_template(nstar_template, consts)

def generate_binary_properties_from_template(template, consts):
    return [template(u,v)
              for u in consts for v in consts]
    

def generate_unary_pred_properties(type_decls, preds, consts):
    ts = type_decls.sorts
    for p in preds:
        for (from_, to) in ts.ary(p, 1):
            if to in ['', 'bool']:
                arg_sort = from_[0]
                for c in consts:
                    if type_decls.sorts.returns(c, arg_sort):
                        yield FolFormula(p, [FolFormula(c)])

def generate_binary_pred_properties(type_decls, preds, consts):
    ts = type_decls.sorts
    for p in preds:
        if p == 'F0': continue  # hack
        for (from_, to) in ts.ary(p, 2):
            if to in ['', 'bool']:
                arg_sort0, arg_sort1 = from_
                for c0 in consts:
                    for c1 in consts:
                        if type_decls.sorts.returns(c0, arg_sort0) and \
                           type_decls.sorts.returns(c1, arg_sort1):
                            yield FolFormula(p, [FolFormula(c0), FolFormula(c1)])


def generate_order_and_sorting_properties(wps, vocab):
    sorted_templ = FolScheme( wps.syn * "sorted(ivlco([?u],[?v],n*))" )
    null = Identifier('null', 'function')
    a = AbstractionProperties.from_named_formulas(
           [('sorted%s%s' % (u,v), sorted_templ(u,v)) 
            for u in vocab.consts for v in [null]], vocab)
    #a.props += generate_binary_properties_from_template\
    #        (FolScheme(wps.syn * "R([?u],[?v])"), vocab.consts)
    return a

def generate_stability_properties(wps, vocab):
    stable_templ = FolScheme( wps.syn * "stable([?u])" )
    a = AbstractionProperties.from_named_formulas(
           [('stable%s' % u, stable_templ(u)) for u in vocab.consts], vocab)
    return a


def generate_rev_properties(wps, vocab):
    np_rev_templ = FolScheme( wps.syn * "subrev(ivlco([?u],null,n*),n*,p*)")
    pn_rev_templ = FolScheme( wps.syn * "subrev(ivlco([?u],[?v],n*),p*,n*)")
    a = AbstractionProperties.from_named_formulas(
           [('n_p_subrev%s' % u, np_rev_templ(u)) for u in vocab.consts], vocab)
    # There is some scalability issue here,
    # so these are currently turned off
    iv = []
    #iv = vocab.consts
    #iv = [v for v in vocab.consts if v in ['i','j']]   # some ad-hoc compromise
    a += AbstractionProperties.from_named_formulas(
           [('p_n_subrev%s%s' % (u,v), pn_rev_templ(u,v)) 
            for u in vocab.consts for v in iv], vocab)
    return a
    

def id_to_z3(x):
    return Const(x.literal, impSort)


def unzip(list_of_tuples, n):
    """ Auxiliary function: 
    [(a1,b1,c1), (a2,b2,c2),...] --> [a1,a2,...], [b1,b2,...], [c1,c2,...]
    """
    ls = [[] for _ in xrange(n)]
    for t in list_of_tuples:
        for i,x in enumerate(t):
            ls[i].append(x)
    return ls


def display_structure(structure, vocab):
    from adt.graph.format import DEFAULT as FORMATTER
    
    m = structure
    m.domain = m.domain['V']
    print "V = %s" % (sorted(m.domain),)
    for c in vocab.consts:
        try:
            print "%s = %s" % (c, m.interpretation[c])
        except KeyError:
            pass
    for nstar in vocab.preds:
        if vocab.type_declarations.sorts.ary(nstar, 2):
            try:
                n = TCSpecific.restore_edge_relation_from_tc(m, rtc_relation_name=nstar)
            except (KeyError, ValueError):
                continue
            print FORMATTER(AlgebraicStructureGraphTool(m, [n])())
   

def report_failure(pdr, z3g, vocab):
    if pdr.counterexample:
        print '-' * 80
        display_structure(z3g.structure(pdr.counterexample), vocab)
        print '-' * 80
    else:
        print 'Looking for concrete error trace...'
        bmc = BMC(pdr.Init, pdr.Rho, pdr.Bad, pdr.background, pdr.Locals)
        m = bmc.find_concrete_trace(pdr.N)
        if m:
            for m_i in m:
                print '-' * 80
                display_structure(z3g.structure(m_i), vocab)
            print '-' * 80
        else:
            print "no concrete trace found (abstraction too coarse)"


WP_EA = """
wp_ea(`skip, Q) := Q
wp_ea([x:=y](x, y), Q) := dr(x := y, Q)
wp_ea([x:=null](x), Q) := dr(x := null, Q)
wp_ea([x.n|=y](x, `n, y), Q) := x!=null & ~n*(y,x) & dr(n*(u,v) := n*(u,v) | (y!=null & n*(u,x) & n*(y,v)), Q)
wp_ea([x.n:=null](x, `n), Q) := x!=null & dr(n*(u,v) := n*(u,v) & (~n*(u,x) | n*(v,x)), Q)
wp_ea([x:=y.n](x, y, `n), Q) := y != null &
   exists z (ntot_(y,z) & dr(x := z, Q))
wp_ea([;](s1, s2), Q) := wp_ea(s1, wp_ea(s2, Q))
wp_ea(if(C, s1, s2), Q) := ite(C, wp_ea(s1, Q), wp_ea(s2, Q))
wp_ea(assume(K), Q) := K -> Q
wp_ea(return(x), Q) := dr(retval:=x, Q)
wp_ea([x:=new](x), Q) :=
   (forall z (z = null | alloc(z)) & dr(x := null, Q)) |
   exists z ((z != null & ~alloc(z)) & dr(x := z, alloc(u) := alloc(u) | u=z, Q))
wp_ea([r:=f(...)](_, free(x)), Q) := x != null & alloc(x) & dr(alloc(u) := alloc(u) & u!=x, Q)

# Same for field p @@@
wp_ea([x.n|=y](x, `p, y), Q) := x!=null & ~p*(y,x) & dr(p*(u,v) := p*(u,v) | (y!=null & p*(u,x) & p*(y,v)), Q)
wp_ea([x.n:=null](x, `p), Q) := x!=null & dr(p*(u,v) := p*(u,v) & (~p*(u,x) | p*(v,x)), Q)
wp_ea([x:=y.n](x, y, `p), Q) := y != null &
   exists z (ptot_(y,z) & dr(x := z, Q))
   

wp_ae(`skip, Q) := Q
wp_ae([x:=y](x, y), Q) := dr(x := y, Q)
wp_ae([x:=null](x), Q) := dr(x := null, Q)
wp_ae([x.n|=y](x, `n, y), Q) := x!=null & ~n*(y,x) & dr(n*(u,v) := n*(u,v) | (y!=null & n*(u,x) & n*(y,v)), Q)
wp_ae([x.n:=null](x, `n), Q) := x!=null & dr(n*(u,v) := n*(u,v) & (~n*(u,x) | n*(v,x)), Q)
wp_ae([x:=y.n](x, y, `n), Q) := y != null &
   forall z (ntot_(y,z) -> dr(x := z, Q))
wp_ae([;](s1, s2), Q) := wp_ae(s1, wp_ae(s2, Q))
wp_ae(if(C, s1, s2), Q) := ite(C, wp_ae(s1, Q), wp_ae(s2, Q))
wp_ae(assume(K), Q) := K -> Q
wp_ae(return(x), Q) := dr(retval:=x, Q)
wp_ae([x:=new](x), Q) :=
   (forall z (z = null | alloc(z)) -> dr(x := null, Q)) &
   forall z ((z != null & ~alloc(z)) -> dr(x := z, alloc(u) := alloc(u) | u=z, Q))
wp_ae([r:=f(...)](_, free(x)), Q) := x != null & alloc(x) & dr(alloc(u) := alloc(u) & u!=x, Q)

# Same for field p @@@
wp_ae([x.n|=y](x, `p, y), Q) := x!=null & ~p*(y,x) & dr(p*(u,v) := p*(u,v) | (y!=null & p*(u,x) & p*(y,v)), Q)
wp_ae([x.n:=null](x, `p), Q) := x!=null & dr(p*(u,v) := p*(u,v) & (~p*(u,x) | p*(v,x)), Q)
wp_ae([x:=y.n](x, y, `p), Q) := y != null &
   forall z (ptot_(y,z) -> dr(x := z, Q))

ite(c,t,e) := (c -> t) & (~c -> e)

"""


EXTRA_PROP_MACROS = """
stable(p) := forall u v (n*(p,u) & alloc(u) & n*(u,v) -> alloc(v))
sorted(ivl) := forall u v (u!=null & v!=null & in(u,ivl) & in(v,ivl) & R(u,v) -> n*(u,v))
subrev(ivl,r,s) := forall u v (in(u,ivl) & in(v,ivl) & s(u,v) -> r(v,u))
"""



class TwoVocabulary(object):
    
    def __init__(self):
        self.locals = []
        self.globals = []
        self.aux = set()
        self.type_declarations = TypeDeclarations()

    def __lshift__(self, formulas):
        trans_decls = []
        for phi in formulas:
            if phi.root == FolFormula.IMPLIES and len(phi.subtrees) == 2:
                if all(self.type_declarations.is_declaration(d) for d in phi.subtrees):
                    trans_decls += [phi]
                    continue
            yield phi

        t = self.type_declarations
        t.read_from([x for d in trans_decls for x in d.subtrees])
        self.locals += [tuple(x.subtrees[0].root for x in d.subtrees)
                        for d in trans_decls]
        self.globals = [s for s in t.symbols if s not in self.locals_flat]

    @property
    def locals_flat(self):
        return set(x for d in self.locals for x in d)

    @property
    def consts(self):
        return [z for z in set(x for _,x in self.locals) | set(self.globals)
                if t.sorts.returns(z, 'V')]
        
    @property
    def preds(self):
        return [z for z in set(x for _,x in self.locals) | set(self.globals)
                if t.sorts.returns(z, 'bool')]



class AbstractionProperties(object):
    
    def __init__(self):
        self.props = []
        self.axioms = []
        self.axioms0 = []
        
    def __iadd__(self, other):
        self.props += other.props
        self.axioms += other.axioms
        self.axioms0 += other.axioms0
        return self
        
    @classmethod
    def from_named_formulas(cls, named_formulas, vocab):
        """
        @param named_formulas:  a list of (name, formula) where 'name' is a string
          and 'formula' is a boolean formula.
        """
        a = cls()
        p, a.axioms = unzip(generate_named_properties(named_formulas, vocab.type_declarations), n=2)
        vocab.locals += zip(past_tense(p, t), p)
    
        rename_subst = FolSubstitution({x: x0 for (x0, x) in vocab.locals})
        a.axioms0 = map(rename_subst, a.axioms)
        
        #a = cls()
        #a.props, a.axioms0, a.axioms = [], ntot0_axioms, ntot_axioms
        vocab.aux |= set(p)
        return a




if __name__ == '__main__':

    import argparse

    a = argparse.ArgumentParser()
    a.add_argument('filename', type=str, nargs='?')
    args = a.parse_args()
    
    if args.filename:
        program = open(args.filename).read()
    else:
        import os.path
        here = os.path.dirname(__file__)
        program = open(os.path.join(here, '..', 'benchmarks', 'pdr', 'sll-sorted-insert.imp')).read()



    wps   = WeakestPreSynthesis()
    wps.configure(detptr_libraries())
    wps.syn.libs += [WP_EA, EXTRA_PROP_MACROS]

    t = wps.syn.type_declarations

    vocab = TwoVocabulary()
    vocab.type_declarations = t
    
    axioms = list(vocab << wps.syn.first_pass(program))

    locals_vocab = vocab.locals
    locals_vocab_flat = vocab.locals_flat
    globals_vocab = vocab.globals
    vocab_consts = vocab.consts

    trans = transition_system(wps, vocab, t)
    cond = FolFormula.conjunction(wps.syn.expansion("cond"))
    cond0 = FolSubstitution({x:x0 for x0,x in locals_vocab})(cond)

    #extra_props, extra_axioms0, extra_axioms = [], [], []
    extra = generate_n_properties(wps, vocab_consts, vocab)

    extra.props += list(generate_unary_pred_properties(t, vocab.preds, vocab_consts))
    extra.props += list(generate_binary_pred_properties(t, vocab.preds, vocab_consts))

    extra.axioms += axioms 

    # Stability (absence of dangling pointers)
    if FolSorts.FunctionType.parse(u'V→bool') in t.sorts.ary('alloc', 1):
        extra += generate_stability_properties(wps, vocab)

    # Properties for order and sorting
    if FolSorts.FunctionType.parse(u'V×V→bool') in t.sorts.ary('R', 2):
        extra += generate_order_and_sorting_properties(wps, vocab)

    if FolSorts.FunctionType.parse(u'V×V→bool') in t.sorts.ary('p*', 2):
        #extra += generate_p_properties(wps, vocab_consts, vocab)
        extra += generate_rev_properties(wps, vocab)


    # --- Now send everything to PDR + Z3

    z3g = Z3Gate()
    z3g.z3_decls = decls = z3g.define_symbols(t)
    z3g.expansion = wps.syn.expansion

    extra_props = [z3g.formula(FolFormula.promote(p)) for p in extra.props]
    extra_axioms = [fol_formula_to_z3(phi, decls) for phi in extra.axioms]
    extra_axioms0 = [fol_formula_to_z3(phi, decls) for phi in extra.axioms0]

    init       = generate_z3_condition(" init ", wps, decls)#And(x == h, Implies(n(h, y), nPlus(y, l)))
    rho        = And(z3g.formula(cond0),
                     z3g.formula(trans),
                     *(extra_axioms + extra_axioms0))

    bad        = generate_z3_condition(" bad ", wps, decls)#And(x == y, x == l)
    background = And(generate_gamma(wps, decls),
                     *extra_axioms)

    globals = [ decls[x] for x in globals_vocab if t.sorts.ary(x, 0)] #   @ReservedAssignment
    locals  = [ (decls[x0], decls[x]) for x0,x in locals_vocab] # @ReservedAssignment

    from mini_pdr import PDR
    pdr = PDR(init, rho, bad, background, globals, locals, [n])
    pdr.ground_properties += extra_props

    watch = Stopwatch()
    with watch:
        outcome = pdr.run()
        
    if not outcome:
        report_failure(pdr, z3g, vocab)

    print "PDR: %.2fs" % watch.total_time
    print "N =", pdr.N
    print pdr.iteration_count, "iterations"
    print pdr.sat_query_count, "calls to SAT"
