from z3 import *

import shape2horn

null, i, j, h, V0, V1, V2, V3 = Ints('null i j h V0 V1 V2 V3')

Int  = IntSort()
A = IntSort()
n     = Array('n', IntSort(), ArraySort(IntSort(), BoolSort()))
I     = Function('I', Int, Int, Int, Int, BoolSort(), BoolSort(), BoolSort())

fml = ForAll([n, null, j, h, i],
       Implies(And(ForAll([V0, V1, V2],
                          And(And(And(And(And(And(And(n[V0][V0],
                                        Implies(And(n[V0][V2],
                                        n[V2][V1]),
                                        n[V0][V1])),
                                        Implies(And(n[V0][V2],
                                        n[V0][V1]),
                                        Or(n[V2][V1],
                                        n[V1][V2]))),
                                        Implies(And(n[V0][V2],
                                        n[V2][V0]),
                                        V0 == V2)),
                                        Implies(n[null][V0],
                                        V0 == null)),
                                      Implies(n[V0][null],
                                        V0 == null)),
                                  And(ForAll(V3,
                                        I(V3,
                                        i,
                                        h,
                                        null,
                                        n[h][i],
                                        n[h][j])),
                                      Not(i != null))),
                              Implies(n[V0][null],
                                      V0 == null))),
                   h != null),
               n[h][j]))



print fml

print shape2horn.blast_horn(fml)
